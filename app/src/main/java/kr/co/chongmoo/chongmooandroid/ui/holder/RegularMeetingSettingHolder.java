package kr.co.chongmoo.chongmooandroid.ui.holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.woojinsoft.chongmoo.android.R;
import kr.co.chongmoo.chongmooandroid.model.RegularMeetingSetting;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 정기모임 설정 목록 holder
 *
 * @author  Hoon Jang
 * @since   2016.04.13
 */
public class RegularMeetingSettingHolder extends RecyclerView.ViewHolder{
    public static final int RESOURCE_ID = R.layout.recycler_item_regular_meeting_setting;

    public ImageView imageIcon;
    public TextView textTitle;

    public RegularMeetingSettingHolder(View itemView) {
        super(itemView);

        imageIcon = (ImageView) itemView.findViewById(R.id.image_icon);
        textTitle = (TextView) itemView.findViewById(R.id.text_title);
    }

    public static class CommonCoordinator implements BeautifulRecyclerAdapter.Coordinator<RegularMeetingSettingHolder, RegularMeetingSetting>{
        private Activity mActivity;

        public CommonCoordinator(Activity mActivity) {
            this.mActivity = mActivity;
        }

        @Override
        public void coordinate(BeautifulRecyclerAdapter<RegularMeetingSettingHolder, RegularMeetingSetting> adapter, RegularMeetingSettingHolder holder, List<RegularMeetingSetting> itemList, int position) {
            final RegularMeetingSetting item = itemList.get(position);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mActivity.startActivityForResult(item.getIntent(), 0);
                }
            });
            holder.imageIcon.setImageResource(item.getImageResource());
            holder.textTitle.setText(item.getTitle());
        }
    }
}
