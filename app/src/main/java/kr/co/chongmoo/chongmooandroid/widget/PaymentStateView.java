package kr.co.chongmoo.chongmooandroid.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.model.Exemption;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.MonthlyFee;
import kr.co.chongmoo.chongmooandroid.model.PaymentStatus;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.ui.regular.PaymentStatusFragment;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;

/**
 *
 * 회비현황 회비 납부 셀
 *
 * @author  Hoon Jang
 * @since   2016.04.08
 */
public class PaymentStateView extends RelativeLayout implements View.OnClickListener{

    public LinearLayout layoutAmount;
    public TextView textAmount;
    public ImageView imagePaid;
    public TextView textDate;
    public Button btnExamption;
    public Button btnX;
    public ImageView image1st;
    public ImageView imageOut;

    private Fragment mFragment;
    private RecyclerView mRecyclerView;
    private int mPosition;

    private PaymentStatus mPaymentStatus;

    public PaymentStateView(Context context) {
        super(context);
        init();
    }

    public PaymentStateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PaymentStateView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PaymentStateView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_payment_state, this);

        layoutAmount = (LinearLayout) findViewById(R.id.layout_amount);
        textAmount = (TextView) findViewById(R.id.text_amount);
        imagePaid = (ImageView) findViewById(R.id.image_paid);
        textDate = (TextView) findViewById(R.id.text_date);
        btnExamption = (Button) findViewById(R.id.btn_exemption);
        btnX = (Button) findViewById(R.id.btn_x);
        image1st = (ImageView) findViewById(R.id.image_1st);
        imageOut = (ImageView) findViewById(R.id.image_out);

        layoutAmount.setOnClickListener(this);
        imagePaid.setOnClickListener(this);
        btnX.setOnClickListener(this);
        btnX.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String date = String.format("%d%02d", mPaymentStatus.getYear(), mPaymentStatus.getMonth() + 1);

                Realm realm = ((PaymentStatusFragment) mFragment).getRealm();
                realm.beginTransaction();

                RealmList<Exemption> realmList = mPaymentStatus.getMember().getExemptionList();
                if (realmList == null) {
                    realmList = new RealmList<Exemption>();
                }

                Exemption exemption = new Exemption();
                exemption.setDate(date);

                realmList.add(exemption);
                realm.commitTransaction();

                Toast.makeText(getContext(), R.string.toast_examption, Toast.LENGTH_SHORT).show();

                final int scrollY = ((PaymentStatusFragment) mFragment).getScrollY();
                ((PaymentStatusFragment) mFragment).refreshList();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRecyclerView.scrollBy(0, scrollY);
                    }
                }, 0);

                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent("REFRESH"));
                return true;
            }
        });

        btnExamption.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Member member = mPaymentStatus.getMember();

                Realm realm =  ((PaymentStatusFragment)mFragment).getRealm();
                RealmQuery<RegularMeeting> meetingRealmQuery = realm.where(RegularMeeting.class);
                RegularMeeting regularMeeting = meetingRealmQuery.equalTo("seq", member.getMeetingSeq()).findFirst();
                RealmList<MonthlyFee> monthlyFeeList = regularMeeting.getMonthlyFeeList();

                RealmQuery<Income> query =  realm.where(Income.class);
                RealmResults<Income> incomeList = query.equalTo("memberSeq", member.getSeq()).findAllSorted("date", Sort.ASCENDING);

                int incomeAmount = 0;
                for(Income income : incomeList){
                    incomeAmount += income.getAmount();
                }

                int selectedYear = mPaymentStatus.getYear();
                int selectedMonth = mPaymentStatus.getMonth();

                int startYear = DateUtil.getYear(member.getStartDate());
                int startMonth = DateUtil.getMonth(member.getStartDate());

                RealmList<Exemption> exemptionList = member.getExemptionList();
                int totalAmount = incomeAmount;
                int y = startYear;
                int m = startMonth;
                long fee = 0;
                loop:
                while(y < selectedYear || (y == selectedYear && m <= selectedMonth)){
                    for(Exemption exemption : exemptionList) {
                        if (y == selectedYear && m == selectedMonth && Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", selectedYear, selectedMonth+1))) {
                            m++;
                            if(m > 11){
                                m %= 12;
                                y++;
                            }
                            continue loop;
                        }

                        if (Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", y, m+1))) {
                            m++;
                            if(m > 11){
                                m %= 12;
                                y++;
                            }
                            continue loop;
                        }
                    }

                    for(MonthlyFee monthlyFee : monthlyFeeList){
                        if(Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m+1))){
                            fee = monthlyFee.getAmount();
                        }
                    }

                    totalAmount -= fee;
                    if(totalAmount < 0){
                        break;
                    }

                    m++;
                    if(m > 11){
                        m %= 12;
                        y++;
                    }
                }

                if(totalAmount > 0){
                    Toast.makeText(getContext(), R.string.toast_change_lately, Toast.LENGTH_SHORT).show();
                    return true;
                }


                realm.beginTransaction();

                String date = String.format("%d%02d", mPaymentStatus.getYear(), mPaymentStatus.getMonth() + 1);
                Exemption removeItem = null;
                RealmList<Exemption> realmList = mPaymentStatus.getMember().getExemptionList();
                for (Exemption exemption : realmList) {
                    if (date.equals(exemption.getDate())) {
                        removeItem = exemption;
                        break;
                    }
                }

                if (removeItem != null) {
                    realmList.remove(removeItem);
                }

                realm.commitTransaction();

                Toast.makeText(getContext(), R.string.toast_examption_cancel, Toast.LENGTH_SHORT).show();
                
                final int scrollY = ((PaymentStatusFragment) mFragment).getScrollY();
                ((PaymentStatusFragment) mFragment).refreshList();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRecyclerView.scrollBy(0, scrollY);
                    }
                }, 0);

                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent("REFRESH"));
                return true;
            }
        });
    }

    public void setPaymentStatus(PaymentStatus paymentStatus){
        mPaymentStatus = paymentStatus;
        switch (paymentStatus.getStatus()){
            case PaymentStatus.STATUS_NOTHING:
                textAmount.setVisibility(View.GONE);
                imagePaid.setVisibility(View.GONE);
                textDate.setVisibility(View.VISIBLE);
                btnExamption.setVisibility(View.GONE);
                btnX.setVisibility(View.GONE);
                image1st.setVisibility(View.GONE);
                imageOut.setVisibility(View.GONE);

                textDate.setText("-");
                break;
            case PaymentStatus.STATUS_NOT_PAID:
                textAmount.setVisibility(View.GONE);
                imagePaid.setVisibility(View.GONE);
                textDate.setVisibility(View.GONE);
                btnExamption.setVisibility(View.GONE);
                btnX.setVisibility(View.VISIBLE);
                image1st.setVisibility(View.GONE);
                imageOut.setVisibility(View.GONE);
                break;
            case PaymentStatus.STATUS_PAID:
                textAmount.setVisibility(View.VISIBLE);
                imagePaid.setVisibility(View.GONE);
                textDate.setVisibility(View.VISIBLE);
                btnExamption.setVisibility(View.GONE);
                btnX.setVisibility(View.GONE);
                image1st.setVisibility(View.GONE);
                imageOut.setVisibility(View.GONE);

                textAmount.setText(String.format("%,d", paymentStatus.getAmount()));
                textDate.setText(DateUtil.getDateToMonthDayString(paymentStatus.getIncome().getDate()));
                break;
            case PaymentStatus.STATUS_PAID_IMAGE:
                textAmount.setVisibility(View.GONE);
                imagePaid.setVisibility(View.VISIBLE);
                textDate.setVisibility(View.VISIBLE);
                btnExamption.setVisibility(View.GONE);
                btnX.setVisibility(View.GONE);
                image1st.setVisibility(View.GONE);
                imageOut.setVisibility(View.GONE);

                textAmount.setText(String.format("%,d", paymentStatus.getAmount()));
                textDate.setText(DateUtil.getDateToMonthDayString(paymentStatus.getIncome().getDate()));
                break;
            case PaymentStatus.STATUS_EXEMPTION:
                textAmount.setVisibility(View.GONE);
                imagePaid.setVisibility(View.GONE);
                textDate.setVisibility(View.GONE);
                btnExamption.setVisibility(View.VISIBLE);
                btnX.setVisibility(View.GONE);
                image1st.setVisibility(View.GONE);
                imageOut.setVisibility(View.GONE);
                break;
        }

        image1st.setVisibility(paymentStatus.isNew() ? View.VISIBLE : View.GONE);
        imageOut.setVisibility(paymentStatus.isOut() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image_paid:
            case R.id.layout_amount: {
                if(mPaymentStatus.getStatus() != PaymentStatus.STATUS_PAID && mPaymentStatus.getStatus() != PaymentStatus.STATUS_PAID_IMAGE)
                    return ;

                Member member = mPaymentStatus.getMember();
                Realm realm =  ((PaymentStatusFragment)mFragment).getRealm();
                RealmQuery<Income> query =  realm.where(Income.class);
                RealmResults<Income> incomeList = query.equalTo("memberSeq", member.getSeq()).findAllSorted("date", Sort.ASCENDING);

                for(Income income : incomeList){
                    if(income.getDate().getTime() > mPaymentStatus.getIncome().getDate().getTime()){
                        Toast.makeText(getContext(), R.string.toast_change_lately, Toast.LENGTH_SHORT).show();
                        return ;
                    }
                }

                final ChongmooDialog dialog = new ChongmooDialog(getContext(), ChongmooDialog.MODE_INCOME_WRITE);
                dialog.setTitle(mFragment.getContext().getString(R.string.dialog_income_modify));
                dialog.setIncome(mPaymentStatus.getIncome());
                dialog.setPositiveButton(mFragment.getContext().getString(R.string.confirm), new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int editAmount = dialog.getAmount();
                        Date date = dialog.getIncomeDate();
                        Member member = mPaymentStatus.getMember();

                        if(editAmount < 1){
                            Toast.makeText(getContext(), R.string.toast_require_fee, Toast.LENGTH_SHORT).show();
                            return ;
                        }

                        Realm realm =  ((PaymentStatusFragment)mFragment).getRealm();

                        RealmQuery<RegularMeeting> meetingRealmQuery = realm.where(RegularMeeting.class);
                        RegularMeeting regularMeeting = meetingRealmQuery.equalTo("seq", member.getMeetingSeq()).findFirst();
                        RealmList<MonthlyFee> monthlyFeeList = regularMeeting.getMonthlyFeeList();

                        RealmQuery<Income> query =  realm.where(Income.class);
                        RealmResults<Income> incomeList = query.equalTo("memberSeq", member.getSeq()).findAllSorted("date", Sort.ASCENDING);

                        long lastTime = 0;
                        int incomeAmount = 0;
                        for(Income income : incomeList){
                            incomeAmount += income.getAmount();
                            if(mPaymentStatus.getIncome().getDate().getTime() <= income.getDate().getTime())
                                continue;

                            if(lastTime < income.getDate().getTime()){
                                lastTime = income.getDate().getTime();
                            }
                        }

                        if(mPaymentStatus.getIncome().getDate().getTime() != date.getTime() && lastTime > date.getTime()){
                            Toast.makeText(getContext(), String.format(mFragment.getContext().getString(R.string.toast_lower_date_2), DateUtil.getDateToYearMonthDayDotString(new Date(lastTime))), Toast.LENGTH_SHORT).show();
                            return ;
                        }

                        int selectedYear = Integer.parseInt(monthlyFeeList.get(monthlyFeeList.size() - 1).getDate().substring(0, 4));
                        int selectedMonth = Integer.parseInt(monthlyFeeList.get(monthlyFeeList.size() - 1).getDate().substring(4, 6));

                        int startYear = DateUtil.getYear(member.getStartDate());
                        int startMonth = DateUtil.getMonth(member.getStartDate());
                        if(startYear > selectedYear || (startYear == selectedYear && startMonth > selectedMonth)){
                            selectedYear = startYear;
                            selectedMonth = startMonth;
                        }

                        RealmList<Exemption> exemptionList = member.getExemptionList();
                        int totalAmount = editAmount + incomeAmount;
                        int y = startYear;
                        int m = startMonth;
                        long fee = 0;

                        for(MonthlyFee monthlyFee : monthlyFeeList){
                            if(Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m+1))){
                                fee = monthlyFee.getAmount();
                            }
                        }

                        loop:
                        while(y < selectedYear || (y == selectedYear && m <= selectedMonth)){
                            for(Exemption exemption : exemptionList) {
                                if (Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", y, m+1))) {
                                    m++;
                                    if(m > 11){
                                        m %= 12;
                                        y++;
                                    }
                                    continue loop;
                                }
                            }

                            for(MonthlyFee monthlyFee : monthlyFeeList){
                                if(Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m+1))){
                                    fee = monthlyFee.getAmount();
                                }
                            }

                            totalAmount -= fee;
                            if(totalAmount <= 0){
                                break;
                            }

                            m++;
                            if(m > 11){
                                m %= 12;
                                y++;
                            }
                        }

                        if(fee != 0 && totalAmount % fee != 0){
                            Toast.makeText(getContext(), R.string.toast_require_multiple, Toast.LENGTH_SHORT).show();
                            return ;
                        }

                        RealmQuery<Income> incomeQuery = realm.where(Income.class);
                        Income income = incomeQuery.equalTo("seq", mPaymentStatus.getIncome().getSeq()).findFirst();
                        realm.beginTransaction();
                        income.setAmount(editAmount);
                        income.setDate(date);
                        realm.commitTransaction();

                        final int scrollY = ((PaymentStatusFragment)mFragment).getScrollY();
                        ((PaymentStatusFragment)mFragment).refreshList();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mRecyclerView.scrollBy(0, scrollY);
                            }
                        }, 0);

                        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent("REFRESH"));
                        Toast.makeText(getContext(), R.string.toast_modified, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(mFragment.getContext().getString(R.string.dialog_income_modify_cancel), new OnClickListener() {
                    private Handler mHandler = new Handler();

                    @Override
                    public void onClick(View v) {
                        Realm realm =  ((PaymentStatusFragment)mFragment).getRealm();
                        RealmQuery<Income> incomeQuery = realm.where(Income.class);
                        Income income = incomeQuery.equalTo("seq", mPaymentStatus.getIncome().getSeq()).findFirst();

                        realm.beginTransaction();
                        income.removeFromRealm();
                        realm.commitTransaction();

                        final int scrollY = ((PaymentStatusFragment)mFragment).getScrollY();
                        ((PaymentStatusFragment)mFragment).refreshList();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mRecyclerView.scrollBy(0, scrollY);
                            }
                        }, 0);

                        Toast.makeText(getContext(), R.string.toast_modified, Toast.LENGTH_SHORT).show();
                        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent("REFRESH"));
                        dialog.dismiss();
                    }
                });
                dialog.show();

                break;
            }
            case R.id.btn_x: {
                Member member = mPaymentStatus.getMember();

                Realm realm =  ((PaymentStatusFragment)mFragment).getRealm();
                RealmQuery<RegularMeeting> meetingRealmQuery = realm.where(RegularMeeting.class);
                RegularMeeting regularMeeting = meetingRealmQuery.equalTo("seq", member.getMeetingSeq()).findFirst();
                RealmList<MonthlyFee> monthlyFeeList = regularMeeting.getMonthlyFeeList();

                RealmQuery<Income> query =  realm.where(Income.class);
                RealmResults<Income> incomeList = query.equalTo("memberSeq", member.getSeq()).findAllSorted("date", Sort.ASCENDING);

                int incomeAmount = 0;
                for(Income income : incomeList){
                    incomeAmount += income.getAmount();
                }

                int selectedYear = ((PaymentStatusFragment)mFragment).getYear();
                int selectedMonth = ((PaymentStatusFragment)mFragment).getMonth();

                int startYear = DateUtil.getYear(member.getStartDate());
                int startMonth = DateUtil.getMonth(member.getStartDate());

                RealmList<Exemption> exemptionList = member.getExemptionList();
                int totalAmount = incomeAmount;
                int y = startYear;
                int m = startMonth;
                long fee = 0;
                loop:
                while(y < selectedYear || (y == selectedYear && m <= selectedMonth)){
                    for(Exemption exemption : exemptionList) {
                        if (Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", y, m+1))) {
                            m++;
                            if(m > 11){
                                m %= 12;
                                y++;
                            }
                            continue loop;
                        }
                    }

                    for(MonthlyFee monthlyFee : monthlyFeeList){
                        if(Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m+1))){
                            fee = monthlyFee.getAmount();
                        }
                    }

                    totalAmount -= fee;
                    if(totalAmount < 0){
                        break;
                    }

                    m++;
                    if(m > 11){
                        m %= 12;
                        y++;
                    }
                }

                final ChongmooDialog dialog = new ChongmooDialog(getContext(), ChongmooDialog.MODE_INCOME_WRITE);
                dialog.setTitle(mFragment.getContext().getString(R.string.dialog_income_title));
                dialog.setAmount(fee);
                dialog.setPositiveButton(mFragment.getContext().getString(R.string.dialog_income_regist), new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        long editAmount = dialog.getAmount();
                        Date date = dialog.getIncomeDate();
                        Member member = mPaymentStatus.getMember();

                        if(editAmount < 1){
                            Toast.makeText(getContext(), R.string.toast_require_fee, Toast.LENGTH_SHORT).show();
                            return ;
                        }

                        Realm realm =  ((PaymentStatusFragment)mFragment).getRealm();

                        RealmQuery<RegularMeeting> meetingRealmQuery = realm.where(RegularMeeting.class);
                        RegularMeeting regularMeeting = meetingRealmQuery.equalTo("seq", member.getMeetingSeq()).findFirst();
                        RealmList<MonthlyFee> monthlyFeeList = regularMeeting.getMonthlyFeeList();

                        RealmQuery<Income> query =  realm.where(Income.class);
                        RealmResults<Income> incomeList = query.equalTo("memberSeq", member.getSeq()).findAllSorted("date", Sort.ASCENDING);

                        long lastTime = 0;
                        int incomeAmount = 0;
                        for(Income income : incomeList){
                            if(lastTime < income.getDate().getTime()){
                                lastTime = income.getDate().getTime();
                            }
                            incomeAmount += income.getAmount();
                        }

                        if(lastTime > date.getTime()){
                            Toast.makeText(getContext(), String.format(mFragment.getContext().getString(R.string.toast_lower_date_3), DateUtil.getDateToYearMonthDayDotString(new Date(lastTime))), Toast.LENGTH_SHORT).show();
                            return ;
                        }

                        int selectedYear = Integer.parseInt(monthlyFeeList.get(monthlyFeeList.size() - 1).getDate().substring(0, 4));
                        int selectedMonth = Integer.parseInt(monthlyFeeList.get(monthlyFeeList.size() - 1).getDate().substring(4, 6));

                        int startYear = DateUtil.getYear(member.getStartDate());
                        int startMonth = DateUtil.getMonth(member.getStartDate());
                        if(startYear > selectedYear || (startYear == selectedYear && startMonth > selectedMonth)){
                            selectedYear = startYear;
                            selectedMonth = startMonth;
                        }

                        RealmList<Exemption> exemptionList = member.getExemptionList();
                        long totalAmount = editAmount + incomeAmount;
                        int y = startYear;
                        int m = startMonth;
                        long fee = 0;
                        for(MonthlyFee monthlyFee : monthlyFeeList){
                            if(Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m+1))){
                                fee = monthlyFee.getAmount();
                            }
                        }

                        loop:
                        while(y < selectedYear || (y == selectedYear && m <= selectedMonth)){
                            for(Exemption exemption : exemptionList) {
                                if (Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", y, m+1))) {
                                    m++;
                                    if(m > 11){
                                        m %= 12;
                                        y++;
                                    }
                                    continue loop;
                                }
                            }

                            for(MonthlyFee monthlyFee : monthlyFeeList){
                                if(Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m+1))){
                                    fee = monthlyFee.getAmount();
                                }
                            }

                            totalAmount -= fee;
                            if(totalAmount <= 0){
                                break;
                            }

                            m++;
                            if(m > 11){
                                m %= 12;
                                y++;
                            }
                        }

                        if(fee != 0 && totalAmount % fee != 0){
                            Toast.makeText(getContext(), R.string.toast_require_multiple, Toast.LENGTH_SHORT).show();
                            return ;
                        }


                        realm.beginTransaction();
                        Income income = realm.createObject(Income.class);
                        income.setSeq(Income.generateSequence(realm));
                        income.setMeetingSeq(member.getMeetingSeq());
                        income.setDate(date);
                        income.setDay(DateUtil.getDayOfMonth(date));
                        income.setType(0);
                        income.setAmount(editAmount);
                        income.setCategory(mFragment.getContext().getString(R.string.activity_income_write_category_fee));
                        income.setContent(member.getName());
                        income.setMemberSeq(member.getSeq());
                        realm.commitTransaction();

                        final int scrollY = ((PaymentStatusFragment)mFragment).getScrollY();
                        ((PaymentStatusFragment)mFragment).refreshList();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mRecyclerView.scrollBy(0, scrollY);
                            }
                        }, 0);

                        Toast.makeText(getContext(), R.string.toast_income_regist, Toast.LENGTH_SHORT).show();
                        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent("REFRESH"));
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(mFragment.getContext().getString(R.string.cancel), null);
                dialog.show();
                break;
            }
        }
    }

    public Fragment getFragment() {
        return mFragment;
    }

    public void setFragment(Fragment fragment) {
        this.mFragment = fragment;
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.mRecyclerView = recyclerView;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        this.mPosition = position;
    }
}
