package kr.co.chongmoo.chongmooandroid.ui.casual;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.model.AddressBook;
import kr.co.chongmoo.chongmooandroid.model.CasualMeeting;
import kr.co.chongmoo.chongmooandroid.model.CasualMeetingMember;
import kr.co.chongmoo.chongmooandroid.model.CasualMeetingStep;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.util.StringUtil;
import kr.co.chongmoo.chongmooandroid.widget.CasualMeetingSharePersonCalculateView;
import kr.co.chongmoo.chongmooandroid.widget.CasualMeetingShareStepView;

/**
 * N빵 정산 공유 관련 화면
 */
public class CasualShareActivity extends BaseSubActivity implements View.OnClickListener {

    private ImageView mImageViewBtnShareKakao;
    private ImageView mImageViewBtnShareSms;
    private ImageView mImageViewBtnShareBand;

    private TextView mTextViewTxtCslShareDate;
    private TextView mTextViewTxtCslShareMeetingName;

    private LinearLayout mLinearLayoutCslShareMeetingAmount;
    private TextView mTextViewTxtCslShareMeetingAmount;

    private LinearLayout mLinearLayoutCslShareMeetingCotentStep;

    private LinearLayout mLinearLayoutCslShareMeetingCotentPersonTotalAmount;

    private LinearLayout mLinearLayoutCslShareMeetingAmountBalance;
    private TextView mTextViewTxtCslShareMeetingAmountBalance;

    private LinearLayout mLinearLayoutCslShareMeetingContentAccount;
    private LinearLayout mLinearLayoutCslContentAccountBank;
    private TextView mTextViewCslContentAccountBank;
    private LinearLayout mLinearLayoutCslContentAccountBankHolder;
    private TextView mTextViewCslContentAccountBankHolder;
    private LinearLayout mLinearLayoutCslContentAccountNumber;
    private TextView mTextViewCslContentAccountBankNumber;

    private LinearLayout mLinearLayoutCslShareContentMemo;
    private TextView mTextViewTxtCslShareContentMemo;

    private CasualMeeting mCasualMeeting;

    private Realm mRealm;

    private StringBuffer mStringBufferShareText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 기본 화면 구성 관련 코드 시작
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_casual_share);
        setTitle(getString(R.string.title_activity_casual_share));

        if(mRealm == null || mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }

        mTextViewTxtCslShareDate = (TextView) findViewById(R.id.txt_csl_share_date);
        mTextViewTxtCslShareMeetingName = (TextView) findViewById(R.id.txt_csl_share_meeting_name);
        mLinearLayoutCslShareMeetingAmount = (LinearLayout) findViewById(R.id.layer_csl_share_meeting_amount);
        mTextViewTxtCslShareMeetingAmount = (TextView) findViewById(R.id.txt_csl_share_meeting_amount);
        mLinearLayoutCslShareMeetingCotentStep = (LinearLayout) findViewById(R.id.layer_csl_content_step);
        mLinearLayoutCslShareMeetingCotentPersonTotalAmount = (LinearLayout) findViewById(R.id.layer_csl_content_person_total_amount);
        mLinearLayoutCslShareMeetingContentAccount = (LinearLayout) findViewById(R.id.layer_csl_content_account);
        mLinearLayoutCslShareMeetingAmountBalance = (LinearLayout) findViewById(R.id.layer_csl_share_meeting_amount_balance);
        mTextViewTxtCslShareMeetingAmountBalance = (TextView) findViewById(R.id.txt_csl_share_meeting_amount_balance);
        mLinearLayoutCslContentAccountBank = (LinearLayout) findViewById(R.id.layer_csl_content_account_bank);
        mTextViewCslContentAccountBank = (TextView) findViewById(R.id.txt_csl_content_account_bank);
        mLinearLayoutCslContentAccountBankHolder = (LinearLayout) findViewById(R.id.layer_csl_content_account_bank_holder);
        mTextViewCslContentAccountBankHolder = (TextView) findViewById(R.id.txt_csl_content_account_bank_holder);
        mLinearLayoutCslContentAccountNumber = (LinearLayout) findViewById(R.id.layer_csl_content_account_bank_number);
        mTextViewCslContentAccountBankNumber = (TextView) findViewById(R.id.txt_csl_content_account_bank_number);
        mLinearLayoutCslShareContentMemo = (LinearLayout) findViewById(R.id.layer_csl_content_memo);
        mTextViewTxtCslShareContentMemo = (TextView) findViewById(R.id.txt_csl_content_memo);

        mImageViewBtnShareKakao = (ImageView) findViewById(R.id.btn_share_kakao);
        mImageViewBtnShareKakao.setOnClickListener(this);
        mImageViewBtnShareSms = (ImageView) findViewById(R.id.btn_share_sms);
        mImageViewBtnShareSms.setOnClickListener(this);
        mImageViewBtnShareBand = (ImageView) findViewById(R.id.btn_share_band);
        mImageViewBtnShareBand.setOnClickListener(this);
        // 기본 화면 구성 관련 코드 끝
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 데이터 베이스 질의 로직 시작
        int seq = getIntent().getIntExtra(CasualMeetingActivity.SEQ_KEY, 0);
        RealmQuery<CasualMeeting> query = mRealm.where(CasualMeeting.class);
        CasualMeeting casualMeeting = query.equalTo(CasualMeetingActivity.SEQ_KEY, seq).findFirst();
        mCasualMeeting = casualMeeting;
        // 데이터 베이스 질의 로직 끝
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 가져온 데이터에 대한 화면 구성 시작

        // 기본적인 데이터 구성
        mStringBufferShareText = new StringBuffer(getString(R.string.plain_text_sns_share_meeting_first_guide));
        int remainAmount = StringUtil.parseToInterger(casualMeeting.getMount());
        int remainAmountPoint = remainAmount; // 중간 중간 체크를 위해 사용 되는 값
        RealmList<AddressBook> addressBookList = casualMeeting.getAddressBookList();
        List<CasualMeetingMember> casualMeetingMemberList = new ArrayList<CasualMeetingMember>();
        for(AddressBook addressBook : addressBookList) {
            CasualMeetingMember casualMeetingMember = new CasualMeetingMember();
            casualMeetingMember.setName(addressBook.getName());
            casualMeetingMember.setAmount(0);
            casualMeetingMemberList.add(casualMeetingMember);
        }

        // 모임 정모 ( 이름, 날짜) 화면 구성 및 공유 텍스트 구성
        mTextViewTxtCslShareDate.setText(String.format(getString(R.string.lable_sns_share_meeting_date_format), DateUtil.getDateToYearMonthDayString(casualMeeting.getDate())));
        mTextViewTxtCslShareMeetingName.setText(String.format(getString(R.string.lable_sns_share_meeting_name_format), casualMeeting.getName()));
        mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_date_format), DateUtil.getDateToYearMonthDayString(casualMeeting.getDate())));
        mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_name_format), casualMeeting.getName()));

        // 모임 수입 및 공유 텍스트 구성 코드
        if(StringUtil.isNotEmpty(casualMeeting.getMount()) == true) {
            mLinearLayoutCslShareMeetingAmount.setVisibility(View.VISIBLE);
            mTextViewTxtCslShareMeetingAmount.setText(String.format(getString(R.string.lable_sns_share_meeting_amount_format), StringUtil.parseToInterger(casualMeeting.getMount())));
            mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_amount_format), StringUtil.parseToInterger(casualMeeting.getMount())));
        } else {
            mLinearLayoutCslShareMeetingAmount.setVisibility(View.GONE);
        }

        mStringBufferShareText.append(getString(R.string.plain_text_sns_share_meeting_hypen));

        // 차수별 금액 정산
        RealmList<CasualMeetingStep> casualMeetingStepRealmList = casualMeeting.getCasualMeetingStepList();
        for( int i = 0 ; i < casualMeetingStepRealmList.size() ; i++ ) {

            CasualMeetingStep casualMeetingStep = casualMeetingStepRealmList.get(i);
            CasualMeetingShareStepView casualMeetingShareStepView = new CasualMeetingShareStepView(this);
            casualMeetingShareStepView.setShareMeetingNameLabel(String.format(getString(R.string.lable_sns_share_meeting_step_label_format), i + 1));
            casualMeetingShareStepView.setShareMeetingName(String.format(getString(R.string.lable_sns_share_meeting_step_label_value_format), casualMeetingStep.getArea(), casualMeetingStep.getAddressBookList().size()));
            mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_step_label_format), i + 1, casualMeetingStep.getArea(), casualMeetingStep.getAddressBookList().size()));

            if(remainAmount > 0) { // 수입 잔액 표시 관련 화면 구성 및 공유 텍스트 구성 코드
                casualMeetingShareStepView.setShareMeetingBalance(String.format(getString(R.string.lable_sns_share_meeting_step_balance_format), remainAmount));
                mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_step_balance_format), remainAmount));
            }
            remainAmount -= StringUtil.parseToInterger(casualMeetingStep.getAmount());
            remainAmountPoint -= StringUtil.parseToInterger(casualMeetingStep.getAmount());

            // n 차별 지출 금액 화면 구성 및 공유 텍스트 구성 코드
            casualMeetingShareStepView.setShareMeetingAmountLable(String.format(getString(R.string.lable_sns_share_meeting_step_amount_label_format), i + 1));
            casualMeetingShareStepView.setShareMeetingAmount(String.format(getString(R.string.lable_sns_share_meeting_step_amount_format), StringUtil.parseToInterger(casualMeetingStep.getAmount())));
            mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_step_amount_format), i + 1, StringUtil.parseToInterger(casualMeetingStep.getAmount())));

            // n 차별 1/N 금액 화면 구성 및 공유 텍스트 구성 코드 , 정산 금액을 위한 데이터 셋팅
            casualMeetingShareStepView.setShareMeetingNAmountLable(String.format(getString(R.string.lable_sns_share_meeting_step_n_amount_label_format), i + 1));
            if(remainAmountPoint > 0) {
                casualMeetingShareStepView.setShareMeetingNAmount(getString(R.string.lable_sns_share_meeting_step_n_amount_zero));
                mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_step_n_amount_zero), i + 1));
            } else {

                int nAmount = 0;
                if(casualMeetingStep != null && casualMeetingStep.getAddressBookList().size() > 0) {
                    nAmount = (remainAmountPoint * -1) / casualMeetingStep.getAddressBookList().size();
                }
                casualMeetingShareStepView.setShareMeetingNAmount(String.format(getString(R.string.lable_sns_share_meeting_step_n_amount_format), nAmount));
                mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_step_n_amount_format), i + 1, nAmount));

                // 데이터 관련 셋팅
                remainAmountPoint = 0;
                RealmList<AddressBook> stepAddressBookList = casualMeetingStep.getAddressBookList();
                for(CasualMeetingMember casualMeetingMember : casualMeetingMemberList) {
                    String personName = casualMeetingMember.getName();
                    for(AddressBook stepAddressBook : stepAddressBookList) {
                        if(personName.equals(stepAddressBook.getName())) {
                            casualMeetingMember.setAmount(casualMeetingMember.getAmount() + nAmount);
                        }
                    }
                }

            }

            if(casualMeetingStepRealmList.size() != i + 1) {
                mStringBufferShareText.append(getString(R.string.plain_text_enter_string));
            }

            mLinearLayoutCslShareMeetingCotentStep.addView(casualMeetingShareStepView);

        }

        // 정산금액 화면 구성 및 공유 텍스트 구성 코드 ( 김철수(1차,2차,3차) :  10000원 )
        mStringBufferShareText.append(getString(R.string.plain_text_sns_share_meeting_calculate_title));
        for(CasualMeetingMember casualMeetingMember : casualMeetingMemberList) {

            StringBuffer memberLabel = new StringBuffer(casualMeetingMember.getName());

            // 관련된 차수를 리스트에 넣는다.
            List<String> step = new ArrayList<String>();
            for( int i = 0 ; i < casualMeetingStepRealmList.size() ; i++ ) {
                CasualMeetingStep casualMeetingStep = casualMeetingStepRealmList.get(i);
                for(AddressBook addressBook : casualMeetingStep.getAddressBookList()){
                    if(memberLabel.toString().equals(addressBook.getName())) {
                        step.add(new Integer( i + 1 ).toString());
                    }
                }
            }

            // 관련된 차수를 스트링 형태로 만들어 준다.
            for( int i = 0 ; i < step.size() ; i++ ) {
                if( i == 0 && step.size() == 1) {
                    memberLabel.append(String.format("(%s)", step.get(i)));
                } else if ( i == 0 ) {
                    memberLabel.append(String.format("(%s", step.get(i)));
                } else if( (step.size() - 1) == i) {
                    memberLabel.append(String.format(",%s)", step.get(i)));
                } else {
                    memberLabel.append(String.format(",%s", step.get(i)));
                }
            }

            // 화면 구성 로직
            CasualMeetingSharePersonCalculateView casualMeetingSharePersonCalculateView = new CasualMeetingSharePersonCalculateView(this);
            casualMeetingSharePersonCalculateView.setPersonName(memberLabel.toString());
            casualMeetingSharePersonCalculateView.setPersonAmount(String.format(getString(R.string.lable_sns_share_meeting_step_person_amount_format), casualMeetingMember.getAmount()));
            mLinearLayoutCslShareMeetingCotentPersonTotalAmount.addView(casualMeetingSharePersonCalculateView);
            mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_step_person_amount_format), memberLabel.toString(), casualMeetingMember.getAmount()));

        }

        // 수입 잔액 화면 구성 및 공유 텍스트 구성 코드
        if(StringUtil.isNotEmpty(casualMeeting.getMount()) == true && remainAmount> 0) {
            mLinearLayoutCslShareMeetingAmountBalance.setVisibility(View.VISIBLE);
            mTextViewTxtCslShareMeetingAmountBalance.setText(String.format("%,d원", remainAmount));
            mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_step_amount_balance_format), remainAmount));
        } else {
            mLinearLayoutCslShareMeetingAmountBalance.setVisibility(View.GONE);
        }

        // 입금 계좌 화면 구성 및 공유 텍스트 구성 코드
        if(StringUtil.isNotEmpty(casualMeeting.getAccountNumber()) == true || StringUtil.isNotEmpty(casualMeeting.getAccountHolder()) == true
                || StringUtil.isNotEmpty(casualMeeting.getBank()) == true ) {

            mStringBufferShareText.append(getString(R.string.plain_text_sns_share_meeting_content_account));
            mLinearLayoutCslShareMeetingContentAccount.setVisibility(View.VISIBLE);

            if(StringUtil.isNotEmpty(casualMeeting.getBank()) == true) {
                mTextViewCslContentAccountBank.setText(casualMeeting.getBank());
                mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_content_account_bank_format), casualMeeting.getBank()));
            } else {
                mLinearLayoutCslContentAccountBank.setVisibility(View.GONE);
            }

            if(StringUtil.isNotEmpty(casualMeeting.getAccountHolder()) == true) {
                mTextViewCslContentAccountBankHolder.setText(casualMeeting.getAccountHolder());
                mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_content_account_bank_holder_format), casualMeeting.getAccountHolder()));
            } else {
                mLinearLayoutCslContentAccountBankHolder.setVisibility(View.GONE);
            }

            if(StringUtil.isNotEmpty(casualMeeting.getAccountNumber()) == true) {
                mTextViewCslContentAccountBankNumber.setText(casualMeeting.getAccountNumber());
                mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_content_account_bank_number_format), casualMeeting.getAccountNumber()));
            } else {
                mLinearLayoutCslContentAccountNumber.setVisibility(View.GONE);
            }

            //if(remainAmount > 0) mStringBufferShareText.append(String.format(getString(R.string.plain_text_sns_share_meeting_remain_ammount_format), remainAmount));

        } else {
            mLinearLayoutCslShareMeetingContentAccount.setVisibility(View.GONE);
        }

        // 메모 화면 구성 및 공유 텍스트 구성 코드
        if(StringUtil.isNotEmpty(casualMeeting.getMemo()) == true) {
            mLinearLayoutCslShareContentMemo.setVisibility(View.VISIBLE);
            mTextViewTxtCslShareContentMemo.setText(casualMeeting.getMemo());
            mStringBufferShareText.append(getString(R.string.plain_text_sns_share_meeting_memo_title));
            mStringBufferShareText.append(casualMeeting.getMemo() + getString(R.string.plain_text_enter_string));
        } else {
            mLinearLayoutCslShareContentMemo.setVisibility(View.GONE);
        }

        // 가져온 데이터에 대한 화면 구성 끝
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent();

        switch (v.getId()) {
            case R.id.btn_share_kakao:
                intent.setPackage("com.kakao.talk");
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, mStringBufferShareText.toString());
                intent.setType("text/plain");
                break;
            case R.id.btn_share_sms:
                intent.setAction(Intent.ACTION_VIEW);
                intent.putExtra("sms_body", mStringBufferShareText.toString());
                intent.setType("vnd.android-dir/mms-sms");
                break;
            case R.id.btn_share_band:
                intent.setPackage("com.nhn.android.band");
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, mStringBufferShareText.toString());
                intent.setType("text/plain");
                break;
        }

        try {
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            //Toast.makeText(getBaseContext(), getString(R.string.toast_casual_meeting_share_fail_txt) + e.toString(), Toast.LENGTH_SHORT).show();
            Toast.makeText(getBaseContext(), getString(R.string.toast_casual_meeting_share_fail_txt), Toast.LENGTH_SHORT).show();
        }

    }

}