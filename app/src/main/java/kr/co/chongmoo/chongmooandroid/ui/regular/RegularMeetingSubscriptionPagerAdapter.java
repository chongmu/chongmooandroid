package kr.co.chongmoo.chongmooandroid.ui.regular;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.viewpagerindicator.IconPagerAdapter;

import com.woojinsoft.chongmoo.android.R;

/**
 *
 * 결제 Adapter
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class RegularMeetingSubscriptionPagerAdapter extends FragmentPagerAdapter implements IconPagerAdapter {

    public RegularMeetingSubscriptionPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new TutorialFragment(R.layout.view_tutorial_0);
                break;
            case 1:
                fragment = new TutorialFragment(R.layout.view_tutorial_1);
                break;
            case 2:
                fragment = new TutorialFragment(R.layout.view_tutorial_2);
                break;
            case 3:
                fragment = new TutorialFragment(R.layout.view_tutorial_3);
                break;
        }

        return fragment;

    }

    @Override
    public int getIconResId(int index) {
        return R.drawable.indicator_pager_tutorial;
    }

    @Override
    public int getCount() {
        return 4;
    }

}
