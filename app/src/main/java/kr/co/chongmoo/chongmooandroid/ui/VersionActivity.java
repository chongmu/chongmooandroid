package kr.co.chongmoo.chongmooandroid.ui;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.model.HttpResponse;
import kr.co.chongmoo.chongmooandroid.model.MobileVersion;
import kr.co.chongmoo.chongmooandroid.network.RetrofitManager;
import kr.co.chongmoo.chongmooandroid.network.service.APIService;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 버젼 확인 관련 엑티비티
 */
public class VersionActivity extends BaseSubActivity {

    private ViewGroup mVersUpgrNeq;
    private ViewGroup mVersUpgrEq;
    private TextView mVersLastestTxt;
    private TextView mVersLastTxt;

    private LinearLayout mLinearLayoutVersBtmLayoutUpgrNeq;
    private LinearLayout mLinearLayoutVersBtmLayoutUpgrEq;

    private ChongmooDialog mChongmooDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version);
        setTitle(getString(R.string.title_activity_version));

        mChongmooDialog = new ChongmooDialog(this, ChongmooDialog.MODE_LOADING);

        mVersUpgrNeq = (ViewGroup) findViewById(R.id.vers_btm_layout_upgr_neq);
        mVersUpgrEq = (ViewGroup) findViewById(R.id.vers_btm_layout_upgr_eq);
        mVersLastestTxt = (TextView) findViewById(R.id.version_lastest_text);
        mVersLastTxt = (TextView) findViewById(R.id.version_last_text);

        mLinearLayoutVersBtmLayoutUpgrNeq = (LinearLayout) findViewById(R.id.vers_btm_layout_upgr_neq);
        mLinearLayoutVersBtmLayoutUpgrNeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=" + getPackageName()));
                startActivity(intent);

            }
        });

        mLinearLayoutVersBtmLayoutUpgrEq = (LinearLayout) findViewById(R.id.vers_btm_layout_upgr_eq);

        boolean netWorkAbleStatus = isNetWorkAble();

        if(netWorkAbleStatus == true) { // 네트워크 통신이 가능하면 API에서 최신 버젼을 가져온다.
            mChongmooDialog.show();
            APIService apiService = RetrofitManager.getInstance().create(APIService.class);
            Call<HttpResponse<MobileVersion>> call = apiService.lastestVersion();
            call.enqueue(new Callback<HttpResponse<MobileVersion>>() {

                @Override
                public void onResponse(Call<HttpResponse<MobileVersion>> call, Response<HttpResponse<MobileVersion>> response) {

                    String currentVersion = getApplicationVersionNumber();
                    int code = response.code();
                    if (code == 200) {
                        HttpResponse<MobileVersion> httpResponse = response.body();
                        String versionNumber = httpResponse.getResults().getVersionNumber();
                        if (currentVersion.equals(versionNumber) == false) {

                            mVersUpgrEq.setVisibility(View.VISIBLE);
                            mVersUpgrNeq.setVisibility(View.GONE);
                            mVersLastTxt.setText(String.format(getString(R.string.activity_version_current_version), currentVersion));
                            mVersLastestTxt.setText(String.format("V%s", versionNumber));

                            mLinearLayoutVersBtmLayoutUpgrEq.setVisibility(View.GONE);
                            mLinearLayoutVersBtmLayoutUpgrNeq.setVisibility(View.VISIBLE);

                        } else {

                            showSameVersion();

                        }

                    } else {
                        showSameVersion();
                    }
                    mChongmooDialog.dismiss();

                }

                @Override
                public void onFailure(Call<HttpResponse<MobileVersion>> call, Throwable t) {

                    showSameVersion();
                    mChongmooDialog.dismiss();

                }

            });

        } else {

            showSameVersion();
            showNetworkProblemDialog(true);

        }

    }

    /**
     * 앱에 버젼과 API에서 가져온 버젼이 일치 할 경우에 화면에 그리는 로직
     */
    private void showSameVersion() {

        String versionNumber = getApplicationVersionNumber();
        mVersUpgrEq.setVisibility(View.GONE);
        mVersUpgrNeq.setVisibility(View.VISIBLE);
        mVersLastTxt.setText(String.format(getString(R.string.activity_version_current_version), versionNumber));
        mVersLastestTxt.setText(String.format("V%s", versionNumber));
        mVersLastestTxt.setTextColor(Color.BLACK);
        mLinearLayoutVersBtmLayoutUpgrEq.setVisibility(View.VISIBLE);
        mLinearLayoutVersBtmLayoutUpgrNeq.setVisibility(View.GONE);
    }

}