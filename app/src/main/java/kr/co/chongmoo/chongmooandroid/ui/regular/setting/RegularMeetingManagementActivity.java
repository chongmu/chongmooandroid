package kr.co.chongmoo.chongmooandroid.ui.regular.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import com.woojinsoft.chongmoo.android.R;

import io.realm.RealmResults;
import io.realm.Sort;
import kr.co.chongmoo.chongmooandroid.model.ClosingAccount;
import kr.co.chongmoo.chongmooandroid.model.Exemption;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.MonthlyFee;
import kr.co.chongmoo.chongmooandroid.model.PaymentStatus;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.ui.common.MoneyCommaEditTextWather;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;

/**
 *
 * 정규모임 수정 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class RegularMeetingManagementActivity extends BaseSubActivity implements View.OnClickListener {

    public static final String EXTRA_MODE = "mode";
    public static final int MODE_WRITE = 1;
    public static final int MODE_READ = 2;
    public static final int MODE_UPDATE = 3;

    private EditText mEditName;
    private EditText mEditMonthlyFee;
    private LinearLayout mLayoutStartDate;
    private TextView mTextStartDate;
    private Button mBtnCalendar;

    private LinearLayout mLayoutShare;
    private EditText mEditAccountHolder;
    private Button mBtnBank;
    private EditText mEditAccountNumber;
    private LinearLayout mLayoutDelete;
    private LinearLayout mLayoutPrev;
    private LinearLayout mLayoutOk;
    private TextView mTextOk;

    private Realm mRealm;

    private int mMode = MODE_READ;
    private int mSeq;

    private Date mStartDate;

    private RegularMeeting mRegularMeeting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regular_meeting_management);
        setTitle(R.string.activity_regular_meeting_management_title);

        mEditName = (EditText) findViewById(R.id.edit_name);
        mEditMonthlyFee = (EditText) findViewById(R.id.edit_monthly_fee);
        mLayoutStartDate = (LinearLayout) findViewById(R.id.layout_start_date);
        mTextStartDate = (TextView) findViewById(R.id.text_start_date);
        mBtnCalendar = (Button) findViewById(R.id.btn_calendar);
        mLayoutShare = (LinearLayout) findViewById(R.id.layout_share);
        mEditAccountHolder = (EditText) findViewById(R.id.edit_account_holder);
        mBtnBank = (Button) findViewById(R.id.btn_bank);
        mEditAccountNumber = (EditText) findViewById(R.id.edit_account_number);
        mLayoutDelete = (LinearLayout) findViewById(R.id.layout_delete);
        mLayoutPrev = (LinearLayout) findViewById(R.id.layout_prev);
        mLayoutOk = (LinearLayout) findViewById(R.id.layout_ok);
        mTextOk = (TextView) findViewById(R.id.text_ok);

        mTextStartDate.setOnClickListener(this);
        mBtnCalendar.setOnClickListener(this);
        mLayoutShare.setOnClickListener(this);
        mBtnBank.setOnClickListener(this);
        mLayoutDelete.setOnClickListener(this);
        mLayoutPrev.setOnClickListener(this);
        mLayoutOk.setOnClickListener(this);

        mEditMonthlyFee.addTextChangedListener(new MoneyCommaEditTextWather(mEditMonthlyFee));

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            mMode = bundle.getInt(EXTRA_MODE);
            mSeq = bundle.getInt("MEETING_SEQ", 0);
        }

        mStartDate = new Date();
        setTextDate(mTextStartDate, mStartDate, true);

        changeMode(mMode);

        mRealm = Realm.getDefaultInstance();

        if(mSeq != 0) {
            mRealm = Realm.getDefaultInstance();
            RealmQuery<RegularMeeting> query = mRealm.where(RegularMeeting.class);
            mRegularMeeting = query.equalTo("seq", mSeq).findFirst();

            refreshUi();
        }
    }

    private void refreshUi(){
        mStartDate = mRegularMeeting.getStartdate();
        mEditName.setText(mRegularMeeting.getName());

        RealmList<MonthlyFee> monthlyFeeList = mRegularMeeting.getMonthlyFeeList();
        MonthlyFee monthlyFee = monthlyFeeList.get(monthlyFeeList.size()-1);
        mEditMonthlyFee.setText(String.format("%,d",monthlyFee.getAmount()));

        setTextDate(mTextStartDate, mStartDate, true);
        mEditAccountHolder.setText(mRegularMeeting.getAccountHolder());
        mBtnBank.setText(mRegularMeeting.getBank());
        mEditAccountNumber.setText(mRegularMeeting.getAccountNumber());
    }

    private void changeMode(int mode){
        boolean isReadMode = (mode == MODE_READ);
        mEditName.setEnabled(isReadMode ? false : true);
        mEditMonthlyFee.setEnabled(isReadMode ? false : true);
        mLayoutStartDate.setActivated(isReadMode);
        mTextStartDate.setEnabled(isReadMode ? false : true);
        mBtnCalendar.setVisibility(isReadMode ? View.GONE : View.VISIBLE);
        mLayoutShare.setVisibility(isReadMode ? View.VISIBLE : View.GONE);
        mEditAccountHolder.setEnabled(isReadMode ? false : true);
        mBtnBank.setEnabled(isReadMode ? false : true);
        mEditAccountNumber.setEnabled(isReadMode ? false : true);
        mLayoutDelete.setVisibility(isReadMode ? View.VISIBLE : View.GONE);
        mLayoutPrev.setVisibility(isReadMode ? View.GONE : View.VISIBLE);
        mTextOk.setText(isReadMode ? R.string.modify : R.string.confirm);

        hideKeyboard();
    }

    private void setTextDate(TextView textView, Date date, boolean modeMonth){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        textView.setText(cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + (modeMonth ? "" : "-" + cal.get(Calendar.DAY_OF_MONTH)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.text_start_date:
            case R.id.btn_calendar: {
                final ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_CALRENDAR_MONTH);
                dialog.setTitle(getString(R.string.dialog_calendar_text));
                dialog.setDate(mStartDate);
                dialog.setPositiveButton(getString(R.string.dialog_calendar_positive_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mStartDate = dialog.getDate();
                        setTextDate(mTextStartDate, mStartDate, true);
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(getString(R.string.dialog_calendar_negative_text), null);
                dialog.show();
                break;
            }
            case R.id.layout_share: {
                String name = mEditName.getText().toString();
                String accountHolder = mEditAccountHolder.getText().toString();
                String bank = mBtnBank.getText().toString();
                String accountNumber = mEditAccountNumber.getText().toString();

                StringBuilder builder = new StringBuilder();
                builder.append(String.format(getString(R.string.dialog_chongmoo_share_1), name) );
                builder.append(getString(R.string.dialog_chongmoo_share_2));
                builder.append(String.format(getString(R.string.dialog_chongmoo_share_3), accountHolder));
                builder.append(String.format(getString(R.string.dialog_chongmoo_share_4), bank));
                builder.append(String.format(getString(R.string.dialog_chongmoo_share_5), accountNumber));

                ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_SHARE);
                dialog.setTitle(getString(R.string.dialog_chongmoo_share_title));
                dialog.setShareMessage(builder.toString());
                dialog.show();
                break;
            }
            case R.id.btn_bank: {
                final String[] bankArray = getResources().getStringArray(R.array.bank_list);
                ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_LIST);
                dialog.setList(bankArray, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mBtnBank.setText(bankArray[which]);
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            }
            case R.id.layout_delete: {
                ChongmooDialog dialog = new ChongmooDialog(this);
                dialog.setMessage(getString(R.string.dialog_chongmoo_regular_delete_message));
                dialog.setPositiveButton(getString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                RealmQuery<Income> incomeQuery = realm.where(Income.class);
                                incomeQuery.equalTo("meetingSeq", mSeq).findAll().deleteAllFromRealm();

                                RealmQuery<Member> memberQuery = realm.where(Member.class);
                                memberQuery.equalTo("meetingSeq", mSeq).findAll().deleteAllFromRealm();

                                RealmQuery<ClosingAccount> closingAccountQuery = realm.where(ClosingAccount.class);
                                closingAccountQuery.equalTo("meetingSeq", mSeq).findAll().deleteAllFromRealm();

                                RealmQuery<RegularMeeting> query = realm.where(RegularMeeting.class);
                                RegularMeeting regularMeeting = query.equalTo("seq", mSeq).findFirst();
                                regularMeeting.removeFromRealm();
                            }
                        }, new Realm.Transaction.Callback() {
                            @Override
                            public void onSuccess() {
                                goHome();
                            }

                            @Override
                            public void onError(Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getBaseContext(), String.format(getString(R.string.dialog_chongmoo_regular_delete_fail), e.toString()), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
                dialog.setNegativeButton(getString(R.string.cancel), null);
                dialog.show();
                break;
            }
            case R.id.layout_prev:
                onBackPressed();
                break;
            case R.id.layout_ok:
                if(mMode == MODE_READ){
                    mMode = MODE_UPDATE;
                    changeMode(mMode);
                }
                else if(mMode == MODE_UPDATE){
                    String name = mEditName.getText().toString();
                    String monthlyFeeStr = mEditMonthlyFee.getText().toString().replaceAll(",", "").trim();
                    String accountHolder = mEditAccountHolder.getText().toString();
                    String bank = mBtnBank.getText().toString();
                    String accountNumber = mEditAccountNumber.getText().toString();

                    if (TextUtils.isEmpty(name)) {
                        Toast.makeText(this, R.string.toast_require_meeting_name, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(monthlyFeeStr)) {
                        Toast.makeText(this, R.string.toast_require_fee, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    mRealm.beginTransaction();

                    RealmQuery<RegularMeeting> meetingRealmQuery = mRealm.where(RegularMeeting.class);
                    RegularMeeting regularMeeting = meetingRealmQuery.equalTo("seq", mSeq).findFirst();
                    RealmList<MonthlyFee> monthlyFeeList = regularMeeting.getMonthlyFeeList();

                    mRegularMeeting.setName(name);

                    MonthlyFee latestMonthlyFee = monthlyFeeList.get(monthlyFeeList.size() - 1);

                    int monthlyFee = Integer.parseInt(monthlyFeeStr);
                    String currentStr = DateUtil.getDateToYearMonthString(mStartDate);
                    int current = Integer.parseInt(currentStr);
                    int latest = Integer.parseInt(latestMonthlyFee.getDate());
                    if(current != latest || monthlyFee != latestMonthlyFee.getAmount()) {

                        int feeIncomeCount = 0;
                        RealmQuery<Member> memberQuery = mRealm.where(Member.class);
                        RealmResults<Member> memberList = memberQuery.equalTo("meetingSeq", mSeq).findAll();
                        for (Member member : memberList) {
                            RealmQuery<Income> query = mRealm.where(Income.class);
                            RealmResults<Income> incomeList = query.equalTo("memberSeq", member.getSeq()).findAllSorted("date", Sort.ASCENDING);
                            feeIncomeCount += incomeList.size();

                            int selectedYear = DateUtil.getYear(mStartDate);
                            int selectedMonth = DateUtil.getMonth(mStartDate);

                            int startYear = DateUtil.getYear(member.getStartDate());
                            int startMonth = DateUtil.getMonth(member.getStartDate());

                            int outYear = DateUtil.getYear(member.getOutDate());
                            int outMonth = DateUtil.getMonth(member.getOutDate());

                            RealmList<Exemption> exemptionList = member.getExemptionList();
                            int status = PaymentStatus.STATUS_NOTHING;
                            boolean isModify = true;
                            if (startYear < selectedYear || (startYear == selectedYear && startMonth <= selectedMonth)) {
                                if (outYear > -1 && (outYear < selectedYear || (outYear == selectedYear && outMonth < selectedMonth))) {
                                    status = PaymentStatus.STATUS_NOTHING;
                                } else {
                                    status = PaymentStatus.STATUS_NOT_PAID;

                                    ArrayList<Income> list = new ArrayList<>();
                                    for (Income ic : incomeList) {
                                        list.add(new Income(ic));
                                    }

                                    int y = startYear;
                                    int m = startMonth;
                                    long fee = 0;
                                    loop:
                                    while (y < selectedYear || (y == selectedYear && m <= selectedMonth)) {
                                        for (Exemption exemption : exemptionList) {
                                            if (Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", y, m + 1))) {
                                                m++;
                                                if (m > 11) {
                                                    m %= 12;
                                                    y++;
                                                }
                                                continue loop;
                                            }
                                        }

                                        int size = monthlyFeeList.size();
                                        for (MonthlyFee mf : monthlyFeeList) {
                                            if (Integer.parseInt(mf.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m + 1))) {
                                                fee = mf.getAmount();
                                                size--;
                                            }
                                        }

                                        if (list.size() > 0) {
                                            Income currnetIncome = list.get(0);
                                            currnetIncome.setAmount(currnetIncome.getAmount() - fee);
                                            long result = currnetIncome.getAmount();
                                            if (result == 0)
                                                list.remove(0);

                                            status = PaymentStatus.STATUS_PAID;
                                            isModify = false;
                                        } else {
                                            status = PaymentStatus.STATUS_NOT_PAID;
                                            isModify = true;
                                            break;
                                        }

                                        m++;
                                        if (m > 11) {
                                            m %= 12;
                                            y++;
                                        }
                                    }
                                }
                            }

                            if (!isModify) {
                                Toast.makeText(RegularMeetingManagementActivity.this, R.string.toast_already_registered_fee_month, Toast.LENGTH_SHORT).show();
                                mEditMonthlyFee.setText(latestMonthlyFee.getAmount() + "");
                                mRealm.cancelTransaction();
                                return;
                            }
                        }

                        if(feeIncomeCount > 0) {

                            if (current == latest) {
                                latestMonthlyFee.setAmount(monthlyFee);
                            }
                            else if(monthlyFee == latestMonthlyFee.getAmount()){
                                Toast.makeText(RegularMeetingManagementActivity.this, R.string.toast_change_together, Toast.LENGTH_SHORT).show();
                                mStartDate = mRegularMeeting.getStartdate();
                                setTextDate(mTextStartDate, mStartDate, true);
                                mRealm.cancelTransaction();
                                return;
                            }
                            else if (current < latest) {
                                Toast.makeText(RegularMeetingManagementActivity.this, R.string.toast_lower_date, Toast.LENGTH_SHORT).show();
                                mEditMonthlyFee.setText(latestMonthlyFee.getAmount() + "");
                                mStartDate = mRegularMeeting.getStartdate();
                                setTextDate(mTextStartDate, mStartDate, true);
                                mRealm.cancelTransaction();
                                return;
                            }
                            else {
                                MonthlyFee newMonthlyFee = mRealm.createObject(MonthlyFee.class);
                                newMonthlyFee.setDate(currentStr);
                                newMonthlyFee.setAmount(monthlyFee);
                                monthlyFeeList.add(newMonthlyFee);
                            }
                        }
                        else{
                            latestMonthlyFee.setDate(currentStr);
                            latestMonthlyFee.setAmount(monthlyFee);

                            for (int i = 0; i < memberList.size(); i++) {
                                memberList.get(i).setStartDate(mStartDate);
                            }
                        }
                    }

                    mRegularMeeting.setStartdate(mStartDate);
                    mRegularMeeting.setAccountHolder(accountHolder);
                    mRegularMeeting.setBank(bank);
                    mRegularMeeting.setAccountNumber(accountNumber);
                    mRegularMeeting.setModifyDate(new Date());

                    mRealm.commitTransaction();

                    Toast.makeText(getBaseContext(), "수정되었습니다", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent();
                    intent.putExtra("name", name);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                else{
                    final String name = mEditName.getText().toString();
                    final String monthlyFeeStr = mEditMonthlyFee.getText().toString().replaceAll(",", "");
                    final String accountHolder = mEditAccountHolder.getText().toString();
                    final String bank = mBtnBank.getText().toString();
                    final String accountNumber = mEditAccountNumber.getText().toString();

                    if (TextUtils.isEmpty(name)) {
                        Toast.makeText(this, R.string.toast_require_meeting_name, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(monthlyFeeStr)) {
                        Toast.makeText(this, R.string.toast_require_fee, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    final int monthlyFee = Integer.parseInt(monthlyFeeStr);

                    mRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RegularMeeting regularMeeting = realm.createObject(RegularMeeting.class);
                            regularMeeting.setSeq(RegularMeeting.generateSequence(realm));
                            regularMeeting.setName(name);

                            RealmList<MonthlyFee> monthlyFeeList = new RealmList<>();
                            MonthlyFee newMonthlyFee = realm.createObject(MonthlyFee.class);
                            newMonthlyFee.setDate(DateUtil.getDateToYearMonthString(mStartDate));
                            newMonthlyFee.setAmount(monthlyFee);
                            monthlyFeeList.add(newMonthlyFee);
                            regularMeeting.setMonthlyFeeList(monthlyFeeList);

                            regularMeeting.setStartdate(mStartDate);
                            regularMeeting.setAccountHolder(accountHolder);
                            regularMeeting.setBank(bank);
                            regularMeeting.setAccountNumber(accountNumber);
                            regularMeeting.setRegistDate(new Date());
                            regularMeeting.setModifyDate(new Date());
                        }
                    }, new Realm.Transaction.Callback() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(getBaseContext(), R.string.toast_regist_success, Toast.LENGTH_SHORT).show();
                            goHome();
                        }

                        @Override
                        public void onError(Exception e) {
                            Toast.makeText(getBaseContext(), String.format(getString(R.string.toast_regist_fail), e.toString()), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(mMode == MODE_UPDATE){
            mMode = MODE_READ;
            changeMode(mMode);
            refreshUi();
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }
}
