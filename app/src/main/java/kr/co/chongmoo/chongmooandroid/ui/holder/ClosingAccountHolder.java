package kr.co.chongmoo.chongmooandroid.ui.holder;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.woojinsoft.chongmoo.android.R;

import io.realm.Realm;
import io.realm.RealmQuery;
import kr.co.chongmoo.chongmooandroid.model.ClosingAccount;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.network.RetrofitManager;
import kr.co.chongmoo.chongmooandroid.ui.common.WebViewActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.ClosingAccountFragment;
import kr.co.chongmoo.chongmooandroid.ui.regular.RegularMeetingActivity;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.util.StringUtil;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 결산 목록 holder
 *
 * @author  Hoon Jang
 * @since   2016.04.06
 */
public class ClosingAccountHolder extends RecyclerView.ViewHolder{
    public static final int RESOURCE_ID = R.layout.recycler_item_closing_account;

    public TextView textPeriod;
    public TextView textRegistDate;
    public TextView btnDown;
    public Button btnShare;
    public Button btnDelete;

    public ClosingAccountHolder(View itemView) {
        super(itemView);

        textPeriod = (TextView) itemView.findViewById(R.id.text_period);
        textRegistDate = (TextView) itemView.findViewById(R.id.text_regist_date);
        btnDown = (Button) itemView.findViewById(R.id.btn_down);
        btnShare = (Button) itemView.findViewById(R.id.btn_share);
        btnDelete = (Button) itemView.findViewById(R.id.btn_delete);
    }

    public static class CommonCoordinator implements BeautifulRecyclerAdapter.Coordinator<ClosingAccountHolder, ClosingAccount>{
        private Activity mActivity;
        private Fragment mFragment;

        public CommonCoordinator(Activity activity, Fragment fragment) {
            this.mActivity = activity;
            this.mFragment = fragment;
        }

        @Override
        public void coordinate(BeautifulRecyclerAdapter<ClosingAccountHolder, ClosingAccount> adapter, ClosingAccountHolder holder, List<ClosingAccount> itemList, int position) {
            final ClosingAccount item = itemList.get(position);

            final int type = item.getType();
            final String typeStr =  type == ClosingAccount.TYPE_INCOME ? "월 결산" : "회비현황";

            final String period;
            final String url;
            final String downloadUrl;
            if(type == ClosingAccount.TYPE_INCOME){
                period = DateUtil.getDateToYearMonthDaySlashString(item.getStartDate()) + " ~ " + DateUtil.getDateToYearMonthDaySlashString(item.getEndDate());
                url = RetrofitManager.API_URL + "frt/gmh?key=" + item.getCode();
                downloadUrl = RetrofitManager.API_URL + "frt/share/getMonthySummary/pdf?key=" + item.getCode();
            }
            else{
                period = DateUtil.getDateToYearMonthSlashString(item.getStartDate()) + " 기준";
                url = RetrofitManager.API_URL + "frt/gfh?key=" + item.getCode();
                downloadUrl = RetrofitManager.API_URL + "frt/share/getFeePaymentSummary/pdf?key=" + item.getCode();
            }

            holder.textPeriod.setText(typeStr + " " + period);
            holder.textRegistDate.setText(DateUtil.getFileYyyymmddHHmm(item.getRegistDate()));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, WebViewActivity.class);
                    intent.putExtra(WebViewActivity.EXTRA_CATEGORY, typeStr);
                    intent.putExtra(WebViewActivity.EXTRA_CONTENT, url);
                    intent.putExtra(WebViewActivity.EXTRA_IS_URL, true);
                    mActivity.startActivity(intent);
                }
            });

            holder.btnDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int permissionCheck = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if(permissionCheck == PackageManager.PERMISSION_DENIED) { // 마시멜로우 버젼 관련 체크
                        new TedPermission(mActivity)
                            .setPermissionListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted() {
                                    Toast.makeText(mActivity, R.string.toast_authority_permit_txt, Toast.LENGTH_SHORT).show();

                                    Realm realm = ((ClosingAccountFragment) mFragment).getRealm();
                                    RealmQuery<RegularMeeting> query = realm.where(RegularMeeting.class);
                                    RegularMeeting regularMeeting = query.equalTo("seq", ((RegularMeetingActivity) mActivity).getSeq()).findFirst();

                                    String fileName = regularMeeting.getName();
                                    if (type == ClosingAccount.TYPE_INCOME) {
                                        fileName += "_월결산_" + DateUtil.getDateToYearMonthDayString(item.getStartDate()) + "~" + DateUtil.getDateToYearMonthDayString(item.getEndDate()) + ".pdf";
                                    } else {
                                        fileName += "_회비현황_" + DateUtil.getDateToYearMonthString(item.getStartDate()) + "_기준.pdf";
                                    }

                                    File downloadDir = new File(Environment.getExternalStorageDirectory() + File.separator + "chongmoo" + File.separator + "download");
                                    if (!downloadDir.exists()) {
                                        downloadDir.mkdirs();
                                    }

                                    File downloadFile = new File(downloadDir, fileName);

                                    Uri urlToDownload = Uri.parse(downloadUrl);
                                    DownloadManager.Request request = new DownloadManager.Request(urlToDownload);
                                    request.setTitle("총무의가계부");
                                    request.setDescription("ing...");
                                    request.setMimeType("application/pdf");
                                    request.setDestinationUri(Uri.fromFile(downloadFile));

                                    DownloadManager downloadManager = (DownloadManager) mActivity.getSystemService(Context.DOWNLOAD_SERVICE);
                                    downloadManager.enqueue(request);
                                    Toast.makeText(mActivity, "다운로드 시작", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                    Toast.makeText(mActivity, mActivity.getString(R.string.toast_authority_deny_txt) + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setDeniedMessage(mActivity.getString(R.string.toast_casual_meeting_file_write_role_txt))
                            .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .check();
                        return ;
                    }

                    Realm realm = ((ClosingAccountFragment)mFragment).getRealm();
                    RealmQuery<RegularMeeting> query = realm.where(RegularMeeting.class);
                    RegularMeeting regularMeeting = query.equalTo("seq", ((RegularMeetingActivity)mActivity).getSeq()).findFirst();

                    String fileName = regularMeeting.getName();
                    if(type == ClosingAccount.TYPE_INCOME){
                        fileName += "_월결산_" + DateUtil.getDateToYearMonthDayString(item.getStartDate()) + "~" + DateUtil.getDateToYearMonthDayString(item.getEndDate()) + ".pdf";
                    }
                    else{
                        fileName += "_회비현황_" + DateUtil.getDateToYearMonthString(item.getStartDate()) + "_기준.pdf";
                    }

                    File downloadDir = new File(Environment.getExternalStorageDirectory() + File.separator + "chongmoo" + File.separator + "download");
                    if(!downloadDir.exists()){
                        downloadDir.mkdirs();
                    }

                    File downloadFile = new File(downloadDir, fileName);

                    Uri urlToDownload = Uri.parse(downloadUrl);
                    DownloadManager.Request request = new DownloadManager.Request(urlToDownload);
                    request.setTitle("총무의가계부");
                    request.setDescription("ing...");
                    request.setMimeType("application/pdf");
                    request.setDestinationUri(Uri.fromFile(downloadFile));

                    DownloadManager downloadManager = (DownloadManager)mActivity.getSystemService(Context.DOWNLOAD_SERVICE);
                    downloadManager.enqueue(request);
                    Toast.makeText(mActivity, "다운로드 시작", Toast.LENGTH_SHORT).show();
                }
            });

            holder.btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Realm realm = ((ClosingAccountFragment)mFragment).getRealm();
                    RealmQuery<RegularMeeting> query = realm.where(RegularMeeting.class);
                    RegularMeeting regularMeeting = query.equalTo("seq", ((RegularMeetingActivity)mActivity).getSeq()).findFirst();

                    String message =
                            "[" + regularMeeting.getName() + "] " + typeStr + "_" + period + " 내역 을 공유하였습니다.\n" +
//                            "미납회비가 있는 회원은 아래의 계 좌로 입금해 주시기 바랍니다.\n"+
//                            "회비 납부계좌\n" +
//                            "예금주 : " + regularMeeting.getAccountHolder() + "\n" +
//                            "은행 : " + regularMeeting.getBank() + "\n" +
//                            "계좌번호 : " + regularMeeting.getAccountNumber() + "\n" +
                            "아래의 링크를 눌러 내역을 확인하세요.\n" +
                            url;

                    ChongmooDialog dialog = new ChongmooDialog(mActivity, ChongmooDialog.MODE_SHARE);
                    dialog.setTitle("공유방법 선택");
                    dialog.setShareMessage(message);
                    dialog.show();
                }
            });

            holder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final ChongmooDialog dialog = new ChongmooDialog(mActivity);
                    dialog.setMessage("삭제하시겠습니까?");
                    dialog.setPositiveButton("삭제", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Realm realm = ((ClosingAccountFragment)mFragment).getRealm();

                            realm.beginTransaction();
                            RealmQuery<ClosingAccount> query = realm.where(ClosingAccount.class);
                            ClosingAccount closingAccount = query.equalTo("code", item.getCode()).findFirst();
                            closingAccount.removeFromRealm();
                            realm.commitTransaction();

                            ((ClosingAccountFragment)mFragment).refreshUi();
                            dialog.dismiss();
                        }
                    });
                    dialog.setNegativeButton("취소", null);
                    dialog.show();
                }
            });


        }
    }

}
