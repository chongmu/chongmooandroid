package kr.co.chongmoo.chongmooandroid.model.wrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import kr.co.chongmoo.chongmooandroid.model.MonthlyFee;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;

/**
 *
 * RegularMeeting Wrapper
 *
 * @author  Hoon Jang
 * @since   2016.04.30
 */
public class RegularMeetingWrapper {

    private int seq;

    private String name;
    private long startdate;

    private String bank;
    private String accountHolder;
    private String accountNumber;

    private List<MonthlyFeeWrapper> monthlyFeeList;

    private Date registDate;
    private Date modifyDate;

    public RegularMeetingWrapper(RegularMeeting original) {
        this.seq = original.getSeq();
        this.name = original.getName();
        this.startdate = original.getStartdate() != null ? original.getStartdate().getTime() : 0;
        this.bank = original.getBank();
        this.accountHolder = original.getAccountHolder();
        this.accountNumber = original.getAccountNumber();

        ArrayList<MonthlyFeeWrapper> monthlyFeeWrapperList = new ArrayList<>();
        RealmList<MonthlyFee> monthlyFeeList = original.getMonthlyFeeList();
        for(MonthlyFee monthlyFee : monthlyFeeList){
            monthlyFeeWrapperList.add(new MonthlyFeeWrapper(monthlyFee));
        }
        this.monthlyFeeList = monthlyFeeWrapperList;

        this.registDate = original.getRegistDate();
        this.modifyDate = original.getModifyDate();
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStartdate() {
        return startdate;
    }

    public void setStartdate(long startdate) {
        this.startdate = startdate;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public List<MonthlyFeeWrapper> getMonthlyFeeList() {
        return monthlyFeeList;
    }

    public void setMonthlyFeeList(List<MonthlyFeeWrapper> monthlyFeeList) {
        this.monthlyFeeList = monthlyFeeList;
    }

    public Date getRegistDate() {
        return registDate;
    }

    public void setRegistDate(Date registDate) {
        this.registDate = registDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }
}
