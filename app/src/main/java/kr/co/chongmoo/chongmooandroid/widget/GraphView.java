package kr.co.chongmoo.chongmooandroid.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import kr.co.chongmoo.chongmooandroid.util.UiUtils;

/**
 *
 * 수입/지출 화면 그래프
 *
 * @author  Hoon Jang
 * @since   2016.04.30
 */
public class GraphView extends View{

    private long mTotalIncome;
    private long mTotalOutome;

    public GraphView(Context context) {
        super(context);
    }

    public GraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GraphView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.parseColor("#FFF9F9F9"));

        int strokeWidth = UiUtils.getPixelFromDp(getContext(), 24);

        Paint paintRing1 = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintRing1.setColor(Color.parseColor("#FFF5606B"));
        paintRing1.setStrokeWidth(strokeWidth);
        paintRing1.setStyle(Paint.Style.STROKE);

        Paint paintRing2 = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintRing2.setColor(Color.parseColor("#FF1686DD"));
        paintRing2.setStrokeWidth(strokeWidth);
        paintRing2.setStyle(Paint.Style.STROKE);

        RectF ring = new RectF(strokeWidth/2, strokeWidth/2, getWidth() - strokeWidth/2, getHeight() - strokeWidth);
        if(mTotalIncome < 0){
            mTotalOutome -= mTotalIncome;
            mTotalIncome = 0;
        }

        long sum = mTotalIncome + mTotalOutome;
        int angle1 = 90;
        int angle2 = 90;
        if(sum > 0) {
            angle1 = (int) Math.round(180.0 * ((float) mTotalOutome / sum));
            angle2 = (int) Math.round(180.0 * ((float) mTotalIncome / sum));
        }
        else if(sum < 0){
            angle1 = 180;
            angle2 = 0;
        }

        canvas.drawArc(ring, 180, angle1 != 0 ? angle1 + 1 : angle1, false, paintRing1);
        canvas.drawArc(ring, 180 + angle1, angle2, false, paintRing2);
    }

    public long getTotalIncome() {
        return mTotalIncome;
    }

    public void setTotalIncome(long totalIncome) {
        this.mTotalIncome = totalIncome;
        invalidate();
    }

    public long getTotalOutome() {
        return mTotalOutome;
    }

    public void setTotalOutome(long totalOutome) {
        this.mTotalOutome = totalOutome;
        invalidate();
    }
}
