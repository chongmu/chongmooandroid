package kr.co.chongmoo.chongmooandroid.model;

/**
 * 번개 모임 참여 맴버 VO
 * Created by ryu on 2016-04-25.
 */
public class CasualMeetingMember {

    private String name;
    private int amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

}