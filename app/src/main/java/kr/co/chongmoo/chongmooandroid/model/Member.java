package kr.co.chongmoo.chongmooandroid.model;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

/**
 *
 * 멤버 정보 저장을 위한 모델
 *
 * @author  Hoon Jang
 * @since   2016.04.08
 */
@RealmClass
public class Member extends RealmObject {

    @PrimaryKey private int seq;

    private int meetingSeq;
    @Required private String name;
    @Required private Date registDate;
    @Required private Date startDate;

    private String memo;
    private Date outDate;

    private RealmList<Exemption> exemptionList;

    public Member() {}

    public Member(String name, Date registDate, Date outDate) {
        this.name = name;
        this.registDate = registDate;
        this.outDate = outDate;
    }

    public Member(int seq, String name, Date registDate, String memo) {
        this.seq = seq;
        this.name = name;
        this.registDate = registDate;
        this.memo = memo;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getMeetingSeq() {
        return meetingSeq;
    }

    public void setMeetingSeq(int meetingSeq) {
        this.meetingSeq = meetingSeq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRegistDate() {
        return registDate;
    }

    public void setRegistDate(Date registDate) {
        this.registDate = registDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Date getOutDate() {
        return outDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }

    public RealmList<Exemption> getExemptionList() {
        return exemptionList;
    }

    public void setExemptionList(RealmList<Exemption> exemptionList) {
        this.exemptionList = exemptionList;
    }

    public static int generateSequence(Realm realm){
        RealmQuery<Member> query = realm.where(Member.class);
        Number number = query.max("seq");
        int seq = (number == null) ? 0 : number.intValue();
        return seq + 1;
    }
}




