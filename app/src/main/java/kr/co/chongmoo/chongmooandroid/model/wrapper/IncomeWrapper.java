package kr.co.chongmoo.chongmooandroid.model.wrapper;

import kr.co.chongmoo.chongmooandroid.model.Income;

/**
 *
 * Income Wrapper
 *
 * @author  Hoon Jang
 * @since   2016.04.30
 */
public class IncomeWrapper {
    private int seq;
    private int meetingSeq;
    private long date;
    private int day;
    private int type;
    private long amount;
    private String category;
    private int memberSeq;
    private String content;
    private String memo;

    public IncomeWrapper(Income original) {
        this.seq = original.getSeq();
        this.meetingSeq = original.getMeetingSeq();
        this.date = original.getDate() != null ? original.getDate().getTime() : 0;
        this.day = original.getDay();
        this.type = original.getType();
        this.amount = original.getAmount();
        this.category = original.getCategory();
        this.memberSeq = original.getMemberSeq();
        this.content = original.getContent();
        this.memo = original.getMemo();
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getMeetingSeq() {
        return meetingSeq;
    }

    public void setMeetingSeq(int meetingSeq) {
        this.meetingSeq = meetingSeq;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getMemberSeq() {
        return memberSeq;
    }

    public void setMemberSeq(int memberSeq) {
        this.memberSeq = memberSeq;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
