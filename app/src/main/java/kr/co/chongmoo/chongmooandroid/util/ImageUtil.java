package kr.co.chongmoo.chongmooandroid.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 이미지 관련 유틸
 * Created by lunawyrd on 2016. 4. 24..
 */
public class ImageUtil {

    public static byte[] bitmapToByteArray( Bitmap bitmap ) {
        if(bitmap == null)
            return null;

        ByteArrayOutputStream stream = null;
        try {
            stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            return byteArray ;
        }finally {
            if(stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Bitmap byteArrayToBitmap( byte[] byteArray ) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length) ;
        return bitmap ;
    }
}
