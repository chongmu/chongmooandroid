package kr.co.chongmoo.chongmooandroid.common;

/**
 *
 * Constant
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class Constant {

    public static final boolean USE_GOOGLE_ANALYTICS = true;
    public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAokvGCX6WNU5UUXc0Oh0rfniW8Z3/E91fudmPuVHDmRJThTBIRVXjEQ4nMr0tD//+nULMxffPswQ90ajGptiQdMEHcukZvLWR6GweBJkXCy8RiWJ3PolU+X1Booh7FCNcjE5PMJeDTFy7lTLzNsL7kV5piZ0t2XtLgEgZQRvEuM5d0+Zc6HYRRO1snKij7hwDObYGuFtao3ye6B6K2Eo9ikwc1pAhNPbxxNyGGPVTM1KJzuMgWmG/Y066N4y34JLcbLpFPSvEZk2hqGHX7dMSBXMZ7HyiDGMm7otwRukYWDJ/0keuYCiviqQmhnxRNraUuAO2EEq7AbJm9SQHh5jHeQIDAQAB"; // (구글에서 발급받은 바이너리키를 입력해줍니다)


}
