package kr.co.chongmoo.chongmooandroid.ui;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.model.HttpResponse;
import kr.co.chongmoo.chongmooandroid.model.Notice;
import kr.co.chongmoo.chongmooandroid.network.RetrofitManager;
import kr.co.chongmoo.chongmooandroid.network.service.APIService;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.ui.holder.NoticeHolder;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * FAQ 엑티비티
 * FAQ 관련 내용을 서버에서 가져와서 화면에서 뷰잉이 가능하게 한다.
 */
public class FAQActivity extends BaseSubActivity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mLayoutRefresh;
    private RecyclerView mRecyclerNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        setTitle(getString(R.string.title_activity_faq));

        mLayoutRefresh = (SwipeRefreshLayout) findViewById(R.id.layouf_refresh);
        mLayoutRefresh.setOnRefreshListener(this);

        mRecyclerNotice = (RecyclerView) findViewById(R.id.recycler_faq);
        mRecyclerNotice.setLayoutManager(new LinearLayoutManager(this));

        if(isNetWorkAble()) {
            requestFaqList();
        } else {
            showNetworkProblemDialog(true);
        }
    }


    public void requestFaqList(){
        APIService apiService = RetrofitManager.getInstance().create(APIService.class);
        apiService.getFaqList().enqueue(new Callback<HttpResponse<List<Notice>>>() {
            @Override
            public void onResponse(Call<HttpResponse<List<Notice>>> call, Response<HttpResponse<List<Notice>>> response) {
                try {
                    int code = response.code();
                    if (code != 200)
                        throw new Exception("Error : " + code + " : " + response.message());

                    HttpResponse<List<Notice>> httpResponse = response.body();
                    String resultCode = httpResponse.getReturnCode();
                    if(!resultCode.equals("600"))
                        throw new Exception("Error : " + resultCode + " : " + httpResponse.getReturnMessage());

                    List<Notice> noticeList = httpResponse.getResults();
                    mRecyclerNotice.setAdapter(new BeautifulRecyclerAdapter<>(FAQActivity.this, NoticeHolder.RESOURCE_ID, noticeList, new NoticeHolder.CommonCoordinator(FAQActivity.this, "FAQ")));
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(FAQActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                mLayoutRefresh.setRefreshing(false);
            }
            @Override
            public void onFailure(Call<HttpResponse<List<Notice>>> call, Throwable t) {
                Toast.makeText(FAQActivity.this, "fail", Toast.LENGTH_SHORT).show();
                mLayoutRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        requestFaqList();
    }
}
