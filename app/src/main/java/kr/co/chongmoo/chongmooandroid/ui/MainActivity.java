package kr.co.chongmoo.chongmooandroid.ui;

import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import kr.co.chongmoo.chongmooandroid.common.Constant;
import kr.co.chongmoo.chongmooandroid.model.CasualMeeting;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.ui.casual.CasualMeetingActivity;
import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.ui.base.BaseActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.RegularMeetingActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.RegularMeetingSubscriptionActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.setting.RegularMeetingManagementActivity;
import kr.co.chongmoo.chongmooandroid.widget.VersionDialog;
import kr.lunawyrd.widget.adapter.MainRecyclerAdapter;

/**
 * 메인 화면 엑티비티
 */
public class MainActivity extends BaseActivity implements View.OnClickListener {

    private BroadcastReceiver mCompleteReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                Bundle data = intent.getExtras();
                long download_id = data.getLong(DownloadManager.EXTRA_DOWNLOAD_ID );

                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(download_id);

                Cursor c = ((DownloadManager)getSystemService(Context.DOWNLOAD_SERVICE)).query(query);
                if(c.moveToFirst()){
                    String localUri = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                    if(localUri == null) {
                        Toast.makeText(MainActivity.this, R.string.toast_download_fail, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    
                    String[] array = localUri.split("/");
                    String fileName = null;
                    try {
                        fileName = URLDecoder.decode(array[array.length - 1], "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    Intent showingIntent = new Intent(Intent.ACTION_VIEW);
                    showingIntent.setDataAndType(Uri.parse(localUri), "application/pdf");
                    showingIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent content = PendingIntent.getActivity(context, 0, showingIntent, 0);


                    Notification.Builder builder = new Notification.Builder(context)
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle(fileName)
                            .setContentText(getString(R.string.toast_download_success))
                            .setContentIntent(content)
                            .setWhen(System.currentTimeMillis());

                    Notification noti = null;
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                        noti = builder.build();
                    }
                    else{
                        noti = builder.getNotification();
                    }

                    noti.defaults |= Notification.DEFAULT_VIBRATE;
                    noti.flags |= Notification.FLAG_AUTO_CANCEL;

                    NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
                    nm.notify((int) System.currentTimeMillis(), noti);

                    Toast.makeText(MainActivity.this, R.string.toast_download_success, Toast.LENGTH_SHORT).show();
                }
//	            String localUrl = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/temp/" + mFileName; //저장했던 경로..
            }
        }
    };

    private TextView mTxtMainVersBtn;

    private ViewGroup mFaqLinLout;
    private ViewGroup mNotiLinLout;
    private ViewGroup mVersLinLout;

    private ViewGroup mCasualLinLout;
    private ViewGroup mRegularLinLout;

    private ViewGroup  mCasualLinLoutButtom;
    private ViewGroup  mRegularLinLoutButtom;

    private RecyclerView mRecyclerMain;

    private Realm mRealm;

    private boolean mIsFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTxtMainVersBtn = (TextView) findViewById(R.id.txt_main_ver_btn);
        mTxtMainVersBtn.setText(String.format(getString(R.string.txt_format_btn_main_version), getApplicationVersionNumber()));

        mFaqLinLout = (ViewGroup) findViewById(R.id.main_faq_linearLayout);
        mFaqLinLout.setOnClickListener(this);

        mNotiLinLout = (ViewGroup) findViewById(R.id.main_notice_linearLayout);
        mNotiLinLout.setOnClickListener(this);

        mVersLinLout = (ViewGroup) findViewById(R.id.main_version_linearLayout);
        mVersLinLout.setOnClickListener(this);

        mCasualLinLout = (ViewGroup) findViewById(R.id.btn_main_casual);
        mCasualLinLout.setOnClickListener(this);

        mRegularLinLout = (ViewGroup) findViewById(R.id.btn_main_regular);
        mRegularLinLout.setOnClickListener(this);

        mCasualLinLoutButtom = (ViewGroup) findViewById(R.id.btn_main_yes_data_bottom_regular);
        mCasualLinLoutButtom.setOnClickListener(this);

        mRegularLinLoutButtom = (ViewGroup) findViewById(R.id.btn_main_yes_data_bottom_casual);
        mRegularLinLoutButtom.setOnClickListener(this);

        mRealm = Realm.getDefaultInstance();

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            mIsFirst = bundle.getBoolean("IS_FIRST");
        }

        checkUpdateVersion();
        registerReceiver(mCompleteReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        int viewId = v.getId();
        switch (viewId) {
            case R.id.main_faq_linearLayout:
                intent= new Intent(MainActivity.this, FAQActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                break;
            case R.id.main_notice_linearLayout:
                Uri uri = Uri.parse("http://cafe.naver.com/smartchongmoo");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                startActivity(intent);
                break;
            case R.id.main_version_linearLayout:
                intent = new Intent(MainActivity.this, VersionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                break;
            case R.id.btn_main_casual:
            case R.id.btn_main_yes_data_bottom_casual:
                intent = new Intent(this, CasualMeetingActivity.class);
                intent.putExtra(CasualMeetingActivity.USE_MODE_KEY, CasualMeetingActivity.USE_MODE_VALUE_WRIT);
                startActivity(intent);
                break;
            case R.id.btn_main_regular:
            case R.id.btn_main_yes_data_bottom_regular:
                if(!isNetWorkAble()){
                    showNetworkProblemDialog(false);
                    return;
                }

                if(isSubscription()){
                    intent = new Intent(this, RegularMeetingManagementActivity.class);
                    intent.putExtra(RegularMeetingManagementActivity.EXTRA_MODE, RegularMeetingManagementActivity.MODE_WRITE);
                    startActivity(intent);
                }
                else{
                    startActivity(new Intent(MainActivity.this, RegularMeetingSubscriptionActivity.class));
                }
                break;
            case R.id.txt_ver_dal_positive:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=" + getPackageName()));
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        showMeetingListArea();
        if(isNetWorkAble()) {
            InAppInit_U(Constant.base64EncodedPublicKey, true);
        }

    }

    @Override
    public void onPaymentChecked(boolean isSubscription) {
        super.onPaymentChecked(isSubscription);
        RealmQuery<RegularMeeting> query = mRealm.where(RegularMeeting.class);
        RealmResults<RegularMeeting> results = query.findAll();

        if(isSubscription()) {
            if (mIsFirst) {
                mIsFirst = false;

                for (RegularMeeting regularMeeting : results) {
                    if (regularMeeting.isTop()) {
                        Intent intent = new Intent(MainActivity.this, RegularMeetingActivity.class);
                        intent.putExtra("NAME", regularMeeting.getName());
                        intent.putExtra("SEQ", regularMeeting.getSeq());
                        startActivity(intent);
                        break;
                    }
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mCompleteReceiver);
        mRealm.close();
    }

    public int getMemberCount(int seq){
        RealmQuery<Member> query = mRealm.where(Member.class);
        return query.equalTo("meetingSeq", seq).isNull("outDate").findAll().size();
    }

    /**
     * 메인에 미팅 목록 관련 영역을 그린다.
     */
    public void showMeetingListArea() {

        RealmQuery<RegularMeeting> query = mRealm.where(RegularMeeting.class);
        RealmResults<RegularMeeting> results = query.findAll();

        RealmQuery<CasualMeeting> casualMeetingQuery = mRealm.where(CasualMeeting.class);
        RealmResults<CasualMeeting> casualMeetingResults = casualMeetingQuery.findAll();

        List<Object> meetingList = new ArrayList<>();
        meetingList.addAll(results);
        meetingList.addAll(casualMeetingResults);

        if(meetingList.size() > 0) {

            findViewById(R.id.main_no_data_layout).setVisibility(View.GONE);
            findViewById(R.id.main_data_layout).setVisibility(View.VISIBLE);

            mRecyclerMain = (RecyclerView) findViewById(R.id.recycler_main);
            mRecyclerMain.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerMain.setAdapter(new MainRecyclerAdapter(MainActivity.this, meetingList));

        } else {

            findViewById(R.id.main_data_layout).setVisibility(View.GONE);
            findViewById(R.id.main_no_data_layout).setVisibility(View.VISIBLE);

        }

    }

    /**
     * 해당 되는 N빵 모임을 삭제한다.
     * @param seq
     */
    public void deleteCasualMeeting(final int seq) {

        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmQuery<CasualMeeting> query = realm.where(CasualMeeting.class);
                CasualMeeting casualMeeting = query.equalTo("seq", seq).findFirst();
                casualMeeting.removeFromRealm();
            }
        }, new Realm.Transaction.Callback() {
            @Override
            public void onSuccess() {
                Toast.makeText(MainActivity.this, getString(R.string.delete_casualmeeting_success), Toast.LENGTH_SHORT).show();
                goHome();
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, String.format(getString(R.string.delete_casualmeeting_fail), e.toString()), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /**
     * 버젼 정보에 따른 업데이트 팝업을 만든다.
     */
    private void checkUpdateVersion() {
        String paramVersion = getIntent().getStringExtra(IntroActivity.VERSION_KEY);
        String applicationVersionNumber = getApplicationVersionNumber();
        if(paramVersion != null && IntroActivity.VERSION_NUMBER_NONE.equals(paramVersion) == false && paramVersion.equals(applicationVersionNumber) == false) {
            paramVersion = paramVersion.replaceAll("\\.", "");
            int paramVersionInt = Integer.parseInt(paramVersion);
            applicationVersionNumber = applicationVersionNumber.replaceAll("\\.", "");
            int applicationVersionNumberInt = Integer.parseInt(applicationVersionNumber);
            if(paramVersionInt > applicationVersionNumberInt) {
                mIsFirst = false;
                VersionDialog versionDialog = new VersionDialog(this);
                versionDialog.setPositiveButtonText(getString(R.string.yes));
                if(Character.getNumericValue(applicationVersionNumber.charAt(0)) < Character.getNumericValue(paramVersion.charAt(0))) { // 메이저 버젼 업데이트 인지 체크
                    versionDialog.setDialogText(getString(R.string.dialog_major_version_text));
                    versionDialog.setPositiveButtonListener(this);
                    versionDialog.setNegativeButtonText(getString(R.string.no));
                    versionDialog.setNegativeButtonListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            moveTaskToBack(true);
                        }
                    });
                } else if(Character.getNumericValue(applicationVersionNumber.charAt(1)) < Character.getNumericValue(paramVersion.charAt(1))) { // 마이저 버젼 업데이트 인지 체크
                    versionDialog.setDialogText(getString(R.string.dialog_minor_version_text));
                    versionDialog.setPositiveButtonListener(this);
                    versionDialog.setNegativeButtonText(getString(R.string.later));
                } else if(Character.getNumericValue(applicationVersionNumber.charAt(2)) < Character.getNumericValue(paramVersion.charAt(2))) { // 마이저 버젼 업데이트 인지 체크
                    versionDialog.setDialogText(getString(R.string.dialog_minor_version_text));
                    versionDialog.setPositiveButtonListener(this);
                    versionDialog.setNegativeButtonText(getString(R.string.later));
                } else {

                }
                versionDialog.show();
            }
        }
        else {
            if(isNetWorkAble() == false) {
                showNetworkProblemDialog(false);
            }
        }
    }

    /**
     * 북마크 설정 및 취소 여부 관련 데이터를 설정한다. (
     * @param seq
     * @param isTop
     */
    public void updateBookMark(int seq, boolean isTop) {

        if(isTop == false) {

            RealmQuery<RegularMeeting> query = mRealm.where(RegularMeeting.class);
            RealmResults<RegularMeeting> results = query.findAll();

            List<RegularMeeting> regularMeetingList = new ArrayList<>(); // java.util.ConcurrentModificationException: No outside changes to a Realm is allowed while iterating a RealmResults. Use iterators methods instead. => 때문에 아래와 같이 로직 변경
            for (RegularMeeting regularMeetingClone : results) {
                regularMeetingList.add(regularMeetingClone);
            }
            mRealm.beginTransaction();
            for (RegularMeeting regularMeetingClone : regularMeetingList) {

                if (regularMeetingClone.getSeq() == seq) {
                    regularMeetingClone.setTop(true);
                } else {
                    regularMeetingClone.setTop(false);
                }

            }
            mRealm.commitTransaction();

        } else {

            RegularMeeting regularMeeting = mRealm.where(RegularMeeting.class).equalTo("seq", seq).findFirst();
            mRealm.beginTransaction();
            regularMeeting.setTop(false);
            mRealm.commitTransaction();

        }

    }

    public Realm getRealm() {
        return mRealm;
    }
}