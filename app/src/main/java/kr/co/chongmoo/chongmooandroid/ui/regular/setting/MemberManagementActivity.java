package kr.co.chongmoo.chongmooandroid.ui.regular.setting;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.ui.casual.AddressBookActivity;
import kr.co.chongmoo.chongmooandroid.ui.holder.MemberHolder;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 회원 목록 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class MemberManagementActivity extends BaseSubActivity implements View.OnClickListener {

    public static final int REQUEST_ADDRESS = 1;
    public static final int REQUEST_MEMBER_WRITE = 2;

    private LinearLayout mLayoutEmpty;

    private LinearLayout mLayoutMember;
    private TextView mTextCount;
    private EditText mEditName;
    private RecyclerView mRecyclerMember;

    private Realm mRealm;

    private int mMeetingSeq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_management);
        setTitle(R.string.activity_member_management_title);

        mLayoutEmpty = (LinearLayout) findViewById(R.id.layout_empty);
        mLayoutMember = (LinearLayout) findViewById(R.id.layout_member);
        mTextCount = (TextView) findViewById(R.id.text_count);
        mEditName = (EditText) findViewById(R.id.edit_name);
        Button btnSearch = (Button) findViewById(R.id.btn_search);
        mRecyclerMember = (RecyclerView) findViewById(R.id.recycler_member);
        LinearLayout layoutAddMember = (LinearLayout) findViewById(R.id.layout_add_member);

        btnSearch.setOnClickListener(this);
        layoutAddMember.setOnClickListener(this);

        Bundle b = getIntent().getExtras();
        if(b != null){
            mMeetingSeq = b.getInt("MEETING_SEQ");
        }

        mRealm = Realm.getDefaultInstance();
        loadMemberList("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case REQUEST_ADDRESS:
                if(resultCode == RESULT_OK){
                    final ArrayList<String> addressNameList = data.getExtras().getStringArrayList("ADDRESS_LIST");
                    if(addressNameList.size() < 1)
                        return ;

                    mRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmQuery<RegularMeeting> query = realm.where(RegularMeeting.class);
                            RegularMeeting regularMeeting = query.equalTo("seq", mMeetingSeq).findFirst();

                            for(String name : addressNameList) {
                                Member member = realm.createObject(Member.class);
                                member.setSeq(Member.generateSequence(realm));
                                member.setMeetingSeq(mMeetingSeq);
                                member.setName(name);
                                member.setRegistDate(new Date());
                                member.setStartDate(regularMeeting.getStartdate());
                            }
                        }
                    }, new Realm.Transaction.Callback() {
                        @Override
                        public void onSuccess() {
                            loadMemberList("");
                            Toast.makeText(getBaseContext(), R.string.toast_regist_success, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(Exception e) {
                            Toast.makeText(getBaseContext(), String.format(getString(R.string.toast_regist_fail), e.toString()), Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
                }
                break;
            case REQUEST_MEMBER_WRITE:
                if(resultCode == RESULT_OK)
                    loadMemberList("");
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_search:
                String name = mEditName.getText().toString();
                loadMemberList(name);
                hideKeyboard();
                break;
            case R.id.layout_add_member:
                hideKeyboard();

                int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
                if(permissionCheck == PackageManager.PERMISSION_DENIED) { // 마시멜로우 버젼 관련 체크
                    new TedPermission(this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                ChongmooDialog dialog = new ChongmooDialog(MemberManagementActivity.this, ChongmooDialog.MODE_LIST);
                                dialog.setList(new String[]{getString(R.string.dialog_chongmoo_list_1), getString(R.string.dialog_chongmoo_list_2)}, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case 0: {
                                                Intent intent = new Intent(getBaseContext(), AddressBookActivity.class);
                                                startActivityForResult(intent, REQUEST_ADDRESS);
                                                break;
                                            }
                                            case 1: {
                                                Intent intent = new Intent(getBaseContext(), MemberWriteActivity.class);
                                                startActivityForResult(intent, REQUEST_ADDRESS);
                                                break;
                                            }
                                        }
                                    }
                                });
                                dialog.show();
                            }

                            @Override
                            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                Toast.makeText(MemberManagementActivity.this, getString(R.string.toast_authority_deny_txt) + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setDeniedMessage(getString(R.string.toast_casual_meeting_file_write_role_txt))
                        .setPermissions(Manifest.permission.READ_CONTACTS)
                        .check();
                    return;
                }

                ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_LIST);
                dialog.setList(new String[]{getString(R.string.dialog_chongmoo_list_1), getString(R.string.dialog_chongmoo_list_2)}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0: {
                                Intent intent = new Intent(getBaseContext(), AddressBookActivity.class);
                                startActivityForResult(intent, REQUEST_ADDRESS);
                                break;
                            }
                            case 1: {
                                Intent intent = new Intent(getBaseContext(), MemberWriteActivity.class);
                                startActivityForResult(intent, REQUEST_ADDRESS);
                                break;
                            }
                        }
                    }
                });
                dialog.show();
                break;
        }
    }

    private void loadMemberList(String name){
        RealmQuery<Member> query = mRealm.where(Member.class);
        RealmResults<Member> result = query.equalTo("meetingSeq", mMeetingSeq).findAllSorted("seq", Sort.DESCENDING);
        if(name.equals("")){
            result = query.equalTo("meetingSeq", mMeetingSeq).isNull("outDate").findAllSorted("name", Sort.ASCENDING);
        }
        else{
            result = query.equalTo("meetingSeq", mMeetingSeq).contains("name", name).isNull("outDate").findAllSorted("name", Sort.ASCENDING);
        }

        if(result.size() == 0 && name.equals("")){
            mLayoutEmpty.setVisibility(View.VISIBLE);
            mLayoutMember.setVisibility(View.GONE);
        }
        else{
            mLayoutEmpty.setVisibility(View.GONE);
            mLayoutMember.setVisibility(View.VISIBLE);
            mTextCount.setText(String.format(getString(R.string.activity_member_management_total), result.size()));
            mRecyclerMember.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerMember.setAdapter(new BeautifulRecyclerAdapter(this, MemberHolder.RESOURCE_ID, result, new MemberHolder.CommonCoordinator(this)));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }
}
