package kr.co.chongmoo.chongmooandroid.ui.holder;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import com.woojinsoft.chongmoo.android.R;
import kr.co.chongmoo.chongmooandroid.model.AddressBook;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 * Created by ryu on 2015. 20. 4..
 */
public class AddressBookHolder extends RecyclerView.ViewHolder {

    public static final int RESOURCE_ID = R.layout.recycler_item_address_book;

    public TextView mTextName;
    public CheckBox mCheckBoxAddressBook;


    public AddressBookHolder(View itemView) {
        super(itemView);
        mTextName = (TextView)itemView.findViewById(R.id.address_book_name);
        mCheckBoxAddressBook = (CheckBox)itemView.findViewById(R.id.chd_address_book);
    }

    public static class CommonCoordinator implements BeautifulRecyclerAdapter.Coordinator<AddressBookHolder, AddressBook>{

        private Activity mActivity;

        public CommonCoordinator(Activity mActivity) {
            this.mActivity = mActivity;
        }

        @Override
        public void coordinate(BeautifulRecyclerAdapter<AddressBookHolder, AddressBook> adapter, final AddressBookHolder holder, final List<AddressBook> itemList, int position) {

            final AddressBook item = itemList.get(position);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    boolean isChecked = holder.mCheckBoxAddressBook.isChecked();
                    holder.mCheckBoxAddressBook.setChecked(!isChecked);
                    item.setIsChecked(!isChecked);
                    updateTextViewBtnAddrBokCnt(itemList);

                }
            });

            holder.mTextName.setText(item.getName());
            holder.mCheckBoxAddressBook.setChecked(item.getIsChecked());
            holder.mCheckBoxAddressBook.setClickable(false);

        }

        private void updateTextViewBtnAddrBokCnt(List<AddressBook> itemList) {

            int checkedCount = 0;
            for(AddressBook addressBook : itemList) {
                boolean isCheckedStatus = addressBook.getIsChecked();
                if(isCheckedStatus == true) {
                    checkedCount++;
                }
            }

            TextView textViewBtnAddrBokCnt = (TextView) mActivity.findViewById(R.id.btn_submit_address_count);
            textViewBtnAddrBokCnt.setText(new Integer(checkedCount).toString());

        }

    }

    public static class MemberCoordinator implements BeautifulRecyclerAdapter.Coordinator<AddressBookHolder, Member>{

        private Activity mActivity;

        public MemberCoordinator(Activity mActivity) {
            this.mActivity = mActivity;
        }

        @Override
        public void coordinate(BeautifulRecyclerAdapter<AddressBookHolder, Member> adapter, final AddressBookHolder holder, final List<Member> itemList, int position) {
            final Member item = itemList.get(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.putExtra("SEQ", item.getSeq());
                    intent.putExtra("NAME", item.getName());
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    mActivity.finish();
                }
            });

            holder.mTextName.setText(item.getName());
            holder.mCheckBoxAddressBook.setVisibility(View.GONE);
        }
    }
}
