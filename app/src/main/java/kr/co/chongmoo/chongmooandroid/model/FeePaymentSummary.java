package kr.co.chongmoo.chongmooandroid.model;

import java.util.List;

import kr.co.chongmoo.chongmooandroid.model.wrapper.IncomeWrapper;
import kr.co.chongmoo.chongmooandroid.model.wrapper.MemberWrapper;
import kr.co.chongmoo.chongmooandroid.model.wrapper.RegularMeetingWrapper;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;

/**
 *
 * 회비 결산을 위한 모델
 *
 * @author  Hoon Jang
 * @since   2016.05.01
 */
public class FeePaymentSummary {

    private String title;
    private String dateMonth;
    private String date;
    private long total;
    private String accountInfo;
    private RegularMeetingWrapper regularMeeting;
    private List<MemberWrapper> memberList;
    private List<IncomeWrapper> incomeList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateMonth() {
        return dateMonth;
    }

    public void setDateMonth(String dateMonth) {
        this.dateMonth = dateMonth;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public String getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(String accountInfo) {
        this.accountInfo = accountInfo;
    }

    public RegularMeetingWrapper getRegularMeeting() {
        return regularMeeting;
    }

    public void setRegularMeeting(RegularMeetingWrapper regularMeeting) {
        this.regularMeeting = regularMeeting;
    }

    public List<MemberWrapper> getMemberList() {
        return memberList;
    }

    public void setMemberList(List<MemberWrapper> memberList) {
        this.memberList = memberList;
    }

    public List<IncomeWrapper> getIncomeList() {
        return incomeList;
    }

    public void setIncomeList(List<IncomeWrapper> incomeList) {
        this.incomeList = incomeList;
    }
}
