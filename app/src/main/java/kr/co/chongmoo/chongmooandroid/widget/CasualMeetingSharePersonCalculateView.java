package kr.co.chongmoo.chongmooandroid.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.woojinsoft.chongmoo.android.R;

/**
 * 번개 모임 공유 개인당 비용 정산 영역 커스텀 뷰
 * Created by ??? on 2016-04-25.
 */
public class CasualMeetingSharePersonCalculateView extends LinearLayout {

    private TextView mTextViewTxtCslShareMeetingPersonName;
    private TextView mTextViewTxtCslShareMeetingPersonAmount;

    public CasualMeetingSharePersonCalculateView(Context context) {
        super(context);
        init();
    }

    public CasualMeetingSharePersonCalculateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CasualMeetingSharePersonCalculateView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CasualMeetingSharePersonCalculateView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {

        inflate(getContext(), R.layout.view_casual_meeting_share_person_calculate, this);

        mTextViewTxtCslShareMeetingPersonName = (TextView) findViewById(R.id.txt_csl_share_meeting_person_name);
        mTextViewTxtCslShareMeetingPersonAmount = (TextView) findViewById(R.id.txt_csl_share_meeting_person_amount);

    }

    public void setPersonName(String personName) {
        mTextViewTxtCslShareMeetingPersonName.setText(personName);
    }

    public void setPersonAmount(String personAmount) {
        mTextViewTxtCslShareMeetingPersonAmount.setText(personAmount);
    }

}