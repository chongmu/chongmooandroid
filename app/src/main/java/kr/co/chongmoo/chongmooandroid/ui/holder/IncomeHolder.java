package kr.co.chongmoo.chongmooandroid.ui.holder;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import com.woojinsoft.chongmoo.android.R;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.ui.common.ImageActivity;
import kr.co.chongmoo.chongmooandroid.ui.common.ImageCropActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.IncomeWriteActivity;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 수입/지출 목록 holder
 *
 * @author  Hoon Jang
 * @since   2016.04.06
 */
public class IncomeHolder extends RecyclerView.ViewHolder{
    public static final int RESOURCE_ID = R.layout.recycler_item_income;

    public RelativeLayout layoutTitle;
    public TextView textDate;

    public LinearLayout layoutContent;
    public TextView textName;
    public TextView textCategory;
    public Button btnAttach;
    public TextView textAmount;

    public IncomeHolder(View itemView) {
        super(itemView);

        layoutTitle = (RelativeLayout)itemView.findViewById(R.id.layout_title);
        textDate = (TextView) itemView.findViewById(R.id.text_date);

        layoutContent = (LinearLayout) itemView.findViewById(R.id.layout_content);
        textName = (TextView) itemView.findViewById(R.id.text_name);
        textCategory = (TextView) itemView.findViewById(R.id.text_category);
        btnAttach = (Button) itemView.findViewById(R.id.btn_attach);
        textAmount = (TextView) itemView.findViewById(R.id.text_amount);
    }

    public static class CommonCoordinator implements BeautifulRecyclerAdapter.Coordinator<IncomeHolder, Income>{
        private Activity mActivity;

        public CommonCoordinator(Activity mActivity) {
            this.mActivity = mActivity;
        }

        @Override
        public void coordinate(BeautifulRecyclerAdapter<IncomeHolder, Income> adapter, IncomeHolder holder, List<Income> itemList, int position) {
            final Income item = itemList.get(position);

            holder.layoutTitle.setVisibility(item.isTop() ? View.VISIBLE : View.GONE);
            holder.textDate.setText(DateUtil.getDateToYearMonthDayWeekString(item.getDate()));
            holder.layoutContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(item.getCategory().equals("전월이월"))
                        return;

                    Intent intent = new Intent(mActivity, IncomeWriteActivity.class);
                    intent.putExtra(IncomeWriteActivity.EXTRA_MODE, IncomeWriteActivity.MODE_READ);
                    intent.putExtra("SEQ", item.getSeq());
                    intent.putExtra("MEETING_SEQ", item.getMeetingSeq());
                    mActivity.startActivity(intent);
                }
            });
            holder.textName.setText(item.getContent());
            holder.textCategory.setText(item.getCategory());
            holder.btnAttach.setVisibility((item.getImageFile() != null && item.getImageFile().length > 0) ? View.VISIBLE : View.GONE);
            holder.btnAttach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, ImageActivity.class);
                    intent.putExtra("SEQ", item.getSeq());
                    mActivity.startActivity(intent);
                }
            });
            holder.textAmount.setText(String.format("%,d", item.getAmount()));
            holder.textAmount.setTextColor(item.getType() == 0 ? Color.parseColor("#FF1686DD") : Color.parseColor("#FFF5606B"));
        }
    }

}
