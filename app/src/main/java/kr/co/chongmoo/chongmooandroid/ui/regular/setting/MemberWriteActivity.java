package kr.co.chongmoo.chongmooandroid.ui.regular.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.woojinsoft.chongmoo.android.R;

import java.util.ArrayList;

import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.ui.casual.CasualMeetingActivity;
import kr.co.chongmoo.chongmooandroid.ui.holder.MemberHolder;
import kr.co.chongmoo.chongmooandroid.ui.holder.MemberWriteHolder;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 회원 작성 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class MemberWriteActivity extends BaseSubActivity implements View.OnClickListener {

    private EditText mEditName;
    private RecyclerView mRecyclerMember;
    private TextView mTextCount;

    private ArrayList<String> mNameList;
    private BeautifulRecyclerAdapter<MemberHolder, String> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_write);
        setTitle(R.string.activity_member_write_title);

        mEditName = (EditText) findViewById(R.id.edit_name);
        Button btnAdd = (Button) findViewById(R.id.btn_add);
        mRecyclerMember = (RecyclerView) findViewById(R.id.recycler_member);
        LinearLayout layoutRegist = (LinearLayout) findViewById(R.id.layout_regist);
        mTextCount = (TextView) findViewById(R.id.text_count);

        btnAdd.setOnClickListener(this);
        layoutRegist.setOnClickListener(this);

        mNameList = new ArrayList<>();

        mAdapter = new BeautifulRecyclerAdapter(this, MemberWriteHolder.RESOURCE_ID, mNameList, new MemberWriteHolder.CommonCoordinator(this));
        mRecyclerMember.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerMember.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_add:
                String name = mEditName.getText().toString().trim();
                if(TextUtils.isEmpty(name)){
                    Toast.makeText(MemberWriteActivity.this, R.string.toast_require_name, Toast.LENGTH_SHORT).show();
                    return ;
                }

                mNameList.add(0, name);
                mAdapter.notifyDataSetChanged();
                mEditName.setText("");
                updateCount();
                break;
            case R.id.layout_regist:
                if(mNameList.size() == 0){
                    Toast.makeText(MemberWriteActivity.this, R.string.toast_require_member, Toast.LENGTH_SHORT).show();
                    return ;
                }
                
                Intent intent = new Intent();
                intent.putStringArrayListExtra(CasualMeetingActivity.REQUEST_PARAM_ADDRESS_BOOK_ACTIVITY, mNameList);
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
    }

    public void updateCount(){
        mTextCount.setText(String.valueOf(mNameList.size()));
    }
}
