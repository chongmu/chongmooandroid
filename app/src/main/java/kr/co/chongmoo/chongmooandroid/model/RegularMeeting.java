package kr.co.chongmoo.chongmooandroid.model;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

/**
 *
 * 정기모임 정보를 위한 모델
 *
 * @author  Hoon Jang
 * @since   2016.04.13
 */
@RealmClass
public class RegularMeeting extends RealmObject {

    @PrimaryKey private int seq;

    @Required private String name;
    @Required private Date startdate;

    private String bank;
    private String accountHolder;
    private String accountNumber;

    private RealmList<MonthlyFee> monthlyFeeList;

    @Required private Date registDate;
    @Required private Date modifyDate;

    private boolean top;

    public boolean isTop() {
        return top;
    }

    public void setTop(boolean top) {
        this.top = top;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public RealmList<MonthlyFee> getMonthlyFeeList() {
        return monthlyFeeList;
    }

    public void setMonthlyFeeList(RealmList<MonthlyFee> monthlyFeeList) {
        this.monthlyFeeList = monthlyFeeList;
    }

    public Date getRegistDate() {
        return registDate;
    }

    public void setRegistDate(Date registDate) {
        this.registDate = registDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public static int generateSequence(Realm realm){
        RealmQuery<RegularMeeting> query = realm.where(RegularMeeting.class);
        Number number = query.max("seq");
        int seq = (number == null) ? 0 : number.intValue();
        return seq + 1;
    }
}
