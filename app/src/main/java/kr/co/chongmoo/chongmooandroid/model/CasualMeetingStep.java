package kr.co.chongmoo.chongmooandroid.model;

import org.parceler.Parcel;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

/**
 * 번개 모임 차수 관련 VO
 * Created by ??? on 2016-04-22.
 */
@RealmClass
public class CasualMeetingStep extends RealmObject {

    /** 차수명 */ @Required private String name;
    /** 모임 장소 */ @Required private String area;
    /** 소비 비용 */ @Required private String amount;
    /** 참여자 목로 */ private RealmList<AddressBook> addressBookList;

    public CasualMeetingStep() {
        addressBookList = new RealmList<AddressBook>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public RealmList<AddressBook> getAddressBookList() {
        return addressBookList;
    }

    public void setAddressBookList(RealmList<AddressBook> addressBookList) {
        this.addressBookList = addressBookList;
    }

}