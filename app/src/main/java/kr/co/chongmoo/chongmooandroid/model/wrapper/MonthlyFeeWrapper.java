package kr.co.chongmoo.chongmooandroid.model.wrapper;

import io.realm.annotations.Required;
import kr.co.chongmoo.chongmooandroid.model.MonthlyFee;

/**
 *
 * MonthlyFee Wrapper
 *
 * @author  Hoon Jang
 * @since   2016.04.30
 */
public class MonthlyFeeWrapper {

    private String date;
    private long amount;

    public MonthlyFeeWrapper(MonthlyFee original){
        this.date = original.getDate();
        this.amount = original.getAmount();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
