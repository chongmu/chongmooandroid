package kr.co.chongmoo.chongmooandroid.model;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

/**
 * 번개 모임 VO
 * Created by ??? on 2016-04-15.
 */
@RealmClass
public class CasualMeeting extends RealmObject {

    /** PK */ @PrimaryKey private int seq;
    /** 모임명 */ @Required private String name;
    /** 등록일 */ @Required private Date date;
    /** */ private String mount;
    /** 주소록 리스트 */ private RealmList<AddressBook> addressBookList;
    /** 모임 차수 리스트 */ private RealmList<CasualMeetingStep> casualMeetingStepList;

    /** 은행 명 */ private String bank;
    /** 계좌 소유자 명*/ private String accountHolder;
    /** 은행 계좌 */ private String accountNumber;
    /** 메모*/ private String memo;

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMount() {
        return mount;
    }

    public void setMount(String mount) {
        this.mount = mount;
    }

    public RealmList<AddressBook> getAddressBookList() {
        return addressBookList;
    }

    public void setAddressBookList(RealmList<AddressBook> addressBookList) {
        this.addressBookList = addressBookList;
    }

    public RealmList<CasualMeetingStep> getCasualMeetingStepList() {
        return casualMeetingStepList;
    }

    public void setCasualMeetingStepList(RealmList<CasualMeetingStep> casualMeetingStepList) {
        this.casualMeetingStepList = casualMeetingStepList;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public static int generateSequence(Realm realm){
        RealmQuery<CasualMeeting> query = realm.where(CasualMeeting.class);
        int seq = query.max("seq").intValue();
        return seq + 1;
    }

}