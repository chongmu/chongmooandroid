package kr.co.chongmoo.chongmooandroid.model;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

/**
 *
 * 면제처리를 위한 모델
 *
 * @author  Hoon Jang
 * @since   2016.04.25
 */
@RealmClass
public class Exemption extends RealmObject{

    @Required
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
