package kr.co.chongmoo.chongmooandroid.common;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.tsengvn.typekit.Typekit;
import com.woojinsoft.chongmoo.android.R;

import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 *
 * Chongmoo Application
 *
 * @author  Hoon Jang
 * @since   2016.04.20
 */
public class ChongmooApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(realmConfiguration);

        Typekit.getInstance()
            .addNormal(Typekit.createFromAsset(this, "font/NanumBarunGothic.ttf"))
            .addBold(Typekit.createFromAsset(this, "font/NanumBarunGothicBold.ttf"));
    }

    public enum TrackerName {
        APP_TRACKER,           // 앱 별로 트래킹
        GLOBAL_TRACKER,        // 모든 앱을 통틀어 트래킹
        ECOMMERCE_TRACKER,     // 아마 유료 결재 트래킹 개념 같음
        COMMON_TRACKER
    }

    private static final String PROPERTY_ID = "UA-77943674-1";

    private HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public synchronized Tracker getTracker(){
        return getTracker(TrackerName.APP_TRACKER);
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : analytics.newTracker(R.xml.common_tracker);
            mTrackers.put(trackerId, t);
        }
        return mTrackers.get(trackerId);
    }
}
