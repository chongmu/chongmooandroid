package kr.co.chongmoo.chongmooandroid.ui.regular;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;

import com.woojinsoft.chongmoo.android.R;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import kr.co.chongmoo.chongmooandroid.model.ClosingAccount;
import kr.co.chongmoo.chongmooandroid.ui.holder.ClosingAccountHolder;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 결산 fragment
 *
 * @author  Hoon Jang
 * @since   2016.04.06
 */
public class ClosingAccountFragment extends Fragment{

    private LinearLayout mLayoutEmpty;
    private RecyclerView mRecyclerClosingAccount;


    private Realm mRealm;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_closing_account, container, false);

        mLayoutEmpty = (LinearLayout)rootView.findViewById(R.id.layout_empty);
        mRecyclerClosingAccount = (RecyclerView)rootView.findViewById(R.id.recycler_closing_account);
        mRecyclerClosingAccount.setLayoutManager(new LinearLayoutManager(getContext()));

        LinearLayout layoutClosingAccount = (LinearLayout)rootView.findViewById(R.id.layout_closing_account);
        layoutClosingAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ClosingAccountActivity.class);
                intent.putExtra("seq", ((RegularMeetingActivity)getActivity()).getSeq());
                startActivity(intent);
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mRealm = Realm.getDefaultInstance();
        refreshUi();
    }

    public void refreshUi(){
        RealmQuery<ClosingAccount> query = mRealm.where(ClosingAccount.class);
        RealmResults<ClosingAccount> results = query.equalTo("meetingSeq", ((RegularMeetingActivity) getActivity()).getSeq()).findAllSorted("registDate", Sort.DESCENDING);
        mRecyclerClosingAccount.setAdapter(new BeautifulRecyclerAdapter<>(getContext(), ClosingAccountHolder.RESOURCE_ID, results, new ClosingAccountHolder.CommonCoordinator(getActivity(), this)));

        mLayoutEmpty.setVisibility(results.size() == 0 ? View.VISIBLE : View.GONE);
        mRecyclerClosingAccount.setVisibility(results.size() != 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onStop() {
        super.onStop();
        mRealm.close();
    }

    public Realm getRealm() {
        return mRealm;
    }
}
