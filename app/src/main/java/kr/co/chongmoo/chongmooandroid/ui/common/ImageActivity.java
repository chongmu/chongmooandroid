package kr.co.chongmoo.chongmooandroid.ui.common;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.woojinsoft.chongmoo.android.R;

import io.realm.Realm;
import io.realm.RealmQuery;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.util.ImageUtil;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 *
 * 이미지 뷰잉 액티비티
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class ImageActivity extends BaseSubActivity {

    private ImageView mImage;

    private Realm mRealm;

    private PhotoViewAttacher mAttacher;
    private Bitmap mPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        setTitle(R.string.activity_image_title);

        mImage = (ImageView) findViewById(R.id.image);

        mRealm = Realm.getDefaultInstance();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int seq = bundle.getInt("SEQ");
            RealmQuery<Income> query = mRealm.where(Income.class);
            Income income = query.equalTo("seq", seq).findFirst();
            if(income != null){
                mPicture = ImageUtil.byteArrayToBitmap(income.getImageFile());
                mImage.setImageBitmap(mPicture);
            }
        }

        mAttacher = new PhotoViewAttacher(mImage);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mPicture != null){
            mPicture.recycle();
            mPicture = null;
        }

        if(mRealm != null){
            mRealm.close();
        }
    }
}
