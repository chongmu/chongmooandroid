package kr.co.chongmoo.chongmooandroid.model.wrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.ExemptionMechanism;

import io.realm.RealmList;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import kr.co.chongmoo.chongmooandroid.model.Exemption;
import kr.co.chongmoo.chongmooandroid.model.Member;

/**
 *
 * Member Wrapper
 *
 * @author  Hoon Jang
 * @since   2016.04.30
 */
public class MemberWrapper {

    private int seq;

    private int meetingSeq;
    private String name;
    private long registDate;
    private long startDate;

    private String memo;
    private long outDate;

    private List<ExemptionWrapper> exemptionList;

    public MemberWrapper(Member original) {
        this.seq = original.getSeq();
        this.meetingSeq = original.getMeetingSeq();
        this.name = original.getName();
        this.registDate = original.getRegistDate() != null ? original.getRegistDate().getTime() : 0;
        this.startDate = original.getStartDate() != null ? original.getStartDate().getTime() : 0;
        this.memo = original.getMemo();
        this.outDate = original.getOutDate() != null ? original.getOutDate().getTime() : 0;

        ArrayList<ExemptionWrapper> exemptionWrapperList = new ArrayList<>();
        RealmList<Exemption> exemptionList = original.getExemptionList();
        if(exemptionList == null){
            exemptionWrapperList = null;
        }
        else {
            for (Exemption exemption : exemptionList) {
                exemptionWrapperList.add(new ExemptionWrapper(exemption));
            }
        }
        this.exemptionList = exemptionWrapperList;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getMeetingSeq() {
        return meetingSeq;
    }

    public void setMeetingSeq(int meetingSeq) {
        this.meetingSeq = meetingSeq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getRegistDate() {
        return registDate;
    }

    public void setRegistDate(long registDate) {
        this.registDate = registDate;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public long getOutDate() {
        return outDate;
    }

    public void setOutDate(long outDate) {
        this.outDate = outDate;
    }

    public List<ExemptionWrapper> getExemptionList() {
        return exemptionList;
    }

    public void setExemptionList(List<ExemptionWrapper> exemptionList) {
        this.exemptionList = exemptionList;
    }
}
