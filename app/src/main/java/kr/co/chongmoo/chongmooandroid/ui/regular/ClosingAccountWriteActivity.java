package kr.co.chongmoo.chongmooandroid.ui.regular;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.woojinsoft.chongmoo.android.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import kr.co.chongmoo.chongmooandroid.model.ClosingAccount;
import kr.co.chongmoo.chongmooandroid.model.Exemption;
import kr.co.chongmoo.chongmooandroid.model.FeePaymentSummary;
import kr.co.chongmoo.chongmooandroid.model.HttpResponse;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.MonthlyFee;
import kr.co.chongmoo.chongmooandroid.model.PaymentStatus;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.model.wrapper.IncomeWrapper;
import kr.co.chongmoo.chongmooandroid.model.wrapper.MemberWrapper;
import kr.co.chongmoo.chongmooandroid.model.wrapper.RegularMeetingWrapper;
import kr.co.chongmoo.chongmooandroid.network.RetrofitManager;
import kr.co.chongmoo.chongmooandroid.network.service.APIService;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 * 결산 작성 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class ClosingAccountWriteActivity extends BaseSubActivity implements View.OnClickListener {

    public static final String EXTRA_SEQ = "seq";
    public static final String EXTRA_MODE = "mode";

    public static final int MODE_SINGLE = 1;
    public static final int MODE_DOUBLE = 2;

    private TextView mTextExplanation;
    private TextView mTextTitleStartDate;
    private TextView mTextStartDate;
    private LinearLayout mLayoutEndDate;
    private TextView mTextEndDate;
    private View mViewLineEndDate;

    private int mSeq = 0;
    private int mMode = MODE_DOUBLE;

    private Date mStartDate;
    private Date mEndDate;

    private Realm mRealm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_closing_account_write);
        setTitle(R.string.activity_closing_account_write_title);

        mTextExplanation = (TextView) findViewById(R.id.text_explanation);
        mTextTitleStartDate = (TextView) findViewById(R.id.text_title_start_date);
        mTextStartDate = (TextView) findViewById(R.id.text_start_date);
        Button btnCalendarStart = (Button) findViewById(R.id.btn_calendar_start);
        mLayoutEndDate = (LinearLayout) findViewById(R.id.layout_end_date);
        mTextEndDate = (TextView) findViewById(R.id.text_end_date);
        Button btnCalendarEnd = (Button) findViewById(R.id.btn_calendar_end);
        mViewLineEndDate = findViewById(R.id.view_line_end_date);
        LinearLayout layoutCreateFile = (LinearLayout) findViewById(R.id.layout_create_file);

        mTextStartDate.setOnClickListener(this);
        btnCalendarStart.setOnClickListener(this);
        mTextEndDate.setOnClickListener(this);
        btnCalendarEnd.setOnClickListener(this);
        layoutCreateFile.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            mSeq = bundle.getInt(EXTRA_SEQ, 0);
            mMode = bundle.getInt(EXTRA_MODE, MODE_DOUBLE);
        }

        if(mMode == MODE_SINGLE){
            mTextExplanation.setText(R.string.activity_closing_account_write_explanation);
            mTextTitleStartDate.setText(R.string.activity_closing_account_write_start_date);
            mLayoutEndDate.setVisibility(View.GONE);
            mViewLineEndDate.setVisibility(View.GONE);
        }

        mStartDate = new Date();
        setTextDate(mTextStartDate, mStartDate, mMode == MODE_SINGLE, mMode == MODE_DOUBLE);


        mEndDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(mEndDate.getTime());
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        mEndDate.setTime(cal.getTimeInMillis());
        setTextDate(mTextEndDate, mEndDate, false, false);

        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.text_start_date:
            case R.id.btn_calendar_start: {
                final ChongmooDialog dialog = new ChongmooDialog(this, mMode == MODE_DOUBLE ? ChongmooDialog.MODE_CALRENDAR : ChongmooDialog.MODE_CALRENDAR_MONTH);
                dialog.setTitle(getString(R.string.dialog_calendar_text));
                dialog.setDate(mStartDate);
                dialog.setPositiveButton(getString(R.string.dialog_calendar_positive_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mStartDate = dialog.getDate();
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(mStartDate.getTime());
                        if(mMode ==MODE_SINGLE){
                            cal.set(Calendar.DAY_OF_MONTH, 1);
                        }
                        cal.set(Calendar.HOUR_OF_DAY, 0);
                        cal.set(Calendar.MINUTE, 0);
                        cal.set(Calendar.SECOND, 0);
                        cal.set(Calendar.MILLISECOND, 0);
                        mStartDate.setTime(cal.getTimeInMillis());
                        setTextDate(mTextStartDate, mStartDate, mMode == MODE_SINGLE, false);
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(getString(R.string.dialog_calendar_negative_text), null);
                dialog.show();
                break;
            }
            case R.id.text_end_date:
            case R.id.btn_calendar_end: {
                final ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_CALRENDAR);
                dialog.setTitle(getString(R.string.dialog_calendar_text));
                dialog.setDate(mEndDate);
                dialog.setPositiveButton(getString(R.string.dialog_calendar_positive_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEndDate = dialog.getDate();
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(mEndDate.getTime());
                        cal.set(Calendar.HOUR_OF_DAY, 23);
                        cal.set(Calendar.MINUTE, 59);
                        cal.set(Calendar.SECOND, 59);
                        cal.set(Calendar.MILLISECOND, 999);
                        mEndDate.setTime(cal.getTimeInMillis());
                        setTextDate(mTextEndDate, mEndDate, false, false);
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(getString(R.string.dialog_calendar_negative_text), null);
                dialog.show();
                break;
            }
            case R.id.layout_create_file:
                if(!isNetWorkAble()) {
                    showNetworkProblemDialog(false);
                    return ;
                }

                if(mMode == MODE_DOUBLE){
                    RealmQuery<Income> incomeQuery = mRealm.where(Income.class);
                    RealmResults<Income> incomeList = incomeQuery.equalTo("meetingSeq", mSeq).between("date", mStartDate, mEndDate).findAll();

                    ArrayList<IncomeWrapper> incomeWrapperList = new ArrayList<>();
                    long incomeTotal = 0;
                    long outcomeTotal = 0;
                    for(Income income : incomeList){
                        if(income.getType() == 0){
                            incomeTotal += income.getAmount();
                        }
                        else{
                            outcomeTotal += income.getAmount();
                        }
                        incomeWrapperList.add(new IncomeWrapper(income));
                    }

                    if(DateUtil.getDay(mStartDate) == 1){
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.YEAR, DateUtil.getYear(mStartDate));
                        cal.set(Calendar.MONTH, DateUtil.getMonth(mStartDate));
                        cal.set(Calendar.DAY_OF_MONTH, 1);
                        cal.set(Calendar.HOUR_OF_DAY, 0);
                        cal.set(Calendar.MINUTE, 0);
                        cal.set(Calendar.SECOND, 0);
                        cal.set(Calendar.MILLISECOND, 0);
                        Date startDate = new Date(cal.getTimeInMillis());

                        int change = 0;
                        incomeQuery = mRealm.where(Income.class);
                        RealmResults<Income> beforeIncomeList = incomeQuery.equalTo("meetingSeq", mSeq).lessThan("date", startDate).findAll();
                        for(Income income : beforeIncomeList){
                            if(income.getType() == 0){
                                change += income.getAmount();
                            }
                            else{
                                change -= income.getAmount();
                            }
                        }

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, DateUtil.getYear(startDate));
                        calendar.set(Calendar.MONTH, DateUtil.getMonth(startDate));
                        calendar.set(Calendar.DAY_OF_MONTH, 1);
                        calendar.set(Calendar.HOUR_OF_DAY, 0);
                        calendar.set(Calendar.MINUTE, 0);
                        calendar.set(Calendar.SECOND, 0);
                        calendar.set(Calendar.MILLISECOND, 0);

                        Income chargeIncome = new Income();
                        chargeIncome.setType(0);
                        chargeIncome.setDate(new Date(calendar.getTimeInMillis()));
                        chargeIncome.setDay(1);
                        chargeIncome.setAmount(change);
                        chargeIncome.setCategory(getString(R.string.activity_closing_account_write_month));
                        chargeIncome.setContent(getString(R.string.activity_closing_account_write_month));
                        incomeWrapperList.add(new IncomeWrapper(chargeIncome));
                        incomeTotal += change;
                    }


                    RealmQuery<RegularMeeting> regularMeetingQuery = mRealm.where(RegularMeeting.class);
                    RegularMeeting regularMeeting = regularMeetingQuery.equalTo("seq", mSeq).findFirst();
                    RealmList<MonthlyFee> monthlyFeeList = regularMeeting.getMonthlyFeeList();

                    RealmQuery<Member> memberQuery = mRealm.where(Member.class);
                    RealmResults<Member> memberList = memberQuery.equalTo("meetingSeq", mSeq).findAllSorted("registDate", Sort.ASCENDING);

                    long feeTotal = calculateTotal(memberList, monthlyFeeList, mEndDate);

                    final HashMap<String, Object> map = new HashMap<>();
                    map.put("title", regularMeeting.getName());
                    map.put("startDate", DateUtil.getDateToYearMonthDayDotString(mStartDate));
                    map.put("endDate", DateUtil.getDateToYearMonthDayDotString(mEndDate));
                    map.put("total", incomeTotal - outcomeTotal);
                    map.put("incomeTotal", incomeTotal);
                    map.put("outcomeTotal", outcomeTotal);
                    map.put("feeTotal", feeTotal);
                    map.put("accountInfo", String.format("%s/%s/%s", regularMeeting.getBank(), regularMeeting.getAccountNumber(), regularMeeting.getAccountHolder()));
                    map.put("incomeList", incomeWrapperList);

                    final String json = new Gson().toJson(map);
                    Log.d("TEST", json);

                    final ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_LOADING);
                    dialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            APIService apiService = RetrofitManager.getInstance().create(APIService.class);
                            apiService.addMonthySummary(map).enqueue(new Callback<HttpResponse<String>>() {
                                @Override
                                public void onResponse(Call<HttpResponse<String>> call, Response<HttpResponse<String>> response) {
                                    try {
                                        int code = response.code();
                                        if (code != 200) {
                                            throw new Exception("Error : " + code + " : " + response.message());
                                        }

                                        HttpResponse<String> httpResponse = response.body();
                                        String resultCode = httpResponse.getReturnCode();

                                        if (!resultCode.equals("600")) {
                                            throw new Exception("Error : " + resultCode + " : " + httpResponse.getReturnMessage());
                                        }

                                        String key = httpResponse.getResults();
                                        mRealm.beginTransaction();

                                        ClosingAccount closingAccount = mRealm.createObject(ClosingAccount.class);
                                        closingAccount.setSeq(ClosingAccount.generateSequence(mRealm));
                                        closingAccount.setCode(key);
                                        closingAccount.setMeetingSeq(mSeq);
                                        closingAccount.setType(ClosingAccount.TYPE_INCOME);
                                        closingAccount.setStartDate(mStartDate);
                                        closingAccount.setEndDate(mEndDate);
                                        closingAccount.setDataJson(json);
                                        closingAccount.setRegistDate(new Date());

                                        mRealm.commitTransaction();
                                        dialog.dismiss();
                                        setResult(RESULT_OK);
                                        finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        Toast.makeText(ClosingAccountWriteActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<HttpResponse<String>> call, Throwable t) {
                                    dialog.dismiss();
                                    Toast.makeText(ClosingAccountWriteActivity.this, "fail", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }, 1000);
                }
                else{
                    RealmQuery<RegularMeeting> regularMeetingQuery = mRealm.where(RegularMeeting.class);
                    RegularMeeting regularMeeting = regularMeetingQuery.equalTo("seq", mSeq).findFirst();
                    RealmList<MonthlyFee> monthlyFeeList = regularMeeting.getMonthlyFeeList();

                    RealmQuery<Member> memberQuery = mRealm.where(Member.class);
                    RealmResults<Member> memberList = memberQuery.equalTo("meetingSeq", mSeq).findAllSorted("name", Sort.ASCENDING);
                    memberList = memberList.sort("outDate", Sort.ASCENDING);

                    ArrayList<MemberWrapper> memberWrapperList = new ArrayList<>();
                    for(Member member : memberList){
                        memberWrapperList.add(new MemberWrapper(member));
                    }

                    long total = calculateTotal(memberList, monthlyFeeList, mStartDate);

                    RealmQuery<Income> incomeQuery = mRealm.where(Income.class);
                    RealmResults<Income> incomeList = incomeQuery.equalTo("meetingSeq", mSeq).findAll();

                    ArrayList<IncomeWrapper> incomeWrapperList = new ArrayList<>();
                    for(Income income : incomeList){
                        incomeWrapperList.add(new IncomeWrapper(income));
                    }

                    final FeePaymentSummary feePaymentSummary = new FeePaymentSummary();
                    feePaymentSummary.setTitle(regularMeeting.getName());
                    feePaymentSummary.setDateMonth(String.valueOf(DateUtil.getMonth(mStartDate) + 1));
                    feePaymentSummary.setDate(DateUtil.getDateToYearMonthDotString(mStartDate));
                    feePaymentSummary.setTotal(total);
                    feePaymentSummary.setAccountInfo(String.format("%s/%s/%s", regularMeeting.getBank(), regularMeeting.getAccountNumber(), regularMeeting.getAccountHolder()));
                    feePaymentSummary.setRegularMeeting(new RegularMeetingWrapper(regularMeeting));
                    feePaymentSummary.setMemberList(memberWrapperList);
                    feePaymentSummary.setIncomeList(incomeWrapperList);

                    final String json = new Gson().toJson(feePaymentSummary);
                    Log.d("TEST", json);


                    final ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_LOADING);
                    dialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            APIService apiService = RetrofitManager.getInstance().create(APIService.class);
                            apiService.addFeePaymentSummary(feePaymentSummary).enqueue(new Callback<HttpResponse<String>>() {
                                @Override
                                public void onResponse(Call<HttpResponse<String>> call, Response<HttpResponse<String>> response) {
                                    try {
                                        int code = response.code();
                                        if (code != 200) {
                                            throw new Exception("Error : " + code + " : " + response.message());
                                        }

                                        HttpResponse<String> httpResponse = response.body();
                                        String resultCode = httpResponse.getReturnCode();

                                        if (!resultCode.equals("600")) {
                                            throw new Exception("Error : " + resultCode + " : " + httpResponse.getReturnMessage());
                                        }

                                        String key = httpResponse.getResults();
                                        mRealm.beginTransaction();

                                        ClosingAccount closingAccount = mRealm.createObject(ClosingAccount.class);
                                        closingAccount.setSeq(ClosingAccount.generateSequence(mRealm));
                                        closingAccount.setCode(key);
                                        closingAccount.setMeetingSeq(mSeq);
                                        closingAccount.setType(ClosingAccount.TYPE_STATUS);
                                        closingAccount.setStartDate(mStartDate);
                                        closingAccount.setDataJson(json);
                                        closingAccount.setRegistDate(new Date());

                                        mRealm.commitTransaction();
                                        dialog.dismiss();
                                        setResult(RESULT_OK);
                                        finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        Toast.makeText(ClosingAccountWriteActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<HttpResponse<String>> call, Throwable t) {
                                    dialog.dismiss();
                                    Toast.makeText(ClosingAccountWriteActivity.this, "fail", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }, 1000);
                }
                break;
        }
    }

    private void setTextDate(TextView textView, Date date, boolean modeMonth, boolean isFirst){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        if(isFirst){
            cal.set(Calendar.DAY_OF_MONTH, 1);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            date.setTime(cal.getTimeInMillis());
        }
        textView.setText(cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + (modeMonth ? "" : "-" + cal.get(Calendar.DAY_OF_MONTH)));
    }

    private long calculateTotal(List<Member> memberList, List<MonthlyFee> monthlyFeeList, Date date){
        long total = 0;
        for(Member member : memberList) {
            if(member.getOutDate() != null)
                continue;

            RealmQuery<Income> query = mRealm.where(Income.class);
            RealmResults<Income> incomeList = query.equalTo("memberSeq", member.getSeq()).findAllSorted("date", Sort.ASCENDING);

            int selectedYear = DateUtil.getYear(date);
            int selectedMonth = DateUtil.getMonth(date);

            int startYear = DateUtil.getYear(member.getStartDate());
            int startMonth = DateUtil.getMonth(member.getStartDate());

            int outYear = DateUtil.getYear(member.getOutDate());
            int outMonth = DateUtil.getMonth(member.getOutDate());

            RealmList<Exemption> exemptionList = member.getExemptionList();
            int status = PaymentStatus.STATUS_NOTHING;
            if (startYear < selectedYear || (startYear == selectedYear && startMonth <= selectedMonth)) {
                if (outYear > -1 && (outYear < selectedYear || (outYear == selectedYear && outMonth < selectedMonth))) {
                    status = PaymentStatus.STATUS_NOTHING;
                } else {
                    status = PaymentStatus.STATUS_NOT_PAID;

                    ArrayList<Income> list = new ArrayList<>();
                    for (Income ic : incomeList) {
                        list.add(new Income(ic));
                    }

                    int y = startYear;
                    int m = startMonth;
                    long fee = 0;
                    loop:
                    while (y < selectedYear || (y == selectedYear && m <= selectedMonth)) {
                        for(Exemption exemption : exemptionList) {
                            if (Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", y, m+1))) {
                                m++;
                                if(m > 11){
                                    m %= 12;
                                    y++;
                                }
                                continue loop;
                            }
                        }

                        int size = monthlyFeeList.size();
                        for (MonthlyFee mf : monthlyFeeList) {
                            if (Integer.parseInt(mf.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m + 1))) {
                                fee = mf.getAmount();
                                size--;
                            }
                        }

                        if (list.size() > 0) {
                            Income currnetIncome = list.get(0);
                            currnetIncome.setAmount(currnetIncome.getAmount() - fee);
                            long result = currnetIncome.getAmount();
                            if (result == 0)
                                list.remove(0);
                        } else {
                            total += fee;
                        }

                        m++;
                        if (m > 11) {
                            m %= 12;
                            y++;
                        }
                    }
                }
            }
        }

        return total;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }
}
