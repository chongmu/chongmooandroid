package kr.co.chongmoo.chongmooandroid.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.woojinsoft.chongmoo.android.R;

/**
 * 번개 모임 맴버 추가 관련 커스텀 뷰
 * Created by ??? on 2016-04-22.
 */
public class CasualMeetingMemberView extends LinearLayout {

    private ImageView mImageViewBtnDelCaulMeb;
    private EditText  mEditTextTxtCaulMeb;
    private OnClickListener mOnClickListener;

    public CasualMeetingMemberView(Context context) {
        super(context);
        init();
    }

    public CasualMeetingMemberView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CasualMeetingMemberView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CasualMeetingMemberView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {

        inflate(getContext(), R.layout.view_casual_meeting_member, this);

        mImageViewBtnDelCaulMeb = (ImageView) findViewById(R.id.btn_del_caul_meb);
        mEditTextTxtCaulMeb = (EditText) findViewById(R.id.txt_caul_meb);

    }

    public void setMember(String name) {

        mEditTextTxtCaulMeb.setText(name);

    }

    public void setDelBtnOnClickListener(OnClickListener onClickListener) {

        mImageViewBtnDelCaulMeb.setOnClickListener(onClickListener);

    }

}