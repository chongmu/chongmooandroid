package kr.co.chongmoo.chongmooandroid.model;

/**
 * 앱버젼 관련 VO
 * @author ryu
 */
public class MobileVersion {
	
	/** 시퀀스 								*/
	private int seq;
	/** 버젼넘버 ( x,y.z 형태 ) 			*/
	private String versionNumber;
	/** 변경사항 							*/
	private String changeLog;
	/** 등록일  							*/
	private String registDate;
	/** 수정일  							*/
	private String modifyDate;
	/** 앱플랫폼종류 ( AND : 안드로이드 / IOS : 애플  ) */
	private String appType;
	/** 업그레이드유형 ( MAJOR / MINOR )  	*/
	private String updateType;
	
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getVersionNumber() {
		return versionNumber;
	}
	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}
	public String getChangeLog() {
		return changeLog;
	}
	public void setChangeLog(String changeLog) {
		this.changeLog = changeLog;
	}
	public String getRegistDate() {
		return registDate;
	}
	public void setRegistDate(String registDate) {
		this.registDate = registDate;
	}
	public String getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getAppType() {
		return appType;
	}
	public void setAppType(String appType) {
		this.appType = appType;
	}
	public String getUpdateType() {
		return updateType;
	}
	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}
	
}
