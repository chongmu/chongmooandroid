package kr.co.chongmoo.chongmooandroid.ui.regular;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;

/**
 *
 * 결산 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class ClosingAccountActivity extends BaseSubActivity implements View.OnClickListener {

    private int mSeq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_closing_account);
        setTitle(R.string.activity_closing_account_title);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            mSeq = bundle.getInt("seq", 0);
        }

        RelativeLayout layoutIncome = (RelativeLayout)findViewById(R.id.layout_income);
        RelativeLayout layoutPaymentStatus = (RelativeLayout)findViewById(R.id.layout_payment_status);

        layoutIncome.setOnClickListener(this);
        layoutPaymentStatus.setOnClickListener(this);

        if(isNetWorkAble() == false) {
            showNetworkProblemDialog(true);
            return ;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case 0:
                if(resultCode == RESULT_OK){
                    finish();
                }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_income: {
                Intent intent = new Intent(this, ClosingAccountWriteActivity.class);
                intent.putExtra(ClosingAccountWriteActivity.EXTRA_SEQ, mSeq);
                startActivityForResult(intent, 0);
                break;
            }
            case R.id.layout_payment_status: {
                Intent intent = new Intent(this, ClosingAccountWriteActivity.class);
                intent.putExtra(ClosingAccountWriteActivity.EXTRA_SEQ, mSeq);
                intent.putExtra(ClosingAccountWriteActivity.EXTRA_MODE, ClosingAccountWriteActivity.MODE_SINGLE);
                startActivityForResult(intent, 0);
                break;
            }
        }
    }
}
