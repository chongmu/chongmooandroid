package kr.co.chongmoo.chongmooandroid.ui.holder;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.woojinsoft.chongmoo.android.R;

import java.util.List;

import kr.co.chongmoo.chongmooandroid.model.AddressBook;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.ui.regular.setting.MemberWriteActivity;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 * Created by ryu on 2015. 20. 4..
 */
public class MemberWriteHolder extends RecyclerView.ViewHolder {

    public static final int RESOURCE_ID = R.layout.recycler_item_member_write;

    public TextView textName;
    public Button btnDel;

    public MemberWriteHolder(View itemView) {
        super(itemView);
        textName = (TextView)itemView.findViewById(R.id.text_name);
        btnDel = (Button)itemView.findViewById(R.id.btn_del);
    }

    public static class CommonCoordinator implements BeautifulRecyclerAdapter.Coordinator<MemberWriteHolder, String>{

        private Activity mActivity;

        public CommonCoordinator(Activity mActivity) {
            this.mActivity = mActivity;
        }

        @Override
        public void coordinate(final BeautifulRecyclerAdapter<MemberWriteHolder, String> adapter, final MemberWriteHolder holder, final List<String> itemList, final int position) {
            final String item = itemList.get(position);

            holder.textName.setText(item);
            holder.btnDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemList.remove(position);
                    adapter.notifyDataSetChanged();
                    ((MemberWriteActivity)mActivity).updateCount();
                }
            });
        }
    }
}
