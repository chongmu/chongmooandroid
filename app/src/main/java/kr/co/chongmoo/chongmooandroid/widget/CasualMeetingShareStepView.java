package kr.co.chongmoo.chongmooandroid.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.woojinsoft.chongmoo.android.R;

/**
 * 번개 모임 공유 차수별 정산 관련 커스텀 뷰
 * Created by ??? on 2016-04-24.
 */
public class CasualMeetingShareStepView extends LinearLayout {

    private TextView mTextviewTxtCslShareMeetingNameLabel;
    private TextView mTextviewTxtCslShareMeetingName;
    private TextView mTextviewTxtCslShareMeetingAmountLabel;
    private TextView mTextviewTxtCslShareMeetingAmount;
    private TextView mTextviewTxtCslShareMeetingNAmountLable;
    private TextView mTextviewTxtCslShareMeetingNAmount;

    private LinearLayout mLinearLayoutLayerCslShareMeetingBalance;
    private TextView mTextviewTxtCslShareMeetingBalance;

    public CasualMeetingShareStepView(Context context) {
        super(context);
        init();
    }

    public CasualMeetingShareStepView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CasualMeetingShareStepView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CasualMeetingShareStepView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {

        inflate(getContext(), R.layout.view_casual_meeting_share_step, this);

        mTextviewTxtCslShareMeetingNameLabel = (TextView) findViewById(R.id.txt_csl_share_meeting_name_label);
        mTextviewTxtCslShareMeetingName = (TextView) findViewById(R.id.txt_csl_share_meeting_name);
        mTextviewTxtCslShareMeetingAmountLabel = (TextView) findViewById(R.id.txt_csl_share_meeting_amount_label);
        mTextviewTxtCslShareMeetingAmount = (TextView) findViewById(R.id.txt_csl_share_meeting_amount);
        mTextviewTxtCslShareMeetingNAmountLable = (TextView) findViewById(R.id.txt_csl_share_meeting_n_amount_lable);
        mTextviewTxtCslShareMeetingNAmount = (TextView) findViewById(R.id.txt_csl_share_meeting_n_amount);

        mLinearLayoutLayerCslShareMeetingBalance = (LinearLayout) findViewById(R.id.layer_csl_share_meeting_balance);
        mTextviewTxtCslShareMeetingBalance  = (TextView) findViewById(R.id.txt_csl_share_meeting_balance);

    }

    public void setShareMeetingNameLabel(String input) {
        mTextviewTxtCslShareMeetingNameLabel.setText(input);
    }

    public void setShareMeetingName(String input) {
        mTextviewTxtCslShareMeetingName.setText(input);
    }

    public void setShareMeetingAmountLable(String input) {
        mTextviewTxtCslShareMeetingAmountLabel.setText(input);
    }

    public void setShareMeetingAmount(String input) {
        mTextviewTxtCslShareMeetingAmount.setText(input);
    }

    public void setShareMeetingNAmountLable(String input) {
        mTextviewTxtCslShareMeetingNAmountLable.setText(input);
    }

    public void setShareMeetingNAmount(String input) {
        mTextviewTxtCslShareMeetingNAmount.setText(input);
    }

    public void setShareMeetingBalance(String amount) {
        mLinearLayoutLayerCslShareMeetingBalance.setVisibility(View.VISIBLE);
        mTextviewTxtCslShareMeetingBalance.setText(amount);
    }

}
