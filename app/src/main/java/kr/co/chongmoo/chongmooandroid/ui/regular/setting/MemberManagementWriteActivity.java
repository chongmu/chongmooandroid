package kr.co.chongmoo.chongmooandroid.ui.regular.setting;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmQuery;
import com.woojinsoft.chongmoo.android.R;

import io.realm.RealmResults;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;

/**
 *
 * 회원 수정 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class MemberManagementWriteActivity extends BaseSubActivity implements View.OnClickListener{

    public static final String EXTRA_MODE = "mode";
    public static final int MODE_WRITE = 1;
    public static final int MODE_READ = 2;
    public static final int MODE_UPDATE = 3;

    private EditText mEditName;
    private LinearLayout mLayoutJoinDate;
    private TextView mTextJoinDate;
    private Button mBtnCalendarJoin;
    private LinearLayout mLayoutStartDate;
    private TextView mTextStartDate;
    private Button mBtnCalendarStart;
    private EditText mEditMemo;

    private LinearLayout mLayoutDelete;
    private LinearLayout mLayoutPrev;
    private LinearLayout mLayoutOk;
    private TextView mTextOk;

    private Realm mRealm;

    private int mMode = MODE_READ;
    private int mSeq;

    private Date mJoinDate;
    private Date mStartDate;

    private Member mMember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_management_write);
        setTitle(R.string.activity_member_management_write_title);

        mEditName = (EditText)findViewById(R.id.edit_name);
        mLayoutJoinDate = (LinearLayout)findViewById(R.id.layout_join_date);
        mTextJoinDate = (TextView)findViewById(R.id.text_join_date);
        mBtnCalendarJoin = (Button)findViewById(R.id.btn_calendar_join);
        mLayoutStartDate = (LinearLayout)findViewById(R.id.layout_start_date);
        mTextStartDate = (TextView)findViewById(R.id.text_start_date);
        mBtnCalendarStart = (Button)findViewById(R.id.btn_calendar_start);
        mEditMemo = (EditText)findViewById(R.id.edit_memo);
        mLayoutDelete = (LinearLayout)findViewById(R.id.layout_delete);
        mLayoutPrev = (LinearLayout)findViewById(R.id.layout_prev);
        mLayoutOk = (LinearLayout)findViewById(R.id.layout_ok);
        mTextOk = (TextView)findViewById(R.id.text_ok);

        mTextJoinDate.setOnClickListener(this);
        mBtnCalendarJoin.setOnClickListener(this);
        mTextStartDate.setOnClickListener(this);
        mBtnCalendarStart.setOnClickListener(this);
        mLayoutDelete.setOnClickListener(this);
        mLayoutPrev.setOnClickListener(this);
        mLayoutOk.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            mMode = bundle.getInt(EXTRA_MODE);
            mSeq = bundle.getInt("SEQ");
        }

        changeMode(mMode);

        mRealm = Realm.getDefaultInstance();
        RealmQuery<Member> query = mRealm.where(Member.class);
        mMember = query.equalTo("seq", mSeq).findFirst();

        refreshUi();
    }

    private void refreshUi(){
        mJoinDate = mMember.getRegistDate();
        mStartDate = mMember.getStartDate();

        mEditName.setText(mMember.getName());
        setTextDate(mTextJoinDate, mJoinDate, false);
        setTextDate(mTextStartDate, mStartDate, true);
        mEditMemo.setText(mMember.getMemo());
    }

    private void changeMode(int mode){
        boolean isReadMode = (mode == MODE_READ);
        mEditName.setEnabled(isReadMode ? false : true);
        mLayoutJoinDate.setActivated(isReadMode);
        mTextJoinDate.setEnabled(isReadMode ? false : true);
        mBtnCalendarJoin.setVisibility(isReadMode ? View.GONE : View.VISIBLE);
        mLayoutStartDate.setActivated(isReadMode);
        mTextStartDate.setEnabled(isReadMode ? false : true);
        mBtnCalendarStart.setVisibility(isReadMode ? View.GONE : View.VISIBLE);
        mEditMemo.setEnabled(isReadMode ? false : true);
        mLayoutDelete.setVisibility(isReadMode ? View.VISIBLE : View.GONE);
        mLayoutPrev.setVisibility(isReadMode ? View.GONE : View.VISIBLE);
        mTextOk.setText(isReadMode ? R.string.modify : R.string.confirm);

        hideKeyboard();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.text_join_date:
            case R.id.btn_calendar_join: {
                final ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_CALRENDAR);
                dialog.setTitle(getString(R.string.dialog_calendar_text));
                dialog.setDate(mJoinDate);
                dialog.setPositiveButton(getString(R.string.dialog_calendar_positive_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mJoinDate = dialog.getDate();
                        setTextDate(mTextJoinDate, mJoinDate,false);
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(getString(R.string.dialog_calendar_negative_text), null);
                dialog.show();
                break;
            }
            case R.id.text_start_date:
            case R.id.btn_calendar_start: {
                final ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_CALRENDAR_MONTH);
                dialog.setTitle(getString(R.string.dialog_calendar_text));
                dialog.setDate(mStartDate);
                dialog.setPositiveButton(getString(R.string.dialog_calendar_positive_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mStartDate = dialog.getDate();
                        setTextDate(mTextStartDate, mStartDate, true);
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(getString(R.string.dialog_calendar_negative_text), null);
                dialog.show();
                break;
            }
            case R.id.layout_delete:
                ChongmooDialog dialog = new ChongmooDialog(this);
                dialog.setMessage(getString(R.string.dialog_chongmoo_member_delete_message));
                dialog.setPositiveButton(getString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final int memberSeq = mMember.getSeq();
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                RealmQuery<Member> query = realm.where(Member.class);
                                Member member = query.equalTo("seq", mSeq).findFirst();
                                RealmQuery<Income> incomeQuery = realm.where(Income.class);
                                RealmResults<Income> results = incomeQuery.equalTo("memberSeq", memberSeq).findAll();
                                if(DateUtil.getDateToYearMonthString(member.getRegistDate()).equals(DateUtil.getDateToYearMonthString(new Date()))) {
                                    if(results.size() == 0 && (member.getExemptionList() == null || member.getExemptionList().size() == 0)) {
                                        member.removeFromRealm();
                                        return ;
                                    }
                                }
                                member.setOutDate(new Date());
                            }
                        }, new Realm.Transaction.Callback() {
                            @Override
                            public void onSuccess() {
                                setResult(RESULT_OK);
                                finish();
                            }

                            @Override
                            public void onError(Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getBaseContext(), String.format(getString(R.string.dialog_chongmoo_member_delete_fail), e.toString()), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
                dialog.setNegativeButton(getString(R.string.cancel), null);
                dialog.show();
                break;
            case R.id.layout_prev:
                onBackPressed();
                break;
            case R.id.layout_ok:
                if(mMode == MODE_READ){
                    mMode = MODE_UPDATE;
                    changeMode(mMode);
                }
                else{
                    final String name = mEditName.getText().toString().trim();
                    final String memo = mEditMemo.getText().toString();

                    if(TextUtils.isEmpty(name)){
                        Toast.makeText(getBaseContext(), R.string.toast_require_name, Toast.LENGTH_SHORT).show();
                        return ;
                    }

                    if(!DateUtil.getDateToYearMonthString(mMember.getStartDate()).equals(DateUtil.getDateToYearMonthString(mStartDate))){
                        RealmQuery<Income> incomeQuery = mRealm.where(Income.class);
                        RealmResults<Income> results = incomeQuery.equalTo("memberSeq", mMember.getSeq()).findAll();
                        if (results.size() > 0) {
                            mStartDate = mMember.getStartDate();
                            setTextDate(mTextStartDate, mStartDate, true);
                            Toast.makeText(getBaseContext(), R.string.toast_already_registered_fee, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        RealmQuery<RegularMeeting> regularMeetingQuery = mRealm.where(RegularMeeting.class);
                        RegularMeeting regularMeeting = regularMeetingQuery.equalTo("seq", mMember.getMeetingSeq()).findFirst();
                        int newStartDate = Integer.parseInt(DateUtil.getDateToYearMonthString(mStartDate));
                        int meetingStartDate = Integer.parseInt(DateUtil.getDateToYearMonthString(regularMeeting.getStartdate()));
                        if(newStartDate < meetingStartDate){
                            mStartDate = mMember.getStartDate();
                            setTextDate(mTextStartDate, mStartDate, true);
                            Toast.makeText(getBaseContext(), R.string.toast_date_mismatch_selected, Toast.LENGTH_SHORT).show();
                            return ;
                        }
                    }

                    mRealm.beginTransaction();
                    mMember.setName(name);
                    mMember.setRegistDate(mJoinDate);
                    mMember.setStartDate(mStartDate);
                    mMember.setMemo(memo);
                    mRealm.commitTransaction();

                    setResult(RESULT_OK);
                    finish();
                }
                break ;
        }
    }

    private void setTextDate(TextView textView, Date date, boolean modeMonth){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        textView.setText(cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + (modeMonth ? "" : "-" + cal.get(Calendar.DAY_OF_MONTH)));
    }

    @Override
    public void onBackPressed() {
        if(mMode == MODE_UPDATE){
            mMode = MODE_READ;
            changeMode(mMode);
            refreshUi();
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }
}
