package kr.co.chongmoo.chongmooandroid.ui.casual;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.model.AddressBook;
import kr.co.chongmoo.chongmooandroid.model.CasualMeeting;
import kr.co.chongmoo.chongmooandroid.model.CasualMeetingStep;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.ui.common.MoneyCommaEditTextWather;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.util.StringUtil;
import kr.co.chongmoo.chongmooandroid.widget.CasualMeetingMemberSubView;
import kr.co.chongmoo.chongmooandroid.widget.CasualMeetingMemberView;
import kr.co.chongmoo.chongmooandroid.widget.CasualMeetingStepView;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;

/**
 * N빵 모임 화면
 */
public class CasualMeetingActivity extends BaseSubActivity {

    public final static String USE_MODE_KEY = "MODE";
    private String mMode;

    public final static String USE_MODE_VALUE_READ = "R";
    public final static String USE_MODE_VALUE_UPDT = "U";
    public final static String USE_MODE_VALUE_WRIT = "W";

    public final static String SEQ_KEY = "seq";

    public final static int REQUEST_CODE_ADDRESS_BOOK_ACTIVITY = 0;
    public final static String REQUEST_PARAM_ADDRESS_BOOK_ACTIVITY = "ADDRESS_LIST";
    private final static String STRING_EMPTY = "";

    /** 모임 등록일 텍스트   */ private ScrollView mScroll;
    /** 모임 등록일 텍스트   */ private TextView mTextViewCslDate;
    /** 모임 등록일 달력버튼 */ private Button mButtonCslCalendar;

    /** 모임 명              */ private EditText mEditTextMeetingName;
    /** 모임 수입            */ private EditText mEditTextMount;
    /** 멤버추가 주소록 버튼 */ private TextView mTextViewBtnAddressBook;
    /** 멤버 수동 추가 버튼  */ private ImageView mImageViewBtnAddCaulMemb;
    /** 예금 주              */ private EditText mEditTextAccountHoler;
    /** 예금 은행            */ private EditText mEditTextBank;
    /** 예금 계좌 번호       */ private EditText mEditTextAccountNumber;
    /** 메모                 */ private EditText mEditTextMemo;
    /** 지접입력창           */ private EditText mEditTextTxtAddCaulMemb;

    private LinearLayout mLinearLayoutcslMeetStep;
    private TextView mTextViewBtnAddCslStep;
    private LinearLayout mLinearLayoutBtnAddCslStep;

    private LinearLayout mLinearLayoutBtnMainYesDataBottomCasual;
    private RelativeLayout mLinearLayoutBtnCslShare;

    private List<String> mMemberList;
    private RealmList<CasualMeetingStep> mCasualMeetingStepList;

    private Realm mRealm;

    private CasualMeeting mCasualMeeting;
    private CasualMeetingStepView mCasualMeetingStepViewFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 기본 화면 구성 관련 코드 시작
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_casual_meeting);
        setTitle(getString(R.string.title_activity_casual_meeting));

        if(mRealm == null || mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }

        mMemberList = new ArrayList<String>();
        mCasualMeetingStepList = new RealmList<CasualMeetingStep>();

        mScroll = (ScrollView) findViewById(R.id.scroll);
        mTextViewCslDate = (TextView) findViewById(R.id.txt_csl_date);
        String currentDate = DateUtil.getDateToYearMonthDayString(new Date());
        mTextViewCslDate.setText(currentDate);

        String stepName = getString(R.string.label_casual_meeting_step_1step);
        CasualMeetingStep casualMeetingStep = new CasualMeetingStep();
        casualMeetingStep.setName(stepName);
        mCasualMeetingStepList.add(casualMeetingStep);

        mLinearLayoutcslMeetStep = (LinearLayout) findViewById(R.id.layout_csl_step);
        mCasualMeetingStepViewFirst = new CasualMeetingStepView(this);
        mCasualMeetingStepViewFirst.setStepName(stepName);
        mCasualMeetingStepViewFirst.setCasualMeetingStep(casualMeetingStep);
        mLinearLayoutcslMeetStep.addView(mCasualMeetingStepViewFirst);

        mEditTextMount = (EditText) findViewById(R.id.edit_csl_monthly_fee);
        mEditTextMount.addTextChangedListener(new MoneyCommaEditTextWather(mEditTextMount));

        // 기본 화면 구성 관련 코드 끝
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 액션에 따른 이용 때문에 주입 되는 케이스 시작
        mEditTextMeetingName = (EditText) findViewById(R.id.edit_csl_name);
        mEditTextAccountHoler = (EditText) findViewById(R.id.txt_csl_account_holder);
        mEditTextBank = (EditText) findViewById(R.id.txt_csl_bank);
        mEditTextAccountNumber = (EditText) findViewById(R.id.txt_csl_account_number);
        mEditTextMemo = (EditText) findViewById(R.id.txt_csl_memo);
        mEditTextMemo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    ((EditText) v).setHint(getString(R.string.string_empty));
                } else {
                    ((EditText) v).setHint(getString(R.string.hint_casual_meeting_memo));
                }
            }
        });
        mEditTextMemo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                    mScroll.requestDisallowInterceptTouchEvent(false);
                else
                    mScroll.requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        // 액션에 따른 이용 때문에 주입 되는 케이스 끝
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 해당 엑티비티를 호출하는 방식에 따라 값 셋팅 로직 시작

        final String mode = getIntent().getStringExtra(USE_MODE_KEY);
        mMode = mode;
        int seq = 0;
        RealmQuery<CasualMeeting> query = null;
        RealmList<AddressBook> addressBookRealmList;

        switch (mode) {
            case USE_MODE_VALUE_READ:
            case USE_MODE_VALUE_UPDT:
                seq = getIntent().getIntExtra(SEQ_KEY, 0);
                query = mRealm.where(CasualMeeting.class);
                mCasualMeeting = query.equalTo(SEQ_KEY, seq).findFirst();
                mTextViewCslDate.setText(DateUtil.getDateToYearMonthDayString(mCasualMeeting.getDate())); // 모임기본정보::날짜
                mEditTextMeetingName.setText(mCasualMeeting.getName());                                   // 모임기본정보::모임 이름

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                RealmList<CasualMeetingStep> casualMeetingStepList = mCasualMeeting.getCasualMeetingStepList();
                RealmList<CasualMeetingStep> casualMeetingStepListClone = new RealmList<CasualMeetingStep>();
                for(CasualMeetingStep casualMeetingStepSub : casualMeetingStepList) {
                    CasualMeetingStep casualMeetingStepClone = new CasualMeetingStep();
                    casualMeetingStepClone.setArea(casualMeetingStepSub.getArea());
                    casualMeetingStepClone.setAmount(casualMeetingStepSub.getAmount());
                    casualMeetingStepClone.setName(casualMeetingStepSub.getName());

                    RealmList<AddressBook> addressBookRealmListClone = new RealmList<AddressBook>();
                    RealmList<AddressBook> addressBookRealmListSub = casualMeetingStepSub.getAddressBookList();
                    for(AddressBook addressBookSub : addressBookRealmListSub) {
                        AddressBook addressBookClone = new AddressBook();
                        addressBookClone.setName(addressBookSub.getName());
                        addressBookClone.setIsChecked(addressBookSub.getIsChecked());
                        addressBookRealmListClone.add(addressBookClone);
                    }
                    casualMeetingStepClone.setAddressBookList(addressBookRealmListClone);
                    casualMeetingStepListClone.add(casualMeetingStepClone);
                }
                mCasualMeetingStepList = casualMeetingStepListClone;

                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_csl_step);
                linearLayout.removeAllViews();
                for( int i = 0 ; i < mCasualMeetingStepList.size() ; i++ ) {

                    // CasualMeetingStepView커스텀 뷰에도 CasualMeetingStep 오브젝트를 넣어준다. 안에서 찾아서 하기 쉽게 만들기 위해서
                    CasualMeetingStepView casualMeetingStepViewSub = new CasualMeetingStepView(CasualMeetingActivity.this);
                    casualMeetingStepViewSub.setStepName(mCasualMeetingStepList.get(i).getName());
                    casualMeetingStepViewSub.setStepAmount(mCasualMeetingStepList.get(i).getAmount());
                    casualMeetingStepViewSub.setStepArea(mCasualMeetingStepList.get(i).getArea());
                    casualMeetingStepViewSub.setCasualMeetingStep(mCasualMeetingStepList.get(i));
                    if(i > 0) {
                        casualMeetingStepViewSub.onDeleteStepButtoon(true);
                    }
                    linearLayout.addView(casualMeetingStepViewSub);

                }
                // 모임 차수 추가에 따른 추가 버튼 텍스트 수정
                int btnAddCslStepTextCount = mCasualMeetingStepList.size() + 1;
                TextView textView = (TextView) findViewById(R.id.btn_add_csl_step);
                textView.setText(String.format(getString(R.string.label_casual_meeting_step_format_txt_1), btnAddCslStepTextCount));
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                addressBookRealmList = mCasualMeeting.getAddressBookList();
                for(AddressBook addressBook :addressBookRealmList) {
                    mMemberList.add(addressBook.getName());
                    putMember(addressBook.getName());
                }
                showCasualMeetingMemberStepSubView();
                mEditTextMount.setText(mCasualMeeting.getMount());                                        // 모임기본정보::수입
                mEditTextAccountHoler.setText(mCasualMeeting.getAccountHolder());                         // 모임기본정보::예금주
                mEditTextBank.setText(mCasualMeeting.getBank());                                          // 모임기본정보::은행명
                mEditTextAccountNumber.setText(mCasualMeeting.getAccountNumber());                        // 모임기본정보::계좌번호
                mEditTextMemo.setText(mCasualMeeting.getMemo());                                          // 모임기본정보::메모
                break;
        }

        // 해당 엑티비티를 호출하는 방식에 따라 값 셋팅 로직 끝
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 저장 버튼 클릭시 로직 시작
        mLinearLayoutBtnMainYesDataBottomCasual = (LinearLayout) findViewById(R.id.btn_main_yes_data_bottom_casual);
        mLinearLayoutBtnMainYesDataBottomCasual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // 디버그 용도 코드
                for(CasualMeetingStep casualMeetingStep : mCasualMeetingStepList) {
                    Log.d("debug", casualMeetingStep.getName());
                    RealmList<AddressBook> addressBookList = casualMeetingStep.getAddressBookList();
                    for(AddressBook addressBook : addressBookList) {
                        Log.d("debug", addressBook.getName());
                    }
                }
                Log.d("debug", "showMember~~~~~~~~~~~~~~~~~~~");
                for(String name : mMemberList) {
                    Log.d("debug", name);
                }

                boolean processStatus = isInsertOrUpdateAble();

                if(processStatus == true) {

                    saveCasualMeeting(getString(R.string.toast_casual_meeting_save_success_txt));

                } else {

                    Toast.makeText(getBaseContext(), getString(R.string.toast_casual_meeting_save_validation_fail_txt) , Toast.LENGTH_SHORT).show();

                }

            }
        });
        // 저장 버튼 클릭시 로직 끝
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 모임 공유 버튼 클릭시 로직 시작
        mLinearLayoutBtnCslShare = (RelativeLayout) findViewById(R.id.btn_csl_share);
        mLinearLayoutBtnCslShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mCasualMeeting != null) {

                    boolean isDataNotSameStatus = isDataNotSame();

                    if(isDataNotSameStatus == true) {

                        Toast.makeText(CasualMeetingActivity.this, getString(R.string.toast_casual_meeting_share_data_not_same_fail_txt), Toast.LENGTH_SHORT).show();

                    } else {

                        Intent intent = new Intent(CasualMeetingActivity.this, CasualShareActivity.class);
                        Bundle extra = new Bundle();
                        extra.putInt(SEQ_KEY, mCasualMeeting.getSeq());
                        intent.putExtras(extra);
                        setResult(RESULT_OK, intent);
                        startActivity(intent);

                    }

                } else {
                    Toast.makeText(getBaseContext(), getString(R.string.toast_casual_meeting_share_validation_fail_txt), Toast.LENGTH_SHORT).show();
                }

            }
        });
        // 모임 공유 버튼 클릭시 로직 끝
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 모임 날짜 달력 버튼 클릭시 로직 시작
        mButtonCslCalendar = (Button) findViewById(R.id.btn_csl_calendar);
        mButtonCslCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ChongmooDialog dialog = new ChongmooDialog(CasualMeetingActivity.this, ChongmooDialog.MODE_CALRENDAR);
                dialog.setTitle(getString(R.string.dialog_calendar_text));
                dialog.setDate(new Date());
                dialog.setPositiveButton(getString(R.string.dialog_calendar_positive_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Date input = dialog.getDate();
                        String date = DateUtil.getDateToYearMonthDayString(input);
                        TextView textView = (TextView) findViewById(R.id.txt_csl_date);
                        textView.setText(date);
                        dialog.dismiss();

                    }
                });
                dialog.setNegativeButton(getString(R.string.cancel), null);
                dialog.show();

            }
        });
        mTextViewCslDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ChongmooDialog dialog = new ChongmooDialog(CasualMeetingActivity.this, ChongmooDialog.MODE_CALRENDAR);
                dialog.setTitle(getString(R.string.dialog_calendar_text));
                dialog.setDate(new Date());
                dialog.setPositiveButton(getString(R.string.dialog_calendar_positive_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Date input = dialog.getDate();
                        String date = DateUtil.getDateToYearMonthDayString(input);
                        TextView textView = (TextView) findViewById(R.id.txt_csl_date);
                        textView.setText(date);
                        dialog.dismiss();

                    }
                });
                dialog.setNegativeButton(getString(R.string.cancel), null);
                dialog.show();

            }
        });
        // 모임 날짜 달력 버튼 클릭시 로직 끝
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 모임 차수 추가 버튼 클릭시 로직 시작
        mTextViewBtnAddCslStep = (TextView) findViewById(R.id.btn_add_csl_step);
        mTextViewBtnAddCslStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_csl_step);
                int nextChildCount = linearLayout.getChildCount() + 1;
                String stepName = String.format(getString(R.string.label_casual_meeting_step_format_txt_2), nextChildCount);

                CasualMeetingStep casualMeetingStep = new CasualMeetingStep();
                casualMeetingStep.setName(stepName);
                mCasualMeetingStepList.add(casualMeetingStep);

                // CasualMeetingStepView커스텀 뷰에도 CasualMeetingStep 오브젝트를 넣어준다. 안에서 찾아서 하기 쉽게 만들기 위해서
                CasualMeetingStepView casualMeetingStepView = new CasualMeetingStepView(CasualMeetingActivity.this);
                casualMeetingStepView.setStepName(stepName);
                casualMeetingStepView.setCasualMeetingStep(casualMeetingStep);
                casualMeetingStepView.onDeleteStepButtoon(true);
                linearLayout.addView(casualMeetingStepView);

                // 모임 차수 추가에 따른 추가 버튼 텍스트 수정
                int btnAddCslStepTextCount = nextChildCount + 1;
                TextView textView = (TextView) findViewById(R.id.btn_add_csl_step);
                textView.setText(String.format(getString(R.string.label_casual_meeting_step_format_txt_1), btnAddCslStepTextCount));
                showCasualMeetingMemberStepSubView();

            }
        });
        mLinearLayoutBtnAddCslStep = (LinearLayout) findViewById(R.id.layer_add_csl_step);
        mLinearLayoutBtnAddCslStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_csl_step);
                int nextChildCount = linearLayout.getChildCount() + 1;
                String stepName = String.format(getString(R.string.label_casual_meeting_step_format_txt_2), nextChildCount);

                CasualMeetingStep casualMeetingStep = new CasualMeetingStep();
                casualMeetingStep.setName(stepName);
                mCasualMeetingStepList.add(casualMeetingStep);

                // CasualMeetingStepView커스텀 뷰에도 CasualMeetingStep 오브젝트를 넣어준다. 안에서 찾아서 하기 쉽게 만들기 위해서
                CasualMeetingStepView casualMeetingStepView = new CasualMeetingStepView(CasualMeetingActivity.this);
                casualMeetingStepView.setStepName(stepName);
                casualMeetingStepView.setCasualMeetingStep(casualMeetingStep);
                casualMeetingStepView.onDeleteStepButtoon(true);
                linearLayout.addView(casualMeetingStepView);

                // 모임 차수 추가에 따른 추가 버튼 텍스트 수정
                int btnAddCslStepTextCount = nextChildCount + 1;
                TextView textView = (TextView) findViewById(R.id.btn_add_csl_step);
                textView.setText(String.format(getString(R.string.label_casual_meeting_step_format_txt_1), btnAddCslStepTextCount));
                showCasualMeetingMemberStepSubView();

            }
        });
        // 모임 차수 추가 버튼 클릭시 로직 끝
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 주소록버튼 로직 시작
        mTextViewBtnAddressBook = (TextView) findViewById(R.id.btn_address_book);
        mTextViewBtnAddressBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int permissionCheck = ContextCompat.checkSelfPermission(CasualMeetingActivity.this, Manifest.permission.READ_CONTACTS);
                if(permissionCheck == PackageManager.PERMISSION_DENIED) { // 마시멜로우 버젼 관련 체크

                    new TedPermission(CasualMeetingActivity.this)
                            .setPermissionListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted() {
                                    Toast.makeText(CasualMeetingActivity.this, getString(R.string.toast_authority_permit_txt), Toast.LENGTH_SHORT).show();
                                }
                                @Override
                                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                    Toast.makeText(CasualMeetingActivity.this, getString(R.string.toast_authority_deny_txt) + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setDeniedMessage(getString(R.string.toast_authority_denied_message))
                            .setPermissions(Manifest.permission.READ_CONTACTS)
                            .check();

                } else {
                    startActivityForResult(new Intent(CasualMeetingActivity.this, AddressBookActivity.class), REQUEST_CODE_ADDRESS_BOOK_ACTIVITY);
                }
            }
        });
        // 주소록버튼 로직 끝
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 맴버 수동 추가 시 수동 추가 버튼 클릭관계된 로직 시작 (주소록버튼 아래에 해당버튼 있음)
        mImageViewBtnAddCaulMemb = (ImageView) findViewById(R.id.btn_add_caul_memb);
        mImageViewBtnAddCaulMemb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText editText = (EditText) findViewById(R.id.txt_add_caul_memb);
                editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if(hasFocus) {
                            ((EditText) v).setHint(getString(R.string.string_empty));
                        } else {
                            ((EditText) v).setHint(getString(R.string.edittext_direct_input));
                        }
                    }
                });

                String memberName = editText.getText().toString();
                if(memberName != null && memberName.trim().equals(STRING_EMPTY) == false) {
                    mMemberList.add(memberName);
                    putMember(memberName);
                    editText.setText(STRING_EMPTY);
                    showCasualMeetingMemberStepSubView();
                }

            }
        });

        mEditTextTxtAddCaulMemb = (EditText) findViewById(R.id.txt_add_caul_memb);
        mEditTextTxtAddCaulMemb.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ((EditText) v).setHint(getString(R.string.string_empty));
                } else {
                    ((EditText) v).setHint(getString(R.string.edittext_direct_input));
                }
            }
        });
        // 맴버 수동 추가 시 수동 추가 버튼 클릭관계된 로직 끝 (주소록버튼 아래에 해당버튼 있음)
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }

    /**
     * 호출 될때마다 모임 차수에 참여 멤버를 계속 그린다.
     */
    private void showCasualMeetingMemberStepSubView() {

        int mLinearLayoutcslMeetStepChildCount = mLinearLayoutcslMeetStep.getChildCount();
        for( int i = 0 ; i < mLinearLayoutcslMeetStepChildCount ; i++ ) {

            CasualMeetingStepView casualMeetingStepView = (CasualMeetingStepView) mLinearLayoutcslMeetStep.getChildAt(i);
            RealmList<AddressBook> addressBookList = casualMeetingStepView.getCasualMeetingStep().getAddressBookList();

            LinearLayout childLinearCslMeetStep  = (LinearLayout) casualMeetingStepView.findViewById(R.id.layout_cusl_met_meb);
            childLinearCslMeetStep.removeAllViews();

            int memberListSize = mMemberList.size();
            for(int m = 0 ; m < memberListSize ; m+=4 ) {

                CasualMeetingMemberSubView casualMeetingMemberSubView = new CasualMeetingMemberSubView(this);
                casualMeetingMemberSubView.setCasualMeetingStepView(casualMeetingStepView);
                int stepCount = m + 4;

                if(stepCount < memberListSize) {

                    casualMeetingMemberSubView.setFirstStatus(mMemberList.get(m), View.VISIBLE);
                    casualMeetingMemberSubView.setSecondStatus(mMemberList.get(m + 1), View.VISIBLE);
                    casualMeetingMemberSubView.setThirdStatus(mMemberList.get(m + 2), View.VISIBLE);
                    casualMeetingMemberSubView.setFourthlyStatus(mMemberList.get(m + 3), View.VISIBLE);

                } else {

                    int showCount = (memberListSize - (stepCount - 4)) ;
                    if(showCount == 1) {
                        casualMeetingMemberSubView.setFirstStatus(mMemberList.get(m), View.VISIBLE);
                    } else if(showCount == 2) {
                        casualMeetingMemberSubView.setFirstStatus(mMemberList.get(m), View.VISIBLE);
                        casualMeetingMemberSubView.setSecondStatus(mMemberList.get(m + 1), View.VISIBLE);
                    } else if(showCount == 3) {
                        casualMeetingMemberSubView.setFirstStatus(mMemberList.get(m), View.VISIBLE);
                        casualMeetingMemberSubView.setSecondStatus(mMemberList.get(m + 1), View.VISIBLE);
                        casualMeetingMemberSubView.setThirdStatus(mMemberList.get(m + 2), View.VISIBLE);
                    } else if(showCount == 4) {
                        casualMeetingMemberSubView.setFirstStatus(mMemberList.get(m), View.VISIBLE);
                        casualMeetingMemberSubView.setSecondStatus(mMemberList.get(m + 1), View.VISIBLE);
                        casualMeetingMemberSubView.setThirdStatus(mMemberList.get(m + 2), View.VISIBLE);
                        casualMeetingMemberSubView.setFourthlyStatus(mMemberList.get(m + 3), View.VISIBLE);
                    }

                }

                childLinearCslMeetStep.addView(casualMeetingMemberSubView);
                casualMeetingMemberSubView.updateCasualStepMemberCount();

            }
        }

    }

    /**
     * 주소록 및 수동 추가 버튼시 추가 UI를 그린다. ( 수동 추가로 생겨나는 화면에 대한 이벤트 리스너도 하위에 포함되어 있다.)
     * @param memberName
     */
    private void putMember(final String memberName) {

        if(memberName != null && memberName.trim().equals(STRING_EMPTY) != true) {

            final LinearLayout linearLayoutDftInfo = (LinearLayout) findViewById(R.id.layout_dft_info);
            final CasualMeetingMemberView casualMeetingMemberView = new CasualMeetingMemberView(CasualMeetingActivity.this);

            casualMeetingMemberView.setMember(memberName);
            casualMeetingMemberView.setDelBtnOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    linearLayoutDftInfo.removeView(casualMeetingMemberView);
                    mMemberList.remove(memberName);
                    for (CasualMeetingStep casualMeetingStep : mCasualMeetingStepList) {
                        RealmList<AddressBook> addressBooks = casualMeetingStep.getAddressBookList();
                        for (int i = 0; i < addressBooks.size(); i++) {
                            if (addressBooks.get(i).getName().equals(memberName)) {
                                addressBooks.remove(i);
                            }
                        }
                    }
                    showCasualMeetingMemberStepSubView();
                }
            });

            linearLayoutDftInfo.addView(casualMeetingMemberView, 8);

        }

    }

    /**
     * 현재 화면에 있는 데이터와 디비에 저장되어 있는 데이터가 같은지 확인한다.
     */
    private boolean isDataNotSame() {

        boolean returnStatus = false;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 디비에서 일치 확인을 위한 조회 시작
        int seq = mCasualMeeting.getSeq();
        RealmQuery<CasualMeeting> query = mRealm.where(CasualMeeting.class);
        CasualMeeting casualMeetingOri = query.equalTo(SEQ_KEY, seq).findFirst();
        // 디비에서 일치 확인을 위한 조회 끝
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 데이터 비교를 위한 디비 값 복사 시작
        RealmList<CasualMeetingStep> casualMeetingStepList = casualMeetingOri.getCasualMeetingStepList();
        RealmList<CasualMeetingStep> casualMeetingStepListCloneOri = new RealmList<CasualMeetingStep>();
        for(CasualMeetingStep casualMeetingStepSub : casualMeetingStepList) {
            CasualMeetingStep casualMeetingStepClone = new CasualMeetingStep();
            casualMeetingStepClone.setArea(casualMeetingStepSub.getArea());
            casualMeetingStepClone.setAmount(casualMeetingStepSub.getAmount());
            casualMeetingStepClone.setName(casualMeetingStepSub.getName());

            RealmList<AddressBook> addressBookRealmListClone = new RealmList<AddressBook>();
            RealmList<AddressBook> addressBookRealmListSub = casualMeetingStepSub.getAddressBookList();
            for(AddressBook addressBookSub : addressBookRealmListSub) {
                AddressBook addressBookClone = new AddressBook();
                addressBookClone.setName(addressBookSub.getName());
                addressBookClone.setIsChecked(addressBookSub.getIsChecked());
                addressBookRealmListClone.add(addressBookClone);
            }
            casualMeetingStepClone.setAddressBookList(addressBookRealmListClone);
            casualMeetingStepListCloneOri.add(casualMeetingStepClone);
        }

        RealmList<CasualMeetingStep> casualMeetingStepListCloneDiff = new RealmList<CasualMeetingStep>();
        for(CasualMeetingStep casualMeetingStepSub : mCasualMeetingStepList) {
            CasualMeetingStep casualMeetingStepClone = new CasualMeetingStep();
            casualMeetingStepClone.setArea(casualMeetingStepSub.getArea());
            casualMeetingStepClone.setAmount(casualMeetingStepSub.getAmount());
            casualMeetingStepClone.setName(casualMeetingStepSub.getName());

            RealmList<AddressBook> addressBookRealmListClone = new RealmList<AddressBook>();
            RealmList<AddressBook> addressBookRealmListSub = casualMeetingStepSub.getAddressBookList();
            for(AddressBook addressBookSub : addressBookRealmListSub) {
                AddressBook addressBookClone = new AddressBook();
                addressBookClone.setName(addressBookSub.getName());
                addressBookClone.setIsChecked(addressBookSub.getIsChecked());
                addressBookRealmListClone.add(addressBookClone);
            }
            casualMeetingStepClone.setAddressBookList(addressBookRealmListClone);
            casualMeetingStepListCloneDiff.add(casualMeetingStepClone);
        }
        // 데이터 비교를 위한 디비 값 복사 끝
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 데이터 비교 로직 시작

        String dateDiff = mTextViewCslDate.getText().toString(); // 모임등록일
        String meetingNameDiff = mEditTextMeetingName.getText().toString(); // 모임명
        String mountDiff = mEditTextMount.getText().toString(); // 수입
        String accountHolerDiff = mEditTextAccountHoler.getText().toString(); // 예금주
        String bankDiff = mEditTextBank.getText().toString(); // 은행
        String accountNumberDiff = mEditTextAccountNumber.getText().toString(); // 계좌번호
        String memoDiff = mEditTextMemo.getText().toString(); // 메모

        if(StringUtil.compareSameString(DateUtil.getDateToYearMonthDayString(casualMeetingOri.getDate()) , dateDiff) == false) { // 모임 등록일 비교
            returnStatus = true;
            return returnStatus;
        }
        if(StringUtil.compareSameString(casualMeetingOri.getName(), meetingNameDiff) == false) { // 모임 명 비교
            returnStatus = true;
            return returnStatus;
        }
        if (StringUtil.compareSameString(casualMeetingOri.getMount(), mountDiff) == false) { // 모임 수입 비교
            returnStatus = true;
            return returnStatus;
        }
        if (StringUtil.compareSameString(casualMeetingOri.getAccountHolder(), accountHolerDiff) == false) { // 모임 예금주 비교
            returnStatus = true;
            return returnStatus;
        }
        if (StringUtil.compareSameString(casualMeetingOri.getBank(), bankDiff) == false) { // 모임 은행 비교
            returnStatus = true;
            return returnStatus;
        }
        if (StringUtil.compareSameString(casualMeetingOri.getAccountNumber(), accountNumberDiff) == false) { // 모임 계좌번호 비교
            returnStatus = true;
            return returnStatus;
        }
        if (StringUtil.compareSameString(casualMeetingOri.getMemo(), memoDiff) == false) { // 모임 계좌번호 비교
            returnStatus = true;
            return returnStatus;
        }
        RealmList<AddressBook> addressBookListOri = casualMeetingOri.getAddressBookList();
        int mMemberListSize = mMemberList.size();
        int addressBookListOriSize = addressBookListOri.size();
        if(mMemberListSize != addressBookListOriSize) {
            returnStatus = true;
            return returnStatus;
        }

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_csl_step);
        int linearLayoutGetChildCount = linearLayout.getChildCount();
        int casualMeetingStepListSize = casualMeetingOri.getCasualMeetingStepList().size();
        if(linearLayoutGetChildCount != casualMeetingStepListSize) {
            returnStatus = true;
            return returnStatus;
        }
        for(int i = 0 ; i < linearLayoutGetChildCount ; i++ ) {

            CasualMeetingStep casualMeetingStepOri = casualMeetingOri.getCasualMeetingStepList().get(i);
            CasualMeetingStepView casualMeetingStepViewTemp = (CasualMeetingStepView) linearLayout.getChildAt(i);
            if(StringUtil.compareSameString(casualMeetingStepOri.getArea(), casualMeetingStepViewTemp.getStepArea()) == false ||
                    StringUtil.compareSameString(casualMeetingStepOri.getAmount(), casualMeetingStepViewTemp.getStepAmount()) == false) {

                returnStatus = true;
                return returnStatus;
            }

            RealmList<AddressBook> addressBookListDiff = casualMeetingStepViewTemp.getCasualMeetingStep().getAddressBookList();
            RealmList<AddressBook> addressBookListOriClone = casualMeetingStepOri.getAddressBookList();
            if(addressBookListDiff.size() != addressBookListOriClone.size()) {

                returnStatus = true;
                return returnStatus;

            } else {

                for( int j = 0 ; j < addressBookListDiff.size() ; j++ ) {

                    AddressBook addressBookDiff = addressBookListDiff.get(j);
                    AddressBook addressBookOri = addressBookListOriClone.get(j);

                    Log.d("DEBUG", addressBookDiff.getName());
                    Log.d("DEBUG", addressBookOri.getName());
                    if(!addressBookDiff.getName().equals(addressBookOri.getName())) {

                        returnStatus = true;
                        return returnStatus;

                    }

                }

            }

        }

        /*for(int i = 0 ; i < linearLayoutGetChildCount ; i++ ) {

            CasualMeetingStep casualMeetingStepOri = casualMeetingOri.getCasualMeetingStepList().get(i);
            CasualMeetingStep casualMeetingStepDiff = mCasualMeetingStepList.get(i);

            RealmList<AddressBook> addressBookListDiff = casualMeetingStepDiff.getAddressBookList();
            RealmList<AddressBook> addressBookListOriClone = casualMeetingStepOri.getAddressBookList();

            if(addressBookListDiff.size() != addressBookListOriClone.size()) {

                returnStatus = true;
                return returnStatus;

            } else {

               for( int j = 0 ; j < addressBookListDiff.size() ; j++ ) {

                    AddressBook addressBookDiff = addressBookListDiff.get(j);
                    AddressBook addressBookOri = addressBookListOriClone.get(j);
                    if(addressBookDiff.getIsChecked() != addressBookOri.getIsChecked() || !addressBookDiff.getName().equals(addressBookOri.getName())) {

                        returnStatus = true;
                        return returnStatus;

                    }

               }

            }

        }*/

        // 데이터 비교 로직 끝
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        return returnStatus;

    }

    /**
     * 기획서에는 날짜 필드 제외하고 하나라도 입력이 되어 있으면 저장한다라고
     * 나와 있기 때문에 한개라도 값이 있으면 값 밸리데이션에 문제가 없다고 생각한다.
     * @return boolean
     */
    private boolean isInsertOrUpdateAble() {

        boolean returnStatus = false;

        String meetingName = mEditTextMeetingName.getText().toString();     // 모임명
        String mount = mEditTextMount.getText().toString();                 // 수입
        String accountHoler = mEditTextAccountHoler.getText().toString();   // 예금주
        String bank = mEditTextBank.getText().toString();                   // 은행
        String accountNumber = mEditTextAccountNumber.getText().toString(); // 계좌번호
        String memo = mEditTextMemo.getText().toString();                   // 메모

        Log.d("DEBUG::StringUtil.isNotEmpty(meetingName)", String.valueOf(StringUtil.isNotEmpty(meetingName)));
        Log.d("DEBUG::StringUtil.isNotEmpty(mount)", String.valueOf(StringUtil.isNotEmpty(mount)));
        Log.d("DEBUG::StringUtil.isNotEmpty(accountHoler)", String.valueOf(StringUtil.isNotEmpty(accountHoler)));
        Log.d("DEBUG::StringUtil.isNotEmpty(bank)", String.valueOf(StringUtil.isNotEmpty(bank)));
        Log.d("DEBUG::StringUtil.isNotEmpty(accountNumber)", String.valueOf(StringUtil.isNotEmpty(accountNumber)));
        Log.d("DEBUG::StringUtil.isNotEmpty(memo)", String.valueOf(StringUtil.isNotEmpty(memo)));
        Log.d("DEBUG::USE_MODE_VALUE_WRIT.equals(mMode)", mMode);

        boolean mCasualMeetingStepListNotEmptyStatus = false;
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_csl_step);
        for(int idx = 0 ; idx < linearLayout.getChildCount() ; idx++ ) {
            CasualMeetingStepView CasualMeetingStepViewTemp = (CasualMeetingStepView) linearLayout.getChildAt(idx);
            if(StringUtil.isNotEmpty(CasualMeetingStepViewTemp.getStepArea()) || StringUtil.isNotEmpty(CasualMeetingStepViewTemp.getStepAmount())){
                mCasualMeetingStepListNotEmptyStatus = true;
                break;
            }
        }

        if(StringUtil.isNotEmpty(meetingName) || StringUtil.isNotEmpty(mount) || StringUtil.isNotEmpty(accountHoler) || StringUtil.isNotEmpty(bank)
                || StringUtil.isNotEmpty(accountNumber) || StringUtil.isNotEmpty(memo) ||
                (USE_MODE_VALUE_WRIT.equals(mMode) &&
                    (StringUtil.isNotEmpty(mCasualMeetingStepViewFirst.getStepArea()) == true || StringUtil.isNotEmpty(mCasualMeetingStepViewFirst.getStepAmount()) == true)
                ) || (!USE_MODE_VALUE_WRIT.equals(mMode) && mCasualMeetingStepListNotEmptyStatus)
        ) {

            returnStatus = true;
        }

        return returnStatus;

    }

    /**
     * 해당 미팅을 저장한다.
     * @param successMesaage
     */
    private void saveCasualMeeting(String successMesaage) {

        final Date date = DateUtil.getYearMonthDayStringToDate(mTextViewCslDate.getText().toString()); // 모임등록일
        final String meetingName = mEditTextMeetingName.getText().toString(); // 모임명
        final String mount = mEditTextMount.getText().toString(); // 수입
        final String accountHoler = mEditTextAccountHoler.getText().toString(); // 예금주
        final String bank = mEditTextBank.getText().toString(); // 은행
        final String accountNumber = mEditTextAccountNumber.getText().toString(); // 계좌번호
        final String memo = mEditTextMemo.getText().toString(); // 메모

        int savedSeq = -1;

        if (USE_MODE_VALUE_WRIT.equals(mMode)) { // 쓰기 모드 일때

            mRealm.beginTransaction();

            CasualMeeting casualMeeting = mRealm.createObject(CasualMeeting.class);
            savedSeq = CasualMeeting.generateSequence(mRealm);
            casualMeeting.setSeq(savedSeq);
            casualMeeting.setName(meetingName);
            casualMeeting.setDate(date);
            casualMeeting.setMount(mount);
            casualMeeting.setBank(bank);
            casualMeeting.setAccountNumber(accountNumber);
            casualMeeting.setMemo(memo);
            casualMeeting.setAccountHolder(accountHoler);

            RealmList<AddressBook> addressBookList = new RealmList<AddressBook>();
            for (String name : mMemberList) {
                AddressBook addressBook = mRealm.createObject(AddressBook.class);
                addressBook.setName(name);
                addressBookList.add(addressBook);
            }

            RealmList<CasualMeetingStep> casualMeetingStepList = new RealmList<CasualMeetingStep>();
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_csl_step);
            int idx = 0;
            for(CasualMeetingStep casualMeetingStepSub : mCasualMeetingStepList) {

                RealmList<AddressBook> casualStepAddressBookClone = new RealmList<AddressBook>();
                RealmList<AddressBook> casualStepAddressBookSub = casualMeetingStepSub.getAddressBookList();
                for(AddressBook addressBookSub : casualStepAddressBookSub) {
                    AddressBook addressBookClone = mRealm.createObject(AddressBook.class);
                    addressBookClone.setName(addressBookSub.getName());
                    addressBookClone.setIsChecked(addressBookSub.getIsChecked());
                    casualStepAddressBookClone.add(addressBookClone);
                }
                CasualMeetingStep casualMeetingStepClone = mRealm.createObject(CasualMeetingStep.class);
                casualMeetingStepClone.setName(casualMeetingStepSub.getName());
                CasualMeetingStepView CasualMeetingStepViewTemp = (CasualMeetingStepView) linearLayout.getChildAt(idx);
                casualMeetingStepClone.setAmount(CasualMeetingStepViewTemp.getStepAmount());
                casualMeetingStepClone.setArea(CasualMeetingStepViewTemp.getStepArea());
                casualMeetingStepClone.setAddressBookList(casualStepAddressBookClone);
                casualMeetingStepList.add(casualMeetingStepClone);
                idx++;
            }
            casualMeeting.setCasualMeetingStepList(casualMeetingStepList);
            casualMeeting.setAddressBookList(addressBookList);

            mRealm.commitTransaction();
            Toast.makeText(getBaseContext(), successMesaage, Toast.LENGTH_SHORT).show();

            RealmQuery<CasualMeeting> query = mRealm.where(CasualMeeting.class);
            mCasualMeeting = query.equalTo(SEQ_KEY, savedSeq).findFirst();
            mMode = USE_MODE_VALUE_UPDT;
            /*mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    CasualMeeting casualMeeting = realm.createObject(CasualMeeting.class);
                    casualMeeting.setSeq(CasualMeeting.generateSequence(realm));
                    casualMeeting.setName(meetingName);
                    casualMeeting.setDate(date);
                    casualMeeting.setMount(mount);
                    casualMeeting.setBank(bank);
                    casualMeeting.setAccountNumber(accountNumber);
                    casualMeeting.setMemo(memo);
                    casualMeeting.setAccountHolder(accountHoler);

                    RealmList<AddressBook> addressBookList = new RealmList<AddressBook>();
                    for (String name : mMemberList) {
                        AddressBook addressBook = realm.createObject(AddressBook.class);
                        addressBook.setName(name);
                        addressBookList.add(addressBook);
                    }

                    RealmList<CasualMeetingStep> casualMeetingStepList = new RealmList<CasualMeetingStep>();
                    LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_csl_step);
                    int idx = 0;
                    for(CasualMeetingStep casualMeetingStepSub : mCasualMeetingStepList) {

                        RealmList<AddressBook> casualStepAddressBookClone = new RealmList<AddressBook>();
                        RealmList<AddressBook> casualStepAddressBookSub = casualMeetingStepSub.getAddressBookList();
                        for(AddressBook addressBookSub : casualStepAddressBookSub) {
                            AddressBook addressBookClone = realm.createObject(AddressBook.class);
                            addressBookClone.setName(addressBookSub.getName());
                            addressBookClone.setIsChecked(addressBookSub.getIsChecked());
                            casualStepAddressBookClone.add(addressBookClone);
                        }
                        CasualMeetingStep casualMeetingStepClone = realm.createObject(CasualMeetingStep.class);
                        casualMeetingStepClone.setName(casualMeetingStepSub.getName());
                        CasualMeetingStepView CasualMeetingStepViewTemp = (CasualMeetingStepView) linearLayout.getChildAt(idx);
                        casualMeetingStepClone.setAmount(CasualMeetingStepViewTemp.getStepAmount());
                        casualMeetingStepClone.setArea(CasualMeetingStepViewTemp.getStepArea());
                        casualMeetingStepClone.setAddressBookList(casualStepAddressBookClone);
                        casualMeetingStepList.add(casualMeetingStepClone);
                        idx++;
                    }
                    casualMeeting.setCasualMeetingStepList(casualMeetingStepList);
                    casualMeeting.setAddressBookList(addressBookList);

                }
            }, new Realm.Transaction.Callback() {
                @Override
                public void onSuccess() {
                    Toast.makeText(getBaseContext(), getString(R.string.toast_casual_meeting_regist_success_txt), Toast.LENGTH_SHORT).show();
                    finish();
                }

                @Override
                public void onError(Exception e) {
                    Toast.makeText(getBaseContext(), getString(R.string.toast_casual_meeting_regist_fail_txt) + e.toString(), Toast.LENGTH_SHORT).show();
                    Log.d("ERROR", e.toString());
                }
            });*/

        } else { // 수정 모드 일때

            boolean isDataNotSameStatus = isDataNotSame();

            if(isDataNotSameStatus == true) {

                mRealm.beginTransaction();
                mCasualMeeting.setName(meetingName);
                mCasualMeeting.setDate(date);
                mCasualMeeting.setMount(mount);
                mCasualMeeting.setBank(bank);
                mCasualMeeting.setAccountNumber(accountNumber);
                mCasualMeeting.setMemo(memo);
                mCasualMeeting.setAccountHolder(accountHoler);
                RealmList<AddressBook> addressBookList = new RealmList<AddressBook>();
                for (String name : mMemberList) {
                    AddressBook addressBook = mRealm.createObject(AddressBook.class);
                    addressBook.setName(name);
                    addressBookList.add(addressBook);
                }
                RealmList<CasualMeetingStep> casualMeetingStepList = new RealmList<CasualMeetingStep>();
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_csl_step);
                int idx = 0;
                for (CasualMeetingStep casualMeetingStepSub : mCasualMeetingStepList) {

                    RealmList<AddressBook> casualStepAddressBookClone = new RealmList<AddressBook>();
                    RealmList<AddressBook> casualStepAddressBookSub = casualMeetingStepSub.getAddressBookList();
                    for (AddressBook addressBookSub : casualStepAddressBookSub) {
                        AddressBook addressBookClone = mRealm.createObject(AddressBook.class);
                        addressBookClone.setName(addressBookSub.getName());
                        addressBookClone.setIsChecked(addressBookSub.getIsChecked());
                        casualStepAddressBookClone.add(addressBookClone);
                    }
                    CasualMeetingStep casualMeetingStepClone = mRealm.createObject(CasualMeetingStep.class);
                    casualMeetingStepClone.setName(casualMeetingStepSub.getName());
                    CasualMeetingStepView CasualMeetingStepViewTemp = (CasualMeetingStepView) linearLayout.getChildAt(idx);
                    casualMeetingStepClone.setAmount(CasualMeetingStepViewTemp.getStepAmount());
                    casualMeetingStepClone.setArea(CasualMeetingStepViewTemp.getStepArea());
                    casualMeetingStepClone.setAddressBookList(casualStepAddressBookClone);
                    casualMeetingStepList.add(casualMeetingStepClone);
                    idx++;
                }
                mCasualMeeting.setCasualMeetingStepList(casualMeetingStepList);
                mCasualMeeting.setAddressBookList(addressBookList);
                mRealm.commitTransaction();

                Toast.makeText(getBaseContext(), successMesaage, Toast.LENGTH_SHORT).show();

            }

        }

    }

    /**
     * 주소록 엑티비티에서 받은 참여자 명단 리스트를 가지고 모임 차수 및 전체 참여자 목록에 업데이트 한다.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_ADDRESS_BOOK_ACTIVITY:
                    ArrayList<String> addressNameList = data.getExtras().getStringArrayList(REQUEST_PARAM_ADDRESS_BOOK_ACTIVITY);
                    for (String name : addressNameList) {
                        mMemberList.add(name);
                        putMember(name);
                    }
                    showCasualMeetingMemberStepSubView();
                    break;
            }
        }
    }

    /**
     * H/W 취소키 누르는 경우 자동 저장처리 한다. 단 한 개라도 등록된 정보가 있는 경우에만 자동 저장됨.(날짜만 입력된 경우 제외)
     */
    @Override
    public void onBackPressed() {

        boolean isProcessAbleStatus = isInsertOrUpdateAble();
        if(isProcessAbleStatus == true) {

            saveCasualMeeting(getString(R.string.toast_casual_meeting_auto_save_success_txt));
            finish();
            //Toast.makeText(getBaseContext(), getString(R.string.toast_casual_meeting_auto_save_success_txt), Toast.LENGTH_SHORT).show();

        } else {

            //Toast.makeText(getBaseContext(), getString(R.string.toast_casual_meeting_auto_save_validation_fail_txt) , Toast.LENGTH_SHORT).show();

        }
        super.onBackPressed();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    public void deleteStep(CasualMeetingStep mCasualMeetingStep) {

        int removeIndex = -1;
        for(int i = 0 ; i < mCasualMeetingStepList.size() ; i++ ) {
            CasualMeetingStep casualMeetingStep = mCasualMeetingStepList.get(i);
            if(casualMeetingStep.equals(mCasualMeetingStep)){
                removeIndex = i;
            }
        }

        if(removeIndex != -1) {
            mCasualMeetingStepList.remove(removeIndex);
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_csl_step);
            CasualMeetingStepView casualMeetingStepView = (CasualMeetingStepView) linearLayout.getChildAt(removeIndex);
            linearLayout.removeView(casualMeetingStepView);
            int nextChildCount = linearLayout.getChildCount() + 1;
            TextView textView = (TextView) findViewById(R.id.btn_add_csl_step);
            textView.setText(String.format(getString(R.string.label_casual_meeting_step_format_txt_1), nextChildCount));
        }
        for(int i = 0 ; i < mCasualMeetingStepList.size() ; i++ ) {
            CasualMeetingStep casualMeetingStep = mCasualMeetingStepList.get(i);
            casualMeetingStep.setName(String.format(getString(R.string.label_casual_meeting_step_format_txt_2), i + 1));
        }
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_csl_step);
        int childCount = linearLayout.getChildCount();
        Log.d("DELETE-STEP", String.format("removeIndex => %s / childCount => %s / mCasualMeetingStepList.size() => %s", removeIndex, childCount, mCasualMeetingStepList.size()));
        if(childCount > removeIndex) {
            for (int i = 0; i < childCount; i++) {
                CasualMeetingStepView casualMeetingStepView = (CasualMeetingStepView) linearLayout.getChildAt(i);
                casualMeetingStepView.setStepName(String.format(getString(R.string.label_casual_meeting_step_format_txt_2), i + 1));
            }
        }

    }
}