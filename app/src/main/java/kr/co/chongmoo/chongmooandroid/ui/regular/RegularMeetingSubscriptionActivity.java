package kr.co.chongmoo.chongmooandroid.ui.regular;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.IconPageIndicator;

import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.common.Constant;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.util.IabHelper;
import kr.co.chongmoo.chongmooandroid.util.IabResult;
import kr.co.chongmoo.chongmooandroid.util.Purchase;

/**
 *
 * 결제 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class RegularMeetingSubscriptionActivity extends BaseSubActivity {

    private TextView mTextInfo;

    private LinearLayout mRelativeLayoutBtnRglBtomMonth;
    private RelativeLayout mRelativeLayoutBtnRglBtomYear;

    private TextView mTextViewBtnGoogleUseage;

    private ViewPager mViewPagerPagerRglMetSubs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regular_meeting_subscription);
        setTitle(getString(R.string.title_activity_regular_meeting_subscription));

        mTextInfo = (TextView) findViewById(R.id.text_info);
        mViewPagerPagerRglMetSubs = (ViewPager) findViewById(R.id.pager_rgl_met_subs);
        mViewPagerPagerRglMetSubs.setAdapter(new RegularMeetingSubscriptionPagerAdapter(getSupportFragmentManager()));
        IconPageIndicator iconPageIndicator = (IconPageIndicator) findViewById(R.id.indicator);
        iconPageIndicator.setViewPager(mViewPagerPagerRglMetSubs);

        mRelativeLayoutBtnRglBtomMonth = (LinearLayout) findViewById(R.id.btn_rgl_btom_month);
        mRelativeLayoutBtnRglBtomMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InAppBuyItem_U("monthlyproduct");
            }
        });

        mRelativeLayoutBtnRglBtomYear = (RelativeLayout) findViewById(R.id.btn_rgl_btom_year);
        mRelativeLayoutBtnRglBtomYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InAppBuyItem_U("yearlyproduct");
            }
        });

        mTextInfo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                InAppBuyItem_U("test1");
                return true;
            }
        });

        mTextViewBtnGoogleUseage = (TextView) findViewById(R.id.btn_google_useage);
        mTextViewBtnGoogleUseage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://support.google.com/googleplay/answer/2476088?hl=ko");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                startActivity(intent);
            }
        });

        InAppInit_U(Constant.base64EncodedPublicKey, true);

    }

    public void InAppBuyItem_U(final String strItemId) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                /*
                 * TODO: for security, generate your payload here for
                 * verification. See the comments on verifyDeveloperPayload()
                 * for more info. Since this is a SAMPLE, we just use an empty
                 * string, but on a production app you should carefully generate
                 * this.
                 */
                String payload = "woojinsoft@gmali.com";

                mHelper.launchPurchaseFlow(RegularMeetingSubscriptionActivity.this, strItemId, IabHelper.ITEM_TYPE_SUBS, 1001, mPurchaseFinishedListener, payload);

                Log.d("myLog", "InAppBuyItem_U " + strItemId);
            }
        });
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d("myLog", "Purchase finished: " + result + ", purchase: " + purchase);  //결제 완료 되었을때 각종 결제 정보들을 볼 수 있습니다. 이정보들을 서버에 저장해 놓아야 결제 취소시 변경된 정보를 관리 할 수 있을것 같습니다~

            if (purchase != null) {
                if (!verifyDeveloperPayload(purchase)) {
                    Log.d("myLog", "Error purchasing. Authenticity verification failed.");
                }
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);

            } else {
                Toast.makeText(RegularMeetingSubscriptionActivity.this, String.format(getString(R.string.toast_payment_fail_2), String.valueOf(result.getResponse())), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d("myLog", "Consumption finished. Purchase: " + purchase + ", result: " + result);
            SendConsumeResult(purchase, result);
            finish();
        }
    };

    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct.
         * It will be the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase
         * and verifying it here might seem like a good approach, but this will
         * fail in the case where the user purchases an item on one device and
         * then uses your app on a different device, because on the other device
         * you will not have access to the random string you originally
         * generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different
         * between them, so that one user's purchase can't be replayed to
         * another user.
         *
         * 2. The payload must be such that you can verify it even when the app
         * wasn't the one who initiated the purchase flow (so that items
         * purchased by the user on one device work on other devices owned by
         * the user).
         *
         * Using your own server to store and verify developer payloads across
         * app installations is recommended.
         */

        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("myLog", "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (requestCode == 1001) {
            // Pass on the activity result to the helper for handling
            if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
                // not handled, so handle it ourselves (here's where you'd
                // perform any handling of activity results not related to
                // in-app
                // billing...
                super.onActivityResult(requestCode, resultCode, data);
            } else {
                Log.d("myLog", "onActivityResult handled by IABUtil.");
//                Toast.makeText(this, "onActivityResult handled by IABUtil.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
