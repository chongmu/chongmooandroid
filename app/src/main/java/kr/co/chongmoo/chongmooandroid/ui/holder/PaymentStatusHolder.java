package kr.co.chongmoo.chongmooandroid.ui.holder;

import android.app.Activity;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.model.Exemption;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.MonthlyFee;
import kr.co.chongmoo.chongmooandroid.model.PaymentStatus;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.ui.regular.PaymentStatusFragment;
import kr.co.chongmoo.chongmooandroid.ui.regular.RegularMeetingActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.setting.MemberManagementActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.setting.MemberManagementWriteActivity;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.widget.PaymentStateView;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 회비납부 상태 목록 holder
 *
 * @author  Hoon Jang
 * @since   2016.04.06
 */
public class PaymentStatusHolder extends RecyclerView.ViewHolder{
    public static final int RESOURCE_ID = R.layout.recycler_item_payment_status;

    public ViewGroup itemViewGroup;
    public TextView textName;
    public LinearLayout layoutFooter;

    public PaymentStatusHolder(View itemView) {
        super(itemView);

        itemViewGroup = (ViewGroup) ((ViewGroup)itemView).getChildAt(0);
        textName = (TextView)itemView.findViewById(R.id.text_name);
        layoutFooter = (LinearLayout) itemView.findViewById(R.id.layout_footer);

        PaymentStateView paymentStateView1 = new PaymentStateView(itemView.getContext());
        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        layoutParams1.rightMargin= 1;
        paymentStateView1.setLayoutParams(layoutParams1);
        itemViewGroup.addView(paymentStateView1);

        PaymentStateView paymentStateView2 = new PaymentStateView(itemView.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        layoutParams2.rightMargin= 1;
        paymentStateView2.setLayoutParams(layoutParams2);
        itemViewGroup.addView(paymentStateView2);

        PaymentStateView paymentStateView3 = new PaymentStateView(itemView.getContext());
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        layoutParams3.rightMargin= 1;
        paymentStateView3.setLayoutParams(layoutParams3);
        itemViewGroup.addView(paymentStateView3);

        PaymentStateView paymentStateView4 = new PaymentStateView(itemView.getContext());
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        layoutParams4.rightMargin= 1;
        paymentStateView4.setLayoutParams(layoutParams4);
        itemViewGroup.addView(paymentStateView4);

        PaymentStateView paymentStateView5 = new PaymentStateView(itemView.getContext());
        paymentStateView5.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1));
        itemViewGroup.addView(paymentStateView5);
    }

    public static class CommonCoordinator implements BeautifulRecyclerAdapter.Coordinator<PaymentStatusHolder, Member>{
        private Activity mActivity;
        private Fragment mFragment;
        private Realm mRealm;
        private int mMode;
        private RecyclerView mRecyclerView;

        public CommonCoordinator(Activity activity, Fragment fragment, Realm realm, int mode, RecyclerView recyclerView) {
            this.mActivity = activity;
            this.mFragment = fragment;
            this.mRealm = realm;
            this.mMode = mode;
            mRecyclerView = recyclerView;
        }

        @Override
        public void coordinate(BeautifulRecyclerAdapter<PaymentStatusHolder, Member> adapter, PaymentStatusHolder holder, List<Member> itemList, int position) {
            final Member item = itemList.get(position);

            String name = item.getName();
            if(name.length() > 5){
                name = name.substring(0, 5);
            }

            if(item.getOutDate() == null)
                holder.textName.setText(name);
            else
                holder.textName.setText("탈퇴회원\n(" + name.substring(0, name.length()-1) + "*)");

            holder.textName.setSingleLine(item.getOutDate() == null);
            holder.textName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(item.getOutDate() != null){
                        return ;
                    }

                    Intent intent = new Intent(mActivity, MemberManagementWriteActivity.class);
                    intent.putExtra(MemberManagementWriteActivity.EXTRA_MODE, MemberManagementWriteActivity.MODE_READ);
                    intent.putExtra("SEQ", item.getSeq());
                    mFragment.startActivityForResult(intent, MemberManagementActivity.REQUEST_MEMBER_WRITE);
                }
            });

            holder.layoutFooter.setVisibility(itemList.size()-1 == position ? View.VISIBLE : View.GONE);

            RealmQuery<RegularMeeting> meetingRealmQuery = mRealm.where(RegularMeeting.class);
            RegularMeeting regularMeeting = meetingRealmQuery.equalTo("seq", ((RegularMeetingActivity) mActivity).getSeq()).findFirst();
            RealmList<MonthlyFee> monthlyFeeList = regularMeeting.getMonthlyFeeList();

            RealmQuery<Income> query =  mRealm.where(Income.class);
            RealmResults<Income> incomeList = query.equalTo("memberSeq", item.getSeq()).findAllSorted("date", Sort.ASCENDING);

            int selectedYear = ((PaymentStatusFragment)mFragment).getYear();
            int selectedMonth = ((PaymentStatusFragment)mFragment).getMonth();

            int startYear = DateUtil.getYear(item.getStartDate());
            int startMonth = DateUtil.getMonth(item.getStartDate());

            int outYear = DateUtil.getYear(item.getOutDate());
            int outMonth = DateUtil.getMonth(item.getOutDate());

            RealmList<Exemption> exemptionList = item.getExemptionList();
            for(int i=4; i>=0; i--){
                int year = selectedYear;
                int month = selectedMonth-i;
                if(month < 0){
                    year--;
                    month = 12 + month;
                }

                int status = PaymentStatus.STATUS_NOTHING;
                Income income = null;
                long amount = 0;
                boolean isModify = false;

                if(startYear < year || (startYear == year && startMonth <= month)){
                    if(outYear > -1 && (outYear < year || (outYear == year && outMonth < month))) {
                        status = PaymentStatus.STATUS_NOTHING;
                    }
                    else{
                        boolean isExemption = false;
                        if(exemptionList != null){
                            for(Exemption exemption : exemptionList) {
                                if (Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", year, month + 1))) {
                                    isExemption = true;
                                }
                            }
                        }

                        int s = monthlyFeeList.size();
                        for (MonthlyFee monthlyFee : monthlyFeeList) {
                            if (Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", year, month + 1))) {
                                s--;
                            }
                        }

                        if(s == 0){
                            isModify = true;
                        }


                        if(isExemption){
                            status = PaymentStatus.STATUS_EXEMPTION;
                        }
                        else{
                            status = PaymentStatus.STATUS_NOT_PAID;

                            ArrayList<Income> list = new ArrayList<>();
                            for(Income ic : incomeList){
                                list.add(new Income(ic));
                            }

                            int y = startYear;
                            int m = startMonth;
                            long fee = 0;
                            loop:
                            while(y < year || (y == year && m <= month)) {
                                for (Exemption exemption : exemptionList) {
                                    if (Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", y, m + 1))) {
                                        m++;
                                        if (m > 11) {
                                            m %= 12;
                                            y++;
                                        }
                                        continue loop;
                                    }
                                }

                                int size = monthlyFeeList.size();
                                for (MonthlyFee monthlyFee : monthlyFeeList) {
                                    if (Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m + 1))) {
                                        fee = monthlyFee.getAmount();
                                        size--;
                                    }
                                }

                                if (list.size() > 0) {
                                    Income currnetIncome = list.get(0);
                                    currnetIncome.setAmount(currnetIncome.getAmount() - fee);
                                    long result = currnetIncome.getAmount();
                                    if (result == 0)
                                        list.remove(0);

                                    status = mMode; //PaymentStatus.STATUS_PAID;
                                    for (Income ic : incomeList) {
                                        if (ic.getSeq() == currnetIncome.getSeq()) {
                                            income = ic;
                                        }
                                    }
                                    amount = fee;
                                    isModify = (size == 0);
                                } else {
                                    status = PaymentStatus.STATUS_NOT_PAID;
                                    income = null;
                                    amount = 0;
//                                    isModify = false;
                                    break;
                                }

                                m++;
                                if (m > 11) {
                                    m %= 12;
                                    y++;
                                }
                            }
                        }
                    }
                }

                PaymentStatus paymentStatus = new PaymentStatus(status, item, income, amount, year, month);
                paymentStatus.setIsNew(startYear == year && startMonth == month);
                paymentStatus.setIsOut(outYear == year && outMonth == month);
                paymentStatus.setIsModify(isModify);

                PaymentStateView paymentStateView = (PaymentStateView)holder.itemViewGroup.getChildAt(5-i);
                paymentStateView.setFragment(mFragment);
                paymentStateView.setRecyclerView(mRecyclerView);
                paymentStateView.setPosition(position);
                paymentStateView.setPaymentStatus(paymentStatus);
            }
        }
    }

}
