package kr.co.chongmoo.chongmooandroid.ui.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.tsengvn.typekit.TypekitContextWrapper;

import com.woojinsoft.chongmoo.android.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kr.co.chongmoo.chongmooandroid.common.ChongmooApplication;
import kr.co.chongmoo.chongmooandroid.common.Constant;
import kr.co.chongmoo.chongmooandroid.ui.MainActivity;
import kr.co.chongmoo.chongmooandroid.util.IabHelper;
import kr.co.chongmoo.chongmooandroid.util.IabResult;
import kr.co.chongmoo.chongmooandroid.util.Inventory;
import kr.co.chongmoo.chongmooandroid.util.Purchase;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;

/**
 *
 * Base Activity
 *
 * @author  Hoon Jang
 * @since   2016.04.01
 */
public class BaseActivity extends AppCompatActivity{
    public static final String ACTION_FINISH = "android.intent.action.FINISH";

    private BroadcastReceiver mFinishReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction() == ACTION_FINISH)
                finish();
        }
    };

    public Tracker appTracker;

    protected IabHelper mHelper;
    private boolean mIsConsuming = false;
    private boolean isSubscription = false; //false

    private ChongmooDialog mChongmooDialog;

    private InputMethodManager mInputMethodManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        if (Constant.USE_GOOGLE_ANALYTICS) {
            GoogleAnalytics.getInstance(this).setDryRun(false);
            GoogleAnalytics.getInstance(this).getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
            appTracker = ((ChongmooApplication) getApplication()).getTracker();
            // Enable Display Features.
            appTracker.enableAdvertisingIdCollection(true);
            sendGaScreen();
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mFinishReceiver, new IntentFilter(BaseActivity.ACTION_FINISH));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    public void InAppInit_U(String strPublicKey, boolean bDebug) {
//        if(mChongmooDialog != null && mChongmooDialog.isShowing()){
//            return ;
//        }

        Log.d("myLog", "Creating IAB helper." + bDebug);
        mHelper = new IabHelper(this, strPublicKey);

        if (bDebug == true) {
            mHelper.enableDebugLogging(true, "IAB");
        }

        mChongmooDialog = new ChongmooDialog(this, ChongmooDialog.MODE_LOADING);
        mChongmooDialog.setCancelable(false);
        mChongmooDialog.show();
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            @Override
            public void onIabSetupFinished(IabResult result) {
                boolean bInit = result.isSuccess();
                //Log.d("myLog", "IAB Init " + bInit + result.getMessage());

                if (bInit == true) {

                    Log.d("myLog", "Querying inventory.");
                    mHelper.queryInventoryAsync(mGotInventoryListener);

                } else {
                    mChongmooDialog.dismiss();
                    Toast.makeText(BaseActivity.this, R.string.toast_payment_fail, Toast.LENGTH_SHORT).show();
                }
                //Toast.makeText(RegularMeetingSubscriptionActivity.this, "InAppInit_U", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            isSubscription = false;
            if (result.isFailure()) {
                Log.d("myLog", "Failed to query inventory: " + result);
                Toast.makeText(BaseActivity.this, "Failed to query inventory: ", Toast.LENGTH_SHORT).show();
                SendConsumeResult(null, result);
                onPaymentChecked(isSubscription);
                return;
            }

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */
            List<String> purchaseList = new ArrayList<>();
//            List<String> inAppList = inventory.getAllOwnedSkus(IabHelper.ITEM_TYPE_INAPP);
//            purchaseList.addAll(inAppList);
            List<String> subsList = inventory.getAllOwnedSkus(IabHelper.ITEM_TYPE_SUBS);
            purchaseList.addAll(subsList);
            for (String inAppSku : purchaseList) {
//                while(mIsConsuming){}
                Purchase purchase = inventory.getPurchase(inAppSku);
                Log.d("myLog", "Consumeing... " + inAppSku);
//                Toast.makeText(BaseActivity.this, "mGotInventoryListener : " + inAppSku, Toast.LENGTH_SHORT).show();
                mIsConsuming = true;
//                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                isSubscription = true;

                SharedPreferences pref = getSharedPreferences("chongmoo", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("PURCHASE", new Gson().toJson(purchase));
                editor.commit();
            }

            Log.d("myLog", "Query inventory was successful.");
//            Toast.makeText(BaseActivity.this, "Query inventory was successful.", Toast.LENGTH_SHORT).show();
            onPaymentChecked(isSubscription);
        }
    };

    private IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d("myLog", "Consumption finished. Purchase: " + purchase + ", result: " + result);
            SendConsumeResult(purchase, result);
            mIsConsuming = false;
        }
    };

    protected void SendConsumeResult(Purchase purchase, IabResult result) {
        JSONObject jsonObj = new JSONObject();

        try {
            StringBuilder builder = new StringBuilder();
            builder.append("\n\nResult : " + result.getResponse() + "\n");

            jsonObj.put("Result", result.getResponse());
            if (purchase != null) {
                jsonObj.put("OrderId", purchase.getOrderId());
                jsonObj.put("Sku", purchase.getSku());
                jsonObj.put("purchaseData", purchase.getOriginalJson());
                jsonObj.put("signature", purchase.getSignature());

                Log.d("myLog", "OrderId" + purchase.getOrderId());
                Log.d("myLog", "Sku" + purchase.getSku());
                Log.d("myLog", "purchaseData" + purchase.getOriginalJson());
                Log.d("myLog", "signature" + purchase.getSignature());

                builder.append("OrderId : " + purchase.getOrderId() + "\n");
                builder.append("Sku : " + purchase.getSku() + "\n");
                builder.append("purchaseData : " + purchase.getOriginalJson() + "\n");
                builder.append("signature : " + purchase.getSignature());
            }
            else{
                builder.append("purchase is null");
            }

//            Toast.makeText(this, builder.toString(), Toast.LENGTH_SHORT).show();
//            mTextInfo.append(builder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void hideKeyboard(){
        mInputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }

    protected void goHome(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public Boolean isNetWorkAble() {

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if(activeNetwork.isAvailable() && activeNetwork.isConnectedOrConnecting())
                return true;
        }
        return false;


//        ConnectivityManager manager = (ConnectivityManager) getSystemService (Context.CONNECTIVITY_SERVICE);
//        boolean isMobileAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isAvailable();
//        boolean isMobileConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
//        boolean isWifiAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isAvailable();
//        boolean isWifiConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
//
//        if ((isWifiAvailable == true && isWifiConnect == true) || (isMobileAvailable == true && isMobileConnect == true)){
//            return true;
//        }else{
//            return false;
//        }
    }


    /**
     * 현재 어플리케이션에 버젼 정보를 가져온다.
     * @return
     */
    protected String getApplicationVersionNumber() {

        String applicationVersionNumber = "";
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            applicationVersionNumber = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } finally {
            return applicationVersionNumber;
        }

    }

    public void showNetworkProblemDialog(final boolean isFinish) {

        final ChongmooDialog chongmooDialog = new ChongmooDialog(this);
        chongmooDialog.setMessage(getString(R.string.dialog_disable_network_text));
        chongmooDialog.setPositiveButton(getString(R.string.confirm), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFinish) {
                    finish();
                }
                chongmooDialog.dismiss();
            }
        });
        chongmooDialog.show();

    }

    public void onPaymentChecked(boolean isSubscription){
        mChongmooDialog.dismiss();
    }

    public boolean isSubscription() {
        return isSubscription;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mFinishReceiver);
        if (mHelper != null) {
            mHelper.dispose();
        }
    }

    public void sendAppStart() {
        if (Constant.USE_GOOGLE_ANALYTICS) {
            Tracker firstTracker = ((ChongmooApplication) getApplication()).getTracker(ChongmooApplication.TrackerName.COMMON_TRACKER);
            firstTracker.enableAutoActivityTracking(false);
            firstTracker.enableExceptionReporting(false);
            firstTracker.send(new HitBuilders.EventBuilder().setCategory(getString(R.string.ga_category_start)).setAction(getString(R.string.ga_action_start)).setLabel(getString(R.string.ga_label_start)).build());
        }
    }

    public void sendGaEvent(final String category, final String action, final String label) {
        if (Constant.USE_GOOGLE_ANALYTICS && appTracker != null) {
            appTracker.setScreenName(getClass().getName());
            appTracker.send(new HitBuilders.EventBuilder().setCategory(category)
                    .setAction(action).setLabel(label).build());
        }
    }

    public void sendGaEvent(final int categoryId, final String action, final String label) {
        if(Constant.USE_GOOGLE_ANALYTICS && appTracker != null) {
            appTracker.setScreenName(getClass().getName());
            appTracker.send(new HitBuilders.EventBuilder().setCategory(getString(categoryId))
                    .setAction(action).setLabel(label).build());
        }
    }

    public void sendGaScreen() {
        sendGaScreen(getClass().getName());
    }

    public void sendGaScreen(final String name) {
        appTracker.setScreenName(name);
        appTracker.send(new HitBuilders.AppViewBuilder().build());
    }
}
