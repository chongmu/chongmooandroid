package kr.co.chongmoo.chongmooandroid.ui.holder;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import com.woojinsoft.chongmoo.android.R;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.ui.regular.setting.MemberManagementActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.setting.MemberManagementWriteActivity;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 멤버 목록 holder
 *
 * @author  Hoon Jang
 * @since   2016.04.06
 */
public class MemberHolder extends RecyclerView.ViewHolder{
    public static final int RESOURCE_ID = R.layout.recycler_item_member;

    public TextView textNo;
    public TextView textName;
    public TextView textJoinDate;
    public TextView textMemo;

    public MemberHolder(View itemView) {
        super(itemView);

        textNo = (TextView) itemView.findViewById(R.id.text_no);
        textName = (TextView) itemView.findViewById(R.id.text_name);
        textJoinDate = (TextView) itemView.findViewById(R.id.text_join_date);
        textMemo = (TextView) itemView.findViewById(R.id.text_memo);
    }

    public static class CommonCoordinator implements BeautifulRecyclerAdapter.Coordinator<MemberHolder, Member>{
        private Activity mActivity;

        public CommonCoordinator(Activity mActivity) {
            this.mActivity = mActivity;
        }

        @Override
        public void coordinate(BeautifulRecyclerAdapter<MemberHolder, Member> adapter, MemberHolder holder, List<Member> itemList, int position) {
            final Member item = itemList.get(position);

            holder.textNo.setText((position + 1) + "");
            holder.textName.setText(item.getName());
            holder.textJoinDate.setText(DateUtil.getDateToYearMonthDayString(item.getRegistDate()));
            holder.textMemo.setText(item.getMemo());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, MemberManagementWriteActivity.class);
                    intent.putExtra(MemberManagementWriteActivity.EXTRA_MODE, MemberManagementWriteActivity.MODE_READ);
                    intent.putExtra("SEQ", item.getSeq());
                    mActivity.startActivityForResult(intent, MemberManagementActivity.REQUEST_MEMBER_WRITE);
                }
            });
        }
    }

}
