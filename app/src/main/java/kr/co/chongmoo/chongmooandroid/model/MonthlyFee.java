package kr.co.chongmoo.chongmooandroid.model;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

/**
 *
 * 월 회비 저장을 위한 모델
 *
 * @author  Hoon Jang
 * @since   2016.04.25
 */
@RealmClass
public class MonthlyFee extends RealmObject{

    @Required private String date;
    private long amount;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
