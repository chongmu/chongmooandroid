package kr.co.chongmoo.chongmooandroid.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.woojinsoft.chongmoo.android.R;

/**
 * 버전 체크 다이얼로그
 * Created by ryu on 2016-04-26.
 */
public class VersionDialog extends Dialog {

    private TextView mTextViewVerDalTitle;
    private TextView mTextViewVerDalPositive;
    private TextView mTextViewVerDalNegative;

    public VersionDialog(Context context) {
        super(context);
        init();
    }

    public VersionDialog(Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    protected VersionDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init();
    }

    /**
     * 제목, OK, CANCEL 버튼 로드 및 다이얼로그 기본 셋팅 적용
     */
    public void init() {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_version);
        setCanceledOnTouchOutside(false);
        mTextViewVerDalTitle = (TextView) findViewById(R.id.txt_ver_dal_title);
        mTextViewVerDalPositive = (TextView) findViewById(R.id.txt_ver_dal_positive);
        mTextViewVerDalNegative = (TextView) findViewById(R.id.txt_ver_dal_negative);
        mTextViewVerDalNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        }); // 추가로 셋팅하지 않으면 취소 버튼 클릭시 다이얼로그 종료

    }

    /**
     * 다이얼로그 텍스트 적용을 한다.
     * @param input
     */
    public void setDialogText(String input) {
        mTextViewVerDalTitle.setText(input);
    }

    /**
     * OK 버튼 텍스트를 적용을 한다.
     * @param input
     */
    public void setPositiveButtonText(String input) {
        mTextViewVerDalPositive.setText(input);
    }

    /**
     * CANCEL 버튼 텍스트를 적용을 한다.
     * @param input
     */
    public void setNegativeButtonText(String input) {
        mTextViewVerDalNegative.setText(input);
    }

    /**
     * OK 버튼 클릭시 이벤트를 교체한다.
     * @param onClickListener
     */
    public void setPositiveButtonListener(View.OnClickListener onClickListener) {
        mTextViewVerDalPositive.setOnClickListener(onClickListener);
    }

    /**
     * CANCEL 버튼 클릭시 이벤트를 교체한다.
     * @param onClickListener
     */
    public void setNegativeButtonListener(View.OnClickListener onClickListener) {
        mTextViewVerDalNegative.setOnClickListener(onClickListener);
    }

}