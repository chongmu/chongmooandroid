package kr.co.chongmoo.chongmooandroid.ui.common;

import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.DecimalFormat;

/**
 * Created by ryu on 2016-04-24.
 */
public class MoneyCommaEditTextWather implements TextWatcher {

    public String DECIMAL_FORMATER = "###,###";

    private EditText mEditText;

    /** 임시 저장값 (콤마) */ private String strAmount = "";

    public MoneyCommaEditTextWather(EditText editText) {
        mEditText = editText;
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!s.toString().equals(strAmount)) { // StackOverflow 방지
            strAmount = makeStringComma(s.toString().replace(",", ""));
            mEditText.setText(strAmount);
            Editable e = mEditText.getText();
            Selection.setSelection(e, strAmount.length());
        }
    }

    protected String makeStringComma(String str) {    // 천단위 콤마 처리
        if (str.length() == 0)
            return "";
        long value = Long.parseLong(str);
        DecimalFormat decimalFormat = new DecimalFormat(DECIMAL_FORMATER);
        return decimalFormat.format(value);
    }

}
