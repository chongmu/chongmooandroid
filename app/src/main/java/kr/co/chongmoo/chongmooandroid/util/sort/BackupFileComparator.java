package kr.co.chongmoo.chongmooandroid.util.sort;

import java.util.Comparator;

import kr.co.chongmoo.chongmooandroid.model.BackupFile;

/**
 * 백업 복원 목록 파일 정렬 클래스
 */
public class BackupFileComparator implements Comparator<BackupFile> {

    @Override
    public int compare(BackupFile lhs, BackupFile rhs) {
        long ihsDate = Long.parseLong(lhs.getSortDate());
        long rhssDate = Long.parseLong(rhs.getSortDate());
        return (int)(rhssDate - ihsDate);
    }

}
