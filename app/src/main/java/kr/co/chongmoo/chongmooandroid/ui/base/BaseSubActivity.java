package kr.co.chongmoo.chongmooandroid.ui.base;

import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.woojinsoft.chongmoo.android.R;

/**
 *
 * Base Sub Activity
 *
 * @author  Hoon Jang
 * @since   2016.04.01
 */
public class BaseSubActivity extends BaseActivity {

    private RelativeLayout mLayoutTitle;
    private TextView mTextTitle;
    private Button mBtnClose;

    private FrameLayout mLayoutContent;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_base_sub);

        mLayoutTitle = (RelativeLayout)findViewById(R.id.layout_title);
        mLayoutContent = (FrameLayout)findViewById(R.id.layout_content);

        getLayoutInflater().inflate(layoutResID, mLayoutContent);

        mTextTitle = (TextView)findViewById(R.id.text_title);
        mBtnClose = (Button)findViewById(R.id.btn_close);
        mBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();            }
        });
    }

    @Override
    public void setTitle(int titleId) {
        setTitle(getText(titleId));
    }

    @Override
    public void setTitle(CharSequence title) {
        mTextTitle.setText(title);
    }

    public void setTitleBacgroundColor(int color){
        mLayoutTitle.setBackgroundColor(color);
    }
}
