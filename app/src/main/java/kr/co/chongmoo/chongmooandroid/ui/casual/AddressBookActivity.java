package kr.co.chongmoo.chongmooandroid.ui.casual;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.model.AddressBook;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.ui.holder.AddressBookHolder;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 * 전화번호부에서 주소록 리스트를 가져오는 화면
 */
public class AddressBookActivity extends BaseSubActivity {

    private RecyclerView mRecyclerAddressBook;
    private ViewGroup mViewGroupBtnAddrBoks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_book);
        setTitle(getString(R.string.title_activity_address_book));

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 주소록에 대한 로직을 가져와서 화면에 뿌리는 로직 시작
        final List<AddressBook> addressBookList = new ArrayList<AddressBook>();

        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        String[] projection = new String[] { ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME  + " COLLATE LOCALIZED ASC");
        int totalAddressBookListCount = cursor.getCount();

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String name = cursor.getString(1);
                if(name.matches("[\\w\\~\\-\\.]+@[\\w\\~\\-]+(\\.[\\w\\~\\-]+)+") == false) {
                    AddressBook addressBook = new AddressBook();
                    addressBook.setIsChecked(false);
                    addressBook.setName(name);
                    addressBookList.add(addressBook);
                }
            }
        }

        mRecyclerAddressBook = (RecyclerView) findViewById(R.id.recycler_address_book);
        mRecyclerAddressBook.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerAddressBook.setAdapter(new BeautifulRecyclerAdapter<>(this, AddressBookHolder.RESOURCE_ID, addressBookList, new AddressBookHolder.CommonCoordinator(this)));
        // 주소록에 대한 로직을 가져와서 화면에 뿌리는 로직 종료
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 주소록에서 선택한 사람을 번개미팅으로 목록을 넘기는 버튼에 대한 로직 시작
        mViewGroupBtnAddrBoks = (ViewGroup) findViewById(R.id.btn_submit_addresses);
        mViewGroupBtnAddrBoks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<String> checkedNameList = new ArrayList<String>();

                for(AddressBook addressBook : addressBookList) {
                    boolean isChecked = addressBook.getIsChecked();
                    if(isChecked == true) {
                        checkedNameList.add(addressBook.getName());
                    }
                }

                Bundle extra = new Bundle();
                Intent intent = new Intent();
                extra.putStringArrayList(CasualMeetingActivity.REQUEST_PARAM_ADDRESS_BOOK_ACTIVITY, checkedNameList);
                intent.putExtras(extra);
                setResult(RESULT_OK, intent);
                finish();

            }
        });
        // 주소록에서 선택한 사람을 번개미팅으로 목록을 넘기는 버튼에 대한 로직 끝
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }

}