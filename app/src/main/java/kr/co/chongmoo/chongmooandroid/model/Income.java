package kr.co.chongmoo.chongmooandroid.model;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

/**
 *
 * 수입/지출 모델
 *
 * @author  Hoon Jang
 * @since   2016.04.06
 */
@RealmClass
public class Income extends RealmObject {

    @PrimaryKey private int seq;

    private int meetingSeq;
    @Required private Date date;
    private int day;
    private int type;
    private long amount;
    private String category;
    private int memberSeq;
    private String content;
    private String memo;
    private byte[] imageFile;

    @Ignore private boolean isTop;

    public Income() {}

    public Income(Income income){
        seq = income.getSeq();
        meetingSeq = income.getMeetingSeq();
        date = income.getDate();
        day = income.getDay();
        type = income.getType();
        amount = income.getAmount();
        category = income.getCategory();
        memberSeq = income.getMemberSeq();
        content = income.getContent();
        memo = income.getMemo();
        imageFile = income.getImageFile();
    }


    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getMeetingSeq() {
        return meetingSeq;
    }

    public void setMeetingSeq(int meetingSeq) {
        this.meetingSeq = meetingSeq;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getMemberSeq() {
        return memberSeq;
    }

    public void setMemberSeq(int memberSeq) {
        this.memberSeq = memberSeq;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public byte[] getImageFile() {
        return imageFile;
    }

    public void setImageFile(byte[] imageFile) {
        this.imageFile = imageFile;
    }

    public boolean isTop() {
        return isTop;
    }

    public void setIsTop(boolean isTop) {
        this.isTop = isTop;
    }

    public static int generateSequence(Realm realm){
        RealmQuery<Income> query = realm.where(Income.class);
        Number number = query.max("seq");
        int seq = (number == null) ? 0 : number.intValue();
        return seq + 1;
    }
}

