package kr.co.chongmoo.chongmooandroid.ui.holder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.woojinsoft.chongmoo.android.R;

import java.io.File;
import java.util.List;

import kr.co.chongmoo.chongmooandroid.model.BackupFile;
import kr.co.chongmoo.chongmooandroid.ui.IntroActivity;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.setting.BackupFileListActivity;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;
import kr.co.chongmoo.chongmooandroid.widget.VersionDialog;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 * Created by ryu on 2016-04-29.
 */
public class BackupFileHolder extends RecyclerView.ViewHolder {

    public static final int RESOURCE_ID = R.layout.recycler_item_backup_file;

    public TextView mTextViewTxtBackupItemFileTitle;
    public TextView mTextViewTxtBackupItemFileMeta;
    public ImageView mImageViewBtnBackupItemDel;

    public BackupFileHolder(View itemView) {

        super(itemView);
        mTextViewTxtBackupItemFileTitle = (TextView) itemView.findViewById(R.id.txt_backup_item_file_title);
        mTextViewTxtBackupItemFileMeta = (TextView) itemView.findViewById(R.id.txt_backup_item_file_meta);
        mImageViewBtnBackupItemDel = (ImageView) itemView.findViewById(R.id.btn_backup_item_del);

    }

    public static class CommonCoordinator implements BeautifulRecyclerAdapter.Coordinator<BackupFileHolder, BackupFile> {

        private BackupFileListActivity mBackupFileListActivity;

        public CommonCoordinator(BackupFileListActivity backupFileListActivity) {
            this.mBackupFileListActivity = backupFileListActivity;
        }

        @Override
        public void coordinate(BeautifulRecyclerAdapter<BackupFileHolder, BackupFile> adapter, final BackupFileHolder holder, List<BackupFile> itemList, int position) {

            final BackupFile item = itemList.get(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String path = item.getAbsolutePath();
                    final VersionDialog versionDialog = new VersionDialog(mBackupFileListActivity);
                    versionDialog.setDialogText(mBackupFileListActivity.getString(R.string.dialog_backup_load_txt));
                    versionDialog.setPositiveButtonText(mBackupFileListActivity.getString(R.string.confirm));
                    versionDialog.setPositiveButtonListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String path = item.getAbsolutePath();
                            String fileName = item.getFileName();
                            int seq = item.getSeq();
                            versionDialog.dismiss();
                            boolean status = mBackupFileListActivity.restore(path, fileName, seq);

                            if(status == true) {

                                final ChongmooDialog chongmooDialog = new ChongmooDialog(mBackupFileListActivity);
                                chongmooDialog.setMessage(mBackupFileListActivity.getString(R.string.backup_restore_success_txt));
                                chongmooDialog.setPositiveButton(mBackupFileListActivity.getString(R.string.confirm), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        chongmooDialog.dismiss();

                                        v.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                LocalBroadcastManager.getInstance(mBackupFileListActivity).sendBroadcast(new Intent(BaseActivity.ACTION_FINISH));
                                                Intent intent = new Intent(mBackupFileListActivity, IntroActivity.class);
                                                mBackupFileListActivity.startActivity(intent);
                                            }
                                        }, 100);
                                        mBackupFileListActivity.finish();
                                    }
                                });
                                chongmooDialog.show();

                            } else {
                                Toast.makeText(mBackupFileListActivity, mBackupFileListActivity.getString(R.string.backup_restore_fail_txt), Toast.LENGTH_LONG).show();
                            }

                        }
                    });
                    versionDialog.setNegativeButtonText(mBackupFileListActivity.getString(R.string.cancel));
                    versionDialog.setNegativeButtonListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            versionDialog.dismiss();
                        }
                    });
                    versionDialog.show();
                }
            });

            holder.mTextViewTxtBackupItemFileTitle.setText(item.getFileName());
            holder.mTextViewTxtBackupItemFileMeta.setText(String.format("%s      %s kb", item.getDate(), item.getFileSize()));
            holder.mImageViewBtnBackupItemDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final VersionDialog versionDialog = new VersionDialog(mBackupFileListActivity);
                    versionDialog.setDialogText(mBackupFileListActivity.getString(R.string.dialog_backup_del_txt));
                    versionDialog.setPositiveButtonText(mBackupFileListActivity.getString(R.string.confirm));
                    versionDialog.setPositiveButtonListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String path = item.getAbsolutePath();
                            boolean status = new File(path).delete();
                            versionDialog.dismiss();
                            if (status) {
                                Toast.makeText(mBackupFileListActivity, mBackupFileListActivity.getString(R.string.backup_del_success_txt), Toast.LENGTH_SHORT).show();
                                mBackupFileListActivity.showBackupFileList();
                            } else {
                                Toast.makeText(mBackupFileListActivity, mBackupFileListActivity.getString(R.string.backup_del_fail_txt), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    versionDialog.setNegativeButtonText(mBackupFileListActivity.getString(R.string.cancel));
                    versionDialog.setNegativeButtonListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            versionDialog.dismiss();
                        }
                    });
                    versionDialog.show();

                }
            });

        }

    }

}