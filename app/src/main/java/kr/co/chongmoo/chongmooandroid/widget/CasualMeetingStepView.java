package kr.co.chongmoo.chongmooandroid.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.model.CasualMeeting;
import kr.co.chongmoo.chongmooandroid.model.CasualMeetingStep;
import kr.co.chongmoo.chongmooandroid.ui.casual.CasualMeetingActivity;
import kr.co.chongmoo.chongmooandroid.ui.common.MoneyCommaEditTextWather;

/**
 * 번개 모임 차수 관련 커스텀 뷰
 * Created by ??? on 2016-04-22.
 */
public class CasualMeetingStepView extends LinearLayout {

    private CasualMeetingStep mCasualMeetingStep;

    private TextView mTextViewStepName;
    private TextView mTextViewCasualStepMemberCount;
    private EditText mEditTextCasualStepMount;
    private EditText mEditTextCasualStepArea;
    private ImageView mImageViewBtnDelCasualMeetingStep;

    public CasualMeetingStepView(Context context) {
        super(context);
        init();
    }

    public CasualMeetingStepView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CasualMeetingStepView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CasualMeetingStepView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {

        inflate(getContext(), R.layout.view_casual_meeting_step, this);
        mTextViewStepName = (TextView) findViewById(R.id.txt_step_name);
        mTextViewCasualStepMemberCount = (TextView) findViewById(R.id.txt_cas_stp_mem_cnt);
        mEditTextCasualStepMount = (EditText) findViewById(R.id.txt_csl_step_mount);
        mEditTextCasualStepMount.addTextChangedListener(new MoneyCommaEditTextWather(mEditTextCasualStepMount));
        mEditTextCasualStepArea =  (EditText) findViewById(R.id.txt_csl_step_area);
        mImageViewBtnDelCasualMeetingStep = (ImageView) findViewById(R.id.btn_del_casual_meeting_step);
        mImageViewBtnDelCasualMeetingStep.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                ((CasualMeetingActivity)getContext()).deleteStep(mCasualMeetingStep);
                
            }
        });

    }

    public void setStepAmount(String stepAmount) {
        mEditTextCasualStepMount.setText(stepAmount);
    }

    public String getStepAmount() {
        return mEditTextCasualStepMount.getText().toString();
    }

    public String getStepArea() {
        return mEditTextCasualStepArea.getText().toString();
    }

    public void setStepArea(String stepArea) {
        mEditTextCasualStepArea.setText(stepArea);
    }

    public void setStepName(String stepName) {
        mTextViewStepName.setText(stepName);
    }

    public void setCasualMeetingStep(CasualMeetingStep casualMeetingStep) {
        this.mCasualMeetingStep = casualMeetingStep;
    }

    public CasualMeetingStep getCasualMeetingStep() {
        return this.mCasualMeetingStep;
    }

    public void setCasualStepMemberCount(int count) {
        mTextViewCasualStepMemberCount.setText(String.format(getContext().getString(R.string.label_casual_meeting_step_member_count_format_txt), count));
    }

    public void onDeleteStepButtoon(boolean status) {

        if (status == true) {
            mImageViewBtnDelCasualMeetingStep.setVisibility(VISIBLE);
        } else {
            mImageViewBtnDelCasualMeetingStep.setVisibility(GONE);
        }

    }

}