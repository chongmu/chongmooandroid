package kr.co.chongmoo.chongmooandroid.ui;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.woojinsoft.chongmoo.android.R;
import kr.co.chongmoo.chongmooandroid.model.HttpResponse;
import kr.co.chongmoo.chongmooandroid.model.MobileVersion;
import kr.co.chongmoo.chongmooandroid.network.RetrofitManager;
import kr.co.chongmoo.chongmooandroid.network.service.APIService;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 앱 시작 화면 엑티비티
 * 1 . 버전 체크를 해서 메인 엑티비티로 넘긴다.
 */
public class IntroActivity extends BaseActivity {

    public static final String VERSION_NUMBER_NONE = "NONE";
    public static final String VERSION_KEY ="VERSION_KEY";

    private ImageView mImageViewImgIntroLoading;
    private AnimationDrawable mAnimationDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        mImageViewImgIntroLoading = (ImageView) findViewById(R.id.img_intro_loading);
        mImageViewImgIntroLoading.setVisibility(View.VISIBLE);
        mImageViewImgIntroLoading.setBackgroundResource(R.drawable.animation_loading);
        mAnimationDrawable = (AnimationDrawable) mImageViewImgIntroLoading.getBackground();
        mAnimationDrawable.start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                    boolean requestAbleStatus = isNetWorkAble();
                    if(requestAbleStatus == true) {

                        APIService apiService = RetrofitManager.getInstance().create(APIService.class);
                        Call<HttpResponse<MobileVersion>> call = apiService.lastestVersion();
                        call.enqueue(new Callback<HttpResponse<MobileVersion>>() {

                            @Override
                            public void onResponse(Call<HttpResponse<MobileVersion>> call, Response<HttpResponse<MobileVersion>> response) {

                                int code = response.code();
                                if (code == 200) {

                                    Intent intent = new Intent(IntroActivity.this, MainActivity.class);
                                    HttpResponse<MobileVersion> httpResponse = response.body();
                                    String versionNumber = httpResponse.getResults().getVersionNumber();
                                    Bundle extra = new Bundle();
                                    extra.putString(VERSION_KEY, versionNumber);
                                    intent.putExtras(extra);
                                    intent.putExtra("IS_FIRST", true);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    goMainOnFailure();
                                }

                            }

                            @Override
                            public void onFailure(Call<HttpResponse<MobileVersion>> call, Throwable t) {
                                goMainOnFailure();
                            }

                        });

                    } else {
                        goMainOnFailure();
                    }
                }
            }
        }).start();

    }

    public void goMainOnFailure() {

        Intent intent = new Intent(IntroActivity.this, MainActivity.class);
        Bundle extra = new Bundle();
        extra.putString(VERSION_KEY, VERSION_NUMBER_NONE);
        intent.putExtras(extra);
        intent.putExtra("IS_FIRST", true);
        startActivity(intent);
        finish();

    }

}
