package kr.co.chongmoo.chongmooandroid.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.woojinsoft.chongmoo.android.R;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.ui.common.MoneyCommaEditTextWather;
import kr.co.chongmoo.chongmooandroid.util.UiUtils;

/**
 * 총무에 가계부 커스텀 다이얼로그
 * Created by lunawyrd on 2016. 4. 22..
 */
public class ChongmooDialog extends Dialog implements View.OnClickListener {

    public static final int MODE_NORMAL = 1;
    public static final int MODE_LIST = 2;
    public static final int MODE_CALRENDAR = 3;
    public static final int MODE_CALRENDAR_MONTH = 4;
    /** 로딩화면 다이얼로그 */ public static final int MODE_LOADING = 5;
    public static final int MODE_INCOME_WRITE = 6;
    public static final int MODE_SHARE = 7;

    public static final int WIDTH_X_LARGE = 300;
    public static final int WIDTH_LARGE = 280;
    public static final int WIDTH_MIDIUM = 240;
    public static final int WIDTH_SMALL = 230;

    private final View.OnClickListener mDefaultOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };

    private LinearLayout mLayoutTitle;
    private TextView mTextTitle;
    private Button mBtnClose;

    private TextView mTextMessage;
    private LinearLayout mLayoutList;
    private DatePicker mDatePicker;
    private LinearLayout mLinearLayoutLoading;
    private ImageView mImageViewLoading;
    private LinearLayout mLayoutIncomeWrite;
    private TextView mTextDate;
    private Button mBtnCalendar;
    private EditText mEditAmount;
    private LinearLayout mLayoutShare;
    private Button mBtnKakao;
    private Button mBtnSms;
    private Button mBtnBand;

    private TextView mTextNegative;
    private TextView mTextPositive;

    private int mMode = MODE_NORMAL;

    private List<String> mItemList;
    private DialogInterface.OnClickListener mOnClickListener;

    private int mIncomeSeq;
    private Date mDate;

    private String mShareMessage;

    public ChongmooDialog(Context context){
        this(context, MODE_NORMAL);
    }

    public ChongmooDialog(Context context, int mode) {
        this(context, mode, false);
    }

    public ChongmooDialog(Context context, int mode, boolean isLimit){
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_chongmoo);
        setCanceledOnTouchOutside(false);

        mLayoutTitle = (LinearLayout)findViewById(R.id.layout_title);
        mTextTitle = (TextView)findViewById(R.id.text_title);
        mBtnClose = (Button)findViewById(R.id.btn_close);
        mTextMessage = (TextView)findViewById(R.id.text_message);
        mLayoutList = (LinearLayout)findViewById(R.id.layout_list);
        mDatePicker = (DatePicker)findViewById(R.id.date_picker);
        mLinearLayoutLoading = (LinearLayout)findViewById(R.id.layer_loadng); // MODE_LOADING 일 경우에 활성화 되는 레이어
        mImageViewLoading = (ImageView)findViewById(R.id.img_loading); // MODE_LOADING 일 경우에 로딩 이미지
        mLayoutIncomeWrite = (LinearLayout)findViewById(R.id.layout_income_write);
        mTextDate = (TextView)findViewById(R.id.text_date);
        mBtnCalendar = (Button)findViewById(R.id.btn_calendar);
        mEditAmount = (EditText)findViewById(R.id.edit_amount);
        mLayoutShare = (LinearLayout)findViewById(R.id.layout_share);
        mBtnKakao = (Button)findViewById(R.id.btn_kakao);
        mBtnSms = (Button)findViewById(R.id.btn_sms);
        mBtnBand = (Button)findViewById(R.id.btn_band);
        mTextNegative = (TextView)findViewById(R.id.text_negative);
        mTextPositive = (TextView)findViewById(R.id.text_positive);

        mBtnClose.setOnClickListener(mDefaultOnClickListener);
        mTextDate.setOnClickListener(this);
        mBtnCalendar.setOnClickListener(this);
        mBtnKakao.setOnClickListener(this);
        mBtnSms.setOnClickListener(this);
        mBtnBand.setOnClickListener(this);

        mTextMessage.setVisibility(View.GONE);
        mLayoutList.setVisibility(View.GONE);
        mDatePicker.setVisibility(View.GONE);
        mLinearLayoutLoading.setVisibility(View.GONE);
        mLayoutIncomeWrite.setVisibility(View.GONE);
        mLayoutShare.setVisibility(View.GONE);

        mDatePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);

        dateTimePickerTextColor(mDatePicker, Color.parseColor("#FF1686DD"));

        if(isLimit)
            mDatePicker.setMaxDate(new Date().getTime());

        mEditAmount.addTextChangedListener(new MoneyCommaEditTextWather(mEditAmount));

        mMode = mode;
        mItemList = new ArrayList<>();

        LayoutParams params = getWindow().getAttributes();
        switch(mode){
            case MODE_CALRENDAR:
                params.width = UiUtils.getPixelFromDp(context, WIDTH_X_LARGE);
                break;
            case MODE_NORMAL:
            case MODE_LIST:
            case MODE_INCOME_WRITE:
                params.width = UiUtils.getPixelFromDp(context, WIDTH_LARGE);
                break;
            case MODE_CALRENDAR_MONTH:
                params.width = UiUtils.getPixelFromDp(context, WIDTH_MIDIUM);
            case MODE_SHARE:
                params.width = UiUtils.getPixelFromDp(context, WIDTH_SMALL);
                break;
            case MODE_LOADING:
                break;
        }
        params.height = LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes((LayoutParams) params);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        switch(mode){
            case MODE_NORMAL:
                mTextMessage.setVisibility(View.VISIBLE);
                break;
            case MODE_LIST:
                mLayoutList.setVisibility(View.VISIBLE);
                break;
            case MODE_CALRENDAR_MONTH:
                mDatePicker.findViewById(Resources.getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
            case MODE_CALRENDAR:
                mDatePicker.setVisibility(View.VISIBLE);
                break;
            case MODE_LOADING:
                getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                mLinearLayoutLoading.setVisibility(View.VISIBLE);
                ((AnimationDrawable) mImageViewLoading.getBackground()).start();
                break;
            case MODE_INCOME_WRITE:
                mLayoutIncomeWrite.setVisibility(View.VISIBLE);
                break;
            case MODE_SHARE:
                mLayoutShare.setVisibility(View.VISIBLE);
                break;
        }

        mDate = new Date();
        setTextDate(mTextDate, mDate, false);
    }

    @Override
    public void show() {
        if(mMode != MODE_LIST){
            super.show();
            return ;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.list_item_alert, mItemList){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                parent.setPadding(0,0,0,0);

                View view = convertView;
                if(view == null){
                    view = View.inflate(getContext(), R.layout.list_item_alert, null);
                }

                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setText(getItem(position));
                return view;
            }
        };

        AlertDialog dialog = new AlertDialog.Builder(getContext())
            .setAdapter(adapter, mOnClickListener)
            .create();

        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = UiUtils.getPixelFromDp(getContext(), 280);
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(params);

        dialog.show();
    }

    @Override
    public void setTitle(CharSequence title) {
        mLayoutTitle.setVisibility(View.VISIBLE);
        mTextTitle.setText(title);
    }

    public void setMessage(CharSequence message){
        mTextMessage.setText(message);
    }

    public void setList(String[] array, DialogInterface.OnClickListener onClickListener){
        ArrayList<String> list = new ArrayList<>();
        for(String item : array){
            list.add(item);
        }
        setList(list, onClickListener);
    }

    public void setList(List<String> list, DialogInterface.OnClickListener onClickListener){
        mItemList = list;
        mOnClickListener = onClickListener;
    }

    public void setDate(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        mDatePicker.updateDate(year, month, day);
    }

    public Date getDate(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, mDatePicker.getYear());
        cal.set(Calendar.MONTH, mDatePicker.getMonth());
        cal.set(Calendar.DAY_OF_MONTH, mDatePicker.getDayOfMonth());
        return new Date(cal.getTimeInMillis());
    }

    public int getIncomeSeq(){
        return mIncomeSeq;
    }

    public void setIncome(Income income){
        mIncomeSeq = income.getSeq();
        mDate = income.getDate();
        setTextDate(mTextDate, mDate, false);
        mEditAmount.setText(income.getAmount() + "");
        mLayoutTitle.setBackgroundColor(Color.parseColor("#FFEF2647"));
        mTextPositive.setBackgroundResource(R.drawable.selector_btn_bottom2);
    }

    public void setAmount(long amount){
        mEditAmount.setText(amount + "");
    }

    public Date getIncomeDate(){
        return mDate;
    }

    public int getAmount(){
        String amount = mEditAmount.getText().toString().replaceAll(",", "").trim();
        if(TextUtils.isEmpty(amount))
            return 0;
        return Integer.parseInt(amount);
    }

    public void setShareMessage(String shareMessage){
        mShareMessage = shareMessage;
    }

    public void setNegativeButton(CharSequence text, View.OnClickListener onClickListener){
        mTextNegative.setVisibility(View.VISIBLE);
        mTextNegative.setText(text);
         if(onClickListener == null){
            onClickListener = mDefaultOnClickListener;
        }
        mTextNegative.setOnClickListener(onClickListener);
    }

    public void setPositiveButton(CharSequence text, View.OnClickListener onClickListener){
        mTextPositive.setVisibility(View.VISIBLE);
        mTextPositive.setText(text);
        if(onClickListener == null){
            onClickListener = mDefaultOnClickListener;
        }
        mTextPositive.setOnClickListener(onClickListener);
    }

    private void dateTimePickerTextColor(ViewGroup picker, int color){
        for( int i = 0, j = picker.getChildCount() ; i < j ; i++ ){
            View t0 = picker.getChildAt(i);
            if(t0 instanceof NumberPicker)
                numberPickerTextColor((NumberPicker) t0, color);
            else if(t0 instanceof ViewGroup)
                dateTimePickerTextColor( (ViewGroup) t0, color);
        }
    }

    private void numberPickerTextColor( NumberPicker numberPicker, int color){
        for(int i = 0, j = numberPicker.getChildCount() ; i < j; i++){
            View t0 = numberPicker.getChildAt(i);
            if( t0 instanceof EditText){
                try{
                    Field t1 = numberPicker.getClass() .getDeclaredField( "mSelectorWheelPaint");
                    t1.setAccessible(true);
                    ((Paint)t1.get(numberPicker)) .setColor(color);
                    ((EditText)t0).setTextColor(color);
                    ((EditText)t0).setClickable(false);
                    ((EditText)t0).setFocusable(false);
                    ((EditText)t0).setFocusableInTouchMode(false);
                    numberPicker.invalidate();
                }
                catch(Exception e){

                }
            }
        }
    }

    private void setTextDate(TextView textView, Date date, boolean modeMonth){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        textView.setText(cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + (modeMonth ? "" : "-" + cal.get(Calendar.DAY_OF_MONTH)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_date:
            case R.id.btn_calendar:
                final ChongmooDialog dialog = new ChongmooDialog(getContext(), ChongmooDialog.MODE_CALRENDAR, false);
                dialog.setTitle(getContext().getString(R.string.dialog_calendar_text));
                dialog.setDate(mDate);
                dialog.setPositiveButton(getContext().getString(R.string.dialog_calendar_positive_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDate = dialog.getDate();
                        setTextDate(mTextDate, mDate, false);
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(getContext().getString(R.string.dialog_calendar_negative_text), null);
                dialog.show();
                break;
            case R.id.btn_kakao: {
                try{
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setPackage("com.kakao.talk");
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, mShareMessage);
                    getContext().startActivity(intent);
                    dismiss();
                }catch (ActivityNotFoundException e){
                    Toast.makeText(getContext(), R.string.toast_not_install_kakao, Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.btn_sms: {
                try{
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setType("vnd.android-dir/mms-sms");
                    intent.putExtra("sms_body", mShareMessage);
                    getContext().startActivity(intent);
                    dismiss();
                }catch (ActivityNotFoundException e){
                    Toast.makeText(getContext(), R.string.toast_not_install_sms, Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.btn_band: {
                try {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setPackage("com.nhn.android.band");
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, mShareMessage);
                    getContext().startActivity(intent);
                    dismiss();
                }catch (ActivityNotFoundException e){
                    Toast.makeText(getContext(), R.string.toast_not_install_band, Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }
}
