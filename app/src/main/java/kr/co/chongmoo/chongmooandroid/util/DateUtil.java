package kr.co.chongmoo.chongmooandroid.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 날짜 관련 유틸
 * Created by ??? on 2016-04-23.
 */
public class DateUtil {

    private final static String YYYY_MM_DD_FORMAT = "yyyy-MM-dd";
    private final static String YYYYMMDD_FORMAT = "yyyyMMdd";
    private final static String YYYYMMDDHHMMSS_FORMAT = "yyyyMMddHmmss";
    private final static String FILE_YYYYDATMMDATDD_HHDATMMFORMAT = "yyyy.MM.dd HH:mm";

    public static String getYyyymmddHHmmssFormat() {
        return getYyyymmddHHmmss(new Date());
    }

    public static String getFileYyyymmddHHmm(Date date) {

        if(date == null)
            return "";

        String dateResult = new SimpleDateFormat(FILE_YYYYDATMMDATDD_HHDATMMFORMAT).format(date);
        return dateResult;

    }

    public static String getYyyymmddHHmmss(Date date) {

        if(date == null)
            return "";

        String dateResult = new SimpleDateFormat(YYYYMMDDHHMMSS_FORMAT).format(date);
        return dateResult;

    }

    public static String getCurrentYyyymmddHHmmss() {
        return new SimpleDateFormat(YYYYMMDDHHMMSS_FORMAT).format(new Date());
    }

    public static String getYyyymmddFormat() {
        return getYyyymmddFormat(new Date());
    }

    public static String getYyyymmddFormat(Date date) {
        if(date == null)
            return "";

        String dateResult = new SimpleDateFormat(YYYYMMDD_FORMAT).format(date);
        return dateResult;
    }

    /**
     * date 객체를 yyyy-MM-dd 포맷의 문자열로 바꾼다.
     * @param   date
     * @return  String
     */
    public static String getDateToYearMonthDayString(Date date) {

        String dateResult = new SimpleDateFormat(YYYY_MM_DD_FORMAT).format(date);
        return dateResult;

    }

    /**
     * yyyy-MM-dd 포맷의 문자열을 date 객체로 바꾼다.
     * @param   input
     * @return  Date
     */
    public static Date getYearMonthDayStringToDate(String input) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YYYY_MM_DD_FORMAT);
        Date convertedCurrentDate = null;
        try {
            convertedCurrentDate = simpleDateFormat.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            if(convertedCurrentDate == null) {
                convertedCurrentDate = new Date();
            }
        }
        return convertedCurrentDate;

    }

    public static String getDateToYearMonthString(Date date) {
        String dateResult = new SimpleDateFormat("yyyyMM").format(date);
        return dateResult;
    }

    public static String getDateToYearMonthDaySlashString(Date date) {
        String dateResult = new SimpleDateFormat("yy/MM/dd").format(date);
        return dateResult;
    }

    public static String getDateToYearMonthSlashString(Date date) {
        String dateResult = new SimpleDateFormat("yy/MM").format(date);
        return dateResult;
    }

    public static String getDateToYearMonthDayDotString(Date date) {
        String dateResult = new SimpleDateFormat("yyyy.MM.dd").format(date);
        return dateResult;
    }

    public static String getDateToYearMonthDotString(Date date) {
        String dateResult = new SimpleDateFormat("yyyy.MM").format(date);
        return dateResult;
    }

    public static String getDateToMonthDayString(Date date) {
        String dateResult = new SimpleDateFormat("MM.dd").format(date);
        return dateResult;
    }

    public static String getDateToYearMonthDayWeekString(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        return String.format("%d.%02d.%02d (%s)", cal.get(Calendar.YEAR), (cal.get(Calendar.MONTH) + 1), cal.get(Calendar.DAY_OF_MONTH), DateUtil.getDayOfWeek(cal));
    }

    public static int getYear(Date date){
        if(date == null)
            return -1;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        return cal.get(Calendar.YEAR);
    }

    public static int getMonth(Date date){
        if(date == null)
            return -1;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        return cal.get(Calendar.MONTH);
    }

    public static int getDay(Date date){
        if(date == null)
            return -1;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static int getDayOfMonth(Date date){
        if(date == null)
            return -1;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static String getDayOfWeek(Calendar cal){
        String dayOfWeek = "";
        switch (cal.get(Calendar.DAY_OF_WEEK)){
            case 1:
                dayOfWeek = "일";
                break;
            case 2:
                dayOfWeek = "월";
                break;
            case 3:
                dayOfWeek = "화";
                break;
            case 4:
                dayOfWeek = "수";
                break;
            case 5:
                dayOfWeek = "목";
                break;
            case 6:
                dayOfWeek = "금";
                break;
            case 7:
                dayOfWeek = "토";
                break;
        }
        return dayOfWeek;
    }
}
