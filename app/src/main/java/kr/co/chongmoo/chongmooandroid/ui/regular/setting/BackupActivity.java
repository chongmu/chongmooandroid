package kr.co.chongmoo.chongmooandroid.ui.regular.setting;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import io.realm.Realm;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.woojinsoft.chongmoo.android.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import kr.co.chongmoo.chongmooandroid.model.ClosingAccount;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.util.StringUtil;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;

/**
 *
 * 백업 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class BackupActivity extends BaseSubActivity implements View.OnClickListener{

    final static public String MEETING_SEQ = "MEETING_SEQ";
    final static public String MEETING_NAME = "MEETING_NAME";
    final static private String PLAIN_TEXT = "plain/text";
    final static private String SDCARD_RELATIVE_PATH = "/chongmoo/backup";

    private Realm mRealm;
    private Realm mBackupRealm;

    private int mSeq;
    private String mName;

    private RegularMeeting mRegularMeeting;
    private RealmResults<Income> mIncomes;
    private RealmResults<Member> mMembers;
    private RealmResults<ClosingAccount> mClosingAccounts;

    private ChongmooDialog mChongmooDialog;

    private TextView mTextViewBackupGuide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backup);
        setTitle(getString(R.string.title_activity_backup));

        mChongmooDialog  = new ChongmooDialog(BackupActivity.this, ChongmooDialog.MODE_LOADING);

        mSeq = getIntent().getIntExtra(MEETING_SEQ, 0);
        mName = getIntent().getStringExtra(MEETING_NAME);
        boolean isSubscription = getIntent().getBooleanExtra("IS_SUBSCRIPTION", false);


        TextView textBackUp = (TextView) findViewById(R.id.text_backup);
        TextView textSdCardBackUp = (TextView) findViewById(R.id.text_sdcard_backup);
        TextView textRecoveryTitle = (TextView) findViewById(R.id.text_recovery_title);
        View viewRecoveryLine1 = findViewById(R.id.view_recovery_line_2);
        TextView textRecovery = (TextView) findViewById(R.id.text_recovery);
        View viewRecoveryLine2 = findViewById(R.id.view_recovery_line_2);
        mTextViewBackupGuide = (TextView) findViewById(R.id.txt_backup_guide);
        StringUtil.setTextViewColorPartial(mTextViewBackupGuide, 0XFFB0071F, getString(R.string.activity_backup_txt_guide1), getString(R.string.activity_backup_txt_guide3));
        textBackUp.setOnClickListener(this);
        textSdCardBackUp.setOnClickListener(this);
        textRecovery.setOnClickListener(this);

        if(!isSubscription){
            textRecoveryTitle.setVisibility(View.GONE);
            viewRecoveryLine1.setVisibility(View.GONE);
            textRecovery.setVisibility(View.GONE);
            viewRecoveryLine2.setVisibility(View.GONE);
        }

        mRealm = Realm.getDefaultInstance();

    }

    @Override
    public void onClick(View v) {

        int permissionCheck = -1;

        switch (v.getId()){
            case R.id.text_backup:

                permissionCheck = ContextCompat.checkSelfPermission(BackupActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if(permissionCheck == PackageManager.PERMISSION_DENIED) { // 마시멜로우 버젼 관련 체크
                    new TedPermission(BackupActivity.this)
                            .setPermissionListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted() {
                                    Toast.makeText(BackupActivity.this, getString(R.string.toast_authority_permit_txt), Toast.LENGTH_SHORT).show();

                                    final Handler handler = new Handler();

                                    mChongmooDialog.show();
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Thread.sleep(1000);
                                            } catch (InterruptedException e) {
                                                e.printStackTrace();
                                            }

                                            Realm realm = null;
                                            try{
                                                realm = Realm.getDefaultInstance();
                                                String fileName = StringUtil.getBackupFileName(mName);
                                                backup(realm, fileName);
                                                showMailIntent(fileName);
                                                handler.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        mChongmooDialog.dismiss();
                                                    }
                                                });
                                            }finally {
                                                if(realm != null)
                                                    realm.close();
                                            }
                                        }
                                    }).start();
                                }

                                @Override
                                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                    Toast.makeText(BackupActivity.this, getString(R.string.toast_authority_deny_txt) + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setDeniedMessage(getString(R.string.toast_casual_meeting_file_write_role_txt))
                            .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .check();
                } else {
                    final Handler handler = new Handler();

                    mChongmooDialog.show();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            Realm realm = null;
                            try{
                                realm = Realm.getDefaultInstance();
                                String fileName = StringUtil.getBackupFileName(mName);
                                backup(realm, fileName);
                                showMailIntent(fileName);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        mChongmooDialog.dismiss();
                                    }
                                });
                            }finally {
                                if(realm != null)
                                    realm.close();
                            }
                        }
                    }).start();
                }

                break;
            case R.id.text_sdcard_backup:

                permissionCheck = ContextCompat.checkSelfPermission(BackupActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if(permissionCheck == PackageManager.PERMISSION_DENIED) { // 마시멜로우 버젼 관련 체크
                    new TedPermission(BackupActivity.this)
                            .setPermissionListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted() {

                                    Toast.makeText(BackupActivity.this, getString(R.string.toast_authority_permit_txt), Toast.LENGTH_SHORT).show();
                                    final Handler handler = new Handler();

                                    mChongmooDialog.show();
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Thread.sleep(1000);
                                            } catch (InterruptedException e) {
                                                e.printStackTrace();
                                            }

                                            Realm realm = null;
                                            try{
                                                realm = Realm.getDefaultInstance();
                                                String fileName = StringUtil.getBackupFileName(mName);
                                                backup(realm, fileName);
                                                copyToSdCard(fileName);
                                                handler.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        mChongmooDialog.dismiss();
                                                    }
                                                });
                                            }finally {
                                                if(realm != null)
                                                    realm.close();
                                            }
                                        }
                                    }).start();

                                }
                                @Override
                                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                    Toast.makeText(BackupActivity.this, getString(R.string.toast_authority_deny_txt) + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setDeniedMessage(getString(R.string.toast_casual_meeting_file_write_role_txt))
                            .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .check();
                } else {
                    final Handler handler = new Handler();

                    mChongmooDialog.show();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            Realm realm = null;
                            try{
                                realm = Realm.getDefaultInstance();
                                String fileName = StringUtil.getBackupFileName(mName);
                                backup(realm, fileName);
                                copyToSdCard(fileName);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        mChongmooDialog.dismiss();
                                    }
                                });
                            }finally {
                                if(realm != null)
                                    realm.close();
                            }
                        }
                    }).start();
                }

                break;
            case R.id.text_recovery:
                Intent intent = new Intent(BackupActivity.this, BackupFileListActivity.class);
                intent.putExtra(MEETING_SEQ, mSeq);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    private void backup(Realm realm, String backupName) {

        RealmQuery<RegularMeeting> queryRegularMeeting = realm.where(RegularMeeting.class);
        RealmQuery<Income> queryIncome = realm.where(Income.class);
        RealmQuery<Member> queryMember = realm.where(Member.class);
        RealmQuery<ClosingAccount> queryClosingAccount = realm.where(ClosingAccount.class);

        mRegularMeeting = queryRegularMeeting.equalTo("seq", mSeq).findFirst();
        mIncomes = queryIncome.equalTo("meetingSeq", mSeq).findAll();
        mMembers = queryMember.equalTo("meetingSeq", mSeq).findAll();
        mClosingAccounts = queryClosingAccount.equalTo("meetingSeq", mSeq).findAll();

        mBackupRealm = Realm.getInstance(new RealmConfiguration.Builder(this).name(backupName + ".realm").build());
        mBackupRealm.beginTransaction();
        RegularMeeting realmRegularMeeting = mBackupRealm.copyToRealm(mRegularMeeting);
        for(Income income : mIncomes) {
            Income realmIncome = mBackupRealm.copyToRealm(income);
        }
        for(Member member : mMembers) {
            Member realmMember = mBackupRealm.copyToRealm(member);
        }
        for(ClosingAccount closingAccount : mClosingAccounts) {
            ClosingAccount realmClosingAccount = mBackupRealm.copyToRealm(closingAccount);
        }
        mBackupRealm.commitTransaction();

    }

    private void copyToSdCard(String fileName) {

        File exportRealmPATH = getExternalFilesDir(null);
        File exportRealmFile = new File(exportRealmPATH, fileName);
        exportRealmFile.delete();

        try {
            mBackupRealm.writeCopyTo(exportRealmFile);
            mBackupRealm.close();
            Realm.deleteRealm(mBackupRealm.getConfiguration());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String sdCardRoot = Environment.getExternalStorageDirectory().getAbsolutePath() + SDCARD_RELATIVE_PATH;
        File sdcardRelativePathDirectory = new File(sdCardRoot);

        if(sdcardRelativePathDirectory.exists() == false) {
            sdcardRelativePathDirectory.mkdirs();
        }

        final File sdCardFile = new File(sdCardRoot, fileName);
        final boolean isMoved = exportRealmFile.renameTo(sdCardFile);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isMoved == true) {
                    Toast.makeText(BackupActivity.this, String.format(getString(R.string.toast_sdcard_backup_success_message_txt), sdCardFile.getAbsolutePath()), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(BackupActivity.this, String.format(getString(R.string.toast_sdcard_backup_fail_message_txt), sdCardFile.getAbsolutePath()), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void showMailIntent(String fileName) {

        File exportRealmPATH = getExternalFilesDir(null);
        //String exportRealmFileName = UPLOAD_BACKUP_FILE_NAME;
        File exportRealmFile = new File(exportRealmPATH, fileName);
        exportRealmFile.delete();

        try {
            mBackupRealm.writeCopyTo(exportRealmFile);
            mBackupRealm.close();
            Realm.deleteRealm(mBackupRealm.getConfiguration());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Uri fileUri = Uri.fromFile(exportRealmFile);
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, mRegularMeeting.getName() + getString(R.string.backup_mail_title_txt));
        intent.putExtra(Intent.EXTRA_STREAM, fileUri);
        startActivity(Intent.createChooser(intent, getString(R.string.backup_mail_content_txt)));
    }

}