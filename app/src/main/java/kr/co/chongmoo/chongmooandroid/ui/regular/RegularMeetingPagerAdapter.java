package kr.co.chongmoo.chongmooandroid.ui.regular;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 *
 * 정기모임 Adapter
 *
 * @author  Hoon Jang
 * @since   2016.04.06
 */
public class RegularMeetingPagerAdapter extends FragmentPagerAdapter {
    private static final String TAB_1 = "수입/지출";
    private static final String TAB_2 = "회비현황";
    private static final String TAB_3 = "결산/공유";
    private String[] title = {TAB_1, TAB_2, TAB_3};

    public RegularMeetingPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new Fragment();
        switch (title[position]){
            case TAB_1:
                fragment = new IncomeFragment();
                break;
            case TAB_2:
                fragment = new PaymentStatusFragment();
                break;
            case TAB_3:
                fragment = new ClosingAccountFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
