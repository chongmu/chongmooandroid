package kr.co.chongmoo.chongmooandroid.model;

import java.util.Date;

import io.realm.annotations.PrimaryKey;

/**
 *
 * 회비상태 저장을 위한 모델
 *
 * @author  Hoon Jang
 * @since   2016.04.07
 */
public class PaymentStatus {
    public static final int STATUS_NOTHING = 0;
    public static final int STATUS_NOT_PAID = 1;
    public static final int STATUS_PAID = 2;
    public static final int STATUS_PAID_IMAGE = 3;
    public static final int STATUS_EXEMPTION = 4;

    private int status;
    private Member member;
    private Income income;
    private long amount;
    private int year;
    private int month;

    private boolean isNew;
    private boolean isOut;
    private boolean isModify;

    public PaymentStatus() {}

    public PaymentStatus(int status,Member member, Income income, long amount, int year, int month) {
        this.status = status;
        this.member = member;
        this.income = income;
        this.amount = amount;
        this.year = year;
        this.month = month;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Income getIncome() {
        return income;
    }

    public void setIncome(Income income) {
        this.income = income;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    public boolean isOut() {
        return isOut;
    }

    public void setIsOut(boolean isOut) {
        this.isOut = isOut;
    }

    public boolean isModify() {
        return isModify;
    }

    public void setIsModify(boolean isModify) {
        this.isModify = isModify;
    }
}
