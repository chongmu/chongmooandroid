package kr.co.chongmoo.chongmooandroid.model;

/**
 * 공지사항 관련 VO
 * Created by lunawyrd on 2016. 4. 1..
 */
public class Notice {

    private int seq;
    private String title;
    private String content;
    private String registDate;
    private String modifyDate;
    private String exposedYn;
    private String deletedYn;

    public Notice() {}

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRegistDate() {
        return registDate;
    }

    public void setRegistDate(String registDate) {
        this.registDate = registDate;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getExposedYn() {
        return exposedYn;
    }

    public void setExposedYn(String exposedYn) {
        this.exposedYn = exposedYn;
    }

    public String getDeletedYn() {
        return deletedYn;
    }

    public void setDeletedYn(String deletedYn) {
        this.deletedYn = deletedYn;
    }
}
