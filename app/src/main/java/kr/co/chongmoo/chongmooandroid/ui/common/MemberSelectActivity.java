package kr.co.chongmoo.chongmooandroid.ui.common;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.Sort;
import com.woojinsoft.chongmoo.android.R;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.ui.holder.AddressBookHolder;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 회원 선택 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class MemberSelectActivity extends BaseSubActivity {

    private Realm mRealm;

    private int mMeetingSeq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_select);
        setTitle(R.string.activity_member_select_title);

        LinearLayout layoutEmpty = (LinearLayout) findViewById(R.id.layout_empty);
        RecyclerView recyclerMember = (RecyclerView) findViewById(R.id.recycler_member);
        recyclerMember.setLayoutManager(new LinearLayoutManager(this));

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            mMeetingSeq = bundle.getInt("MEETING_SEQ", 0);
        }

        mRealm = Realm.getDefaultInstance();
        RealmQuery<Member> query = mRealm.where(Member.class);
        ArrayList<Member> memberList = new ArrayList<>(query.equalTo("meetingSeq", mMeetingSeq).isNull("outDate").findAllSorted("seq", Sort.DESCENDING));
        recyclerMember.setAdapter(new BeautifulRecyclerAdapter<>(this, AddressBookHolder.RESOURCE_ID, memberList, new AddressBookHolder.MemberCoordinator(this)));

        layoutEmpty.setVisibility(memberList.size() == 0 ? View.VISIBLE : View.GONE);
        recyclerMember.setVisibility(memberList.size() != 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }
}
