package kr.co.chongmoo.chongmooandroid.ui.regular.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.parceler.Parcels;

import java.util.ArrayList;

import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.model.RegularMeetingSetting;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.ui.holder.RegularMeetingSettingHolder;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 정규모임 설정 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class RegularMeetingSettingActivity extends BaseSubActivity {

    private int mMeetingSeq = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regular_meeting_setting);
        setTitle(R.string.activity_regular_meeting_setting_title);

        boolean isSubscription = false;
        Bundle b = getIntent().getExtras();
        if(b != null) {
            RegularMeeting meeting = Parcels.unwrap(b.getParcelable("item"));
            setTitle(b.getString("NAME"));
            mMeetingSeq = b.getInt("SEQ", 0);
            isSubscription = b.getBoolean("IS_SUBSCRIPTION");
        }

        RecyclerView recyclerRegularMeetingSetting = (RecyclerView) findViewById(R.id.recycler_regular_meeting_setting);
        recyclerRegularMeetingSetting.setLayoutManager(new LinearLayoutManager(this));

        ArrayList<RegularMeetingSetting> itemList = new ArrayList<>();

        Intent intent;
        if(isSubscription) {
            intent = new Intent(this, RegularMeetingManagementActivity.class);
            intent.putExtra("MEETING_SEQ", mMeetingSeq);
            intent.putExtra(RegularMeetingManagementActivity.EXTRA_MODE, RegularMeetingManagementActivity.MODE_READ);
            itemList.add(new RegularMeetingSetting(getString(R.string.activity_regular_meeting_setting_menu_1), R.drawable.ico_con_groupset1, intent));

            intent = new Intent(this, MemberManagementActivity.class);
            intent.putExtra("MEETING_SEQ", mMeetingSeq);
            itemList.add(new RegularMeetingSetting(getString(R.string.activity_regular_meeting_setting_menu_2), R.drawable.ico_con_groupset2, intent));
        }

        intent = new Intent(this, BackupActivity.class);
        intent.putExtra("MEETING_SEQ", mMeetingSeq);
        if(b != null) {
            intent.putExtra("MEETING_NAME", b.getString("NAME"));
            intent.putExtra("IS_SUBSCRIPTION", isSubscription);
        }
        itemList.add(new RegularMeetingSetting(getString(R.string.activity_regular_meeting_setting_menu_3), R.drawable.ico_con_groupset3, intent));


        if(isSubscription) {
            intent = new Intent(this, SubscriptionInfoActivity.class);
            intent.putExtra("MEETING_SEQ", mMeetingSeq);
            itemList.add(new RegularMeetingSetting(getString(R.string.activity_regular_meeting_setting_menu_4), R.drawable.ico_btm_write, intent));
        }

        recyclerRegularMeetingSetting.setAdapter(new BeautifulRecyclerAdapter(this, RegularMeetingSettingHolder.RESOURCE_ID, itemList, new RegularMeetingSettingHolder.CommonCoordinator(this)));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if(data != null){
                if(data.hasExtra("name")){
                    setTitle(data.getStringExtra("name"));
                }
            }
        }
    }
}
