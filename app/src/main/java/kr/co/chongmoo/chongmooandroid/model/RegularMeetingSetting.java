package kr.co.chongmoo.chongmooandroid.model;

import android.content.Intent;

/**
 *
 * 정기모임 설정을 위한 모델
 *
 * @author  Hoon Jang
 * @since   2016.04.13
 */
public class RegularMeetingSetting {

    private String title;
    private int imageResource;
    private Intent intent;

    public RegularMeetingSetting(String title, int imageResource, Intent intent) {
        this.title = title;
        this.imageResource = imageResource;
        this.intent = intent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }
}
