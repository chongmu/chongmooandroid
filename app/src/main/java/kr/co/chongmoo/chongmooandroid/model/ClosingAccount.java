package kr.co.chongmoo.chongmooandroid.model;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

/**
 *
 * 결산 정보를 담기 위한 모델
 *
 * @author  Hoon Jang
 * @since   2016.04.30
 */
@RealmClass
public class ClosingAccount extends RealmObject{
    public static final int TYPE_INCOME = 1;
    public static final int TYPE_STATUS = 2;

    @PrimaryKey private int seq;

    private int type;
    private String code;
    private int meetingSeq;
    private Date startDate;
    private Date endDate;
    private String dataJson;
    private Date registDate;

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getMeetingSeq() {
        return meetingSeq;
    }

    public void setMeetingSeq(int meetingSeq) {
        this.meetingSeq = meetingSeq;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDataJson() {
        return dataJson;
    }

    public void setDataJson(String dataJson) {
        this.dataJson = dataJson;
    }

    public Date getRegistDate() {
        return registDate;
    }

    public void setRegistDate(Date registDate) {
        this.registDate = registDate;
    }

    public static int generateSequence(Realm realm){
        RealmQuery<ClosingAccount> query = realm.where(ClosingAccount.class);
        Number number = query.max("seq");
        int seq = (number == null) ? 0 : number.intValue();
        return seq + 1;
    }
}
