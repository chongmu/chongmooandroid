package kr.co.chongmoo.chongmooandroid.model;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 *
 * 구독정보를 위한 모델
 *
 * @author  Hoon Jang
 * @since   2016.05.08
 */
@RealmClass
public class PurchaseHistory extends RealmObject{
    @PrimaryKey
    private String token;

    private String itemType;  // ITEM_TYPE_INAPP or ITEM_TYPE_SUBS
    private String orderId;
    private String packageName;
    private String sku;
    private long purchaseTime;
    private int purchaseState;
    private String developerPayload;
    private String originalJson;
    private String signature;

    public PurchaseHistory() {
    }

    public PurchaseHistory(String itemType, String jsonPurchaseInfo, String signature) throws JSONException {
        this.itemType = itemType;
        this.originalJson = jsonPurchaseInfo;

        JSONObject o = new JSONObject(originalJson);
        this.orderId = o.optString("orderId");
        this.packageName = o.optString("packageName");
        this.sku = o.optString("productId");
        this.purchaseTime = o.optLong("purchaseTime");
        this.purchaseState = o.optInt("purchaseState");
        this.developerPayload = o.optString("developerPayload");
        this.token = o.optString("token", o.optString("purchaseToken"));
        this.signature = signature;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public long getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(long purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public int getPurchaseState() {
        return purchaseState;
    }

    public void setPurchaseState(int purchaseState) {
        this.purchaseState = purchaseState;
    }

    public String getDeveloperPayload() {
        return developerPayload;
    }

    public void setDeveloperPayload(String developerPayload) {
        this.developerPayload = developerPayload;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getOriginalJson() {
        return originalJson;
    }

    public void setOriginalJson(String originalJson) {
        this.originalJson = originalJson;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
