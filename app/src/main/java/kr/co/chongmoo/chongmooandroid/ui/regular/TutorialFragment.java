package kr.co.chongmoo.chongmooandroid.ui.regular;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 *
 * Tutorial Fragment
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
@SuppressLint("ValidFragment")
public class TutorialFragment extends Fragment {

    private int mResourceId;

    public TutorialFragment(int resourceId) {
        mResourceId = resourceId;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(mResourceId, container, false);
        return rootView;
    }

}