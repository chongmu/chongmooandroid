package kr.co.chongmoo.chongmooandroid.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import io.realm.RealmList;
import com.woojinsoft.chongmoo.android.R;
import kr.co.chongmoo.chongmooandroid.model.AddressBook;

/**
 * 번개 모임 차수 관련 참석자 관련 선택 커스텀 뷰
 * Created by ??? on 2016-04-22.
 */
public class CasualMeetingMemberSubView extends LinearLayout implements View.OnClickListener {

    CasualMeetingStepView mCasualMeetingStepView;

    ToggleButton mToggleButtonCslMetStpSubFst;
    ToggleButton mToggleButtonCslMetStpSubScd;
    ToggleButton mToggleButtonCslMetStpSubThd;
    ToggleButton mToggleButtonCslMetStpSubFth;

    public CasualMeetingMemberSubView(Context context) {
        super(context);
        init();
    }

    public CasualMeetingMemberSubView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CasualMeetingMemberSubView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CasualMeetingMemberSubView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {

        inflate(getContext(), R.layout.view_casual_meeting_step_sub, this);

        mToggleButtonCslMetStpSubFst = (ToggleButton) findViewById(R.id.csl_met_stp_sub_fst);
        mToggleButtonCslMetStpSubFst.setOnClickListener(this);

        mToggleButtonCslMetStpSubScd = (ToggleButton) findViewById(R.id.csl_met_stp_sub_scd);
        mToggleButtonCslMetStpSubScd.setOnClickListener(this);

        mToggleButtonCslMetStpSubThd = (ToggleButton) findViewById(R.id.csl_met_stp_sub_thd);
        mToggleButtonCslMetStpSubThd.setOnClickListener(this);

        mToggleButtonCslMetStpSubFth = (ToggleButton) findViewById(R.id.csl_met_stp_sub_fhd);
        mToggleButtonCslMetStpSubFth.setOnClickListener(this);

    }

    public void setFirstStatus(String text, int visibility) {
        mToggleButtonCslMetStpSubFst.setText(text);
        mToggleButtonCslMetStpSubFst.setTextOff(text);
        mToggleButtonCslMetStpSubFst.setTextOn(text);
        mToggleButtonCslMetStpSubFst.setVisibility(visibility);
        boolean isActive = isActiveUser(text);
        if(isActive == true) {
            mToggleButtonCslMetStpSubFst.setBackgroundResource(R.drawable.selector_btn_csl_step_atv_on);
            mToggleButtonCslMetStpSubFst.setTextColor(Color.parseColor("#FFFFFFFF"));
            mToggleButtonCslMetStpSubFst.setChecked(isActive);
        }
    }

    public void setSecondStatus(String text, int visibility) {
        mToggleButtonCslMetStpSubScd.setText(text);
        mToggleButtonCslMetStpSubScd.setTextOff(text);
        mToggleButtonCslMetStpSubScd.setTextOn(text);
        mToggleButtonCslMetStpSubScd.setVisibility(visibility);
        boolean isActive = isActiveUser(text);
        if(isActive == true) {
            mToggleButtonCslMetStpSubScd.setBackgroundResource(R.drawable.selector_btn_csl_step_atv_on);
            mToggleButtonCslMetStpSubScd.setTextColor(Color.parseColor("#FFFFFFFF"));
            mToggleButtonCslMetStpSubScd.setChecked(isActive);
        }
    }

    public void setThirdStatus(String text, int visibility) {
        mToggleButtonCslMetStpSubThd.setText(text);
        mToggleButtonCslMetStpSubThd.setTextOff(text);
        mToggleButtonCslMetStpSubThd.setTextOn(text);
        mToggleButtonCslMetStpSubThd.setVisibility(visibility);
        boolean isActive = isActiveUser(text);
        if(isActive == true) {
            mToggleButtonCslMetStpSubThd.setBackgroundResource(R.drawable.selector_btn_csl_step_atv_on);
            mToggleButtonCslMetStpSubThd.setTextColor(Color.parseColor("#FFFFFFFF"));
            mToggleButtonCslMetStpSubThd.setChecked(isActive);
        }
    }

    public void setFourthlyStatus(String text, int visibility) {
        mToggleButtonCslMetStpSubFth.setText(text);
        mToggleButtonCslMetStpSubFth.setTextOff(text);
        mToggleButtonCslMetStpSubFth.setTextOn(text);
        mToggleButtonCslMetStpSubFth.setVisibility(visibility);
        boolean isActive = isActiveUser(text);
        if(isActive == true) {
            mToggleButtonCslMetStpSubFth.setBackgroundResource(R.drawable.selector_btn_csl_step_atv_on);
            mToggleButtonCslMetStpSubFth.setTextColor(Color.parseColor("#FFFFFFFF"));
            mToggleButtonCslMetStpSubFth.setChecked(isActive);
        }
    }

    public void setCasualMeetingStepView(CasualMeetingStepView casualMeetingStepView) {
        this.mCasualMeetingStepView = casualMeetingStepView;
    }

    public boolean isActiveUser(String name) {
        boolean returnStatus = false;
        RealmList<AddressBook> addressBookList = mCasualMeetingStepView.getCasualMeetingStep().getAddressBookList();
        for(AddressBook addressBook : addressBookList) { // Log.d("IS_ACTIVE_USER", String.format("%s vs %s", addressBook.getName(), name));
            if(addressBook.getName().equals(name) == true) {
                returnStatus = true;
                break;
            }
        }
        return returnStatus;
    }

    @Override
    public void onClick(View v) {

        ToggleButton toggleButton = (ToggleButton) v;
        boolean status = toggleButton.isChecked();
        RealmList<AddressBook> addressBookList = mCasualMeetingStepView.getCasualMeetingStep().getAddressBookList();

        if(status == true) {

            toggleButton.setBackgroundResource(R.drawable.selector_btn_csl_step_atv_on);
            toggleButton.setTextColor(Color.parseColor("#FFFFFFFF"));
            String name = toggleButton.getTextOn().toString(); //Log.d("IS_ACTIVE_USER_INSERTED", String.format("%s, %s, %s", name, toggleButton.getTextOff().toString(), toggleButton.getTextOn().toString()));
            AddressBook addressBook = new AddressBook();
            addressBook.setName(name);
            addressBook.setIsChecked(true);

            addressBookList.add(addressBook);

        } else {

            toggleButton.setBackgroundResource(R.drawable.selector_btn_csl_step_atv_off);
            toggleButton.setTextColor(Color.parseColor("#FFBABABA"));
            String name = toggleButton.getTextOff().toString();
            AddressBook addressBook = new AddressBook();
            addressBook.setName(name);
            addressBook.setIsChecked(false);

            for( int i = 0 ; i < addressBookList.size() ; i++ ) {
                AddressBook addressBookSub = addressBookList.get(i);
                if(addressBookSub.getName().equals(addressBook.getName()) == true) {
                    addressBookList.remove(i);
                }
            }

        }

        updateCasualStepMemberCount();

    }

    public void updateCasualStepMemberCount() {

        RealmList<AddressBook> addressBookList = mCasualMeetingStepView.getCasualMeetingStep().getAddressBookList();
        int checkedCount = addressBookList.size();
        /*int checkedCount = 0;
        for(AddressBook addressBook: addressBookList) {
            if(addressBook.getIsChecked() == true) {
                checkedCount++;
            }
        }*/
        mCasualMeetingStepView.setCasualStepMemberCount(checkedCount);

    }


}
