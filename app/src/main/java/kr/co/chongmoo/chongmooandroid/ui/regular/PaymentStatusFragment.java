package kr.co.chongmoo.chongmooandroid.ui.regular;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.model.Exemption;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.MonthlyFee;
import kr.co.chongmoo.chongmooandroid.model.PaymentStatus;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.ui.casual.AddressBookActivity;
import kr.co.chongmoo.chongmooandroid.ui.holder.PaymentStatusHolder;
import kr.co.chongmoo.chongmooandroid.ui.regular.setting.MemberWriteActivity;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 회비현황 Fragment
 *
 * @author  Hoon Jang
 * @since   2016.04.06
 */
public class PaymentStatusFragment extends Fragment implements View.OnClickListener{

    public static final int REQUEST_ADDRESS = 1;
    public static final int REQUEST_MEMBER_WRITE = 2;

    private LinearLayout mLayoutEmpty;
    private LinearLayout mLayoutStatus;

    private TextView mTextMonth;
    private TextView mTextTotal;
    private LinearLayout mLayoutTitle;
    private RecyclerView mRecyclerPaymentStatus;

    private Realm mRealm;

    private int mYear;
    private int mMonth;

    private int mMode = PaymentStatus.STATUS_PAID_IMAGE;

    private int mScrollY = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_payment_status, container, false);

        mLayoutEmpty = (LinearLayout) rootView.findViewById(R.id.layout_empty);
        LinearLayout mLayoutAddMember = (LinearLayout) rootView.findViewById(R.id.layout_add_member);
        mLayoutStatus = (LinearLayout) rootView.findViewById(R.id.layout_status);
        Button btnMonthLeft = (Button) rootView.findViewById(R.id.btn_month_left);
        mTextMonth = (TextView) rootView.findViewById(R.id.text_month);
        Button btnMonthRight = (Button) rootView.findViewById(R.id.btn_month_right);

        mLayoutAddMember.setOnClickListener(this);
        btnMonthLeft.setOnClickListener(this);
        mTextMonth.setOnClickListener(this);
        btnMonthRight.setOnClickListener(this);

        mTextTotal = (TextView) rootView.findViewById(R.id.text_total);
        ToggleButton toggle = (ToggleButton) rootView.findViewById(R.id.toggle);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mMode = isChecked ? PaymentStatus.STATUS_PAID_IMAGE : PaymentStatus.STATUS_PAID;
                refreshList();
            }
        });

        mLayoutTitle = (LinearLayout) rootView.findViewById(R.id.layout_title);

        mRecyclerPaymentStatus = (RecyclerView)rootView.findViewById(R.id.recycler_payment_status);
        mRecyclerPaymentStatus.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerPaymentStatus.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mScrollY += dy;
            }
        });

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(new Date().getTime());

        mYear = cal.get(Calendar.YEAR);
        mMonth = cal.get(Calendar.MONTH);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mRealm = Realm.getDefaultInstance();

        refreshList();
    }

    @Override
    public void onStop() {
        super.onStop();
        mRealm.close();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case REQUEST_ADDRESS:
                if(resultCode == Activity.RESULT_OK){
                    final ArrayList<String> addressNameList = data.getExtras().getStringArrayList("ADDRESS_LIST");
                    if(addressNameList.size() < 1)
                        return ;

                    mRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmQuery<RegularMeeting> query = realm.where(RegularMeeting.class);
                            RegularMeeting regularMeeting = query.equalTo("seq", ((RegularMeetingActivity)getActivity()).getSeq()).findFirst();

                            for(String name : addressNameList) {
                                Member member = realm.createObject(Member.class);
                                member.setSeq(Member.generateSequence(realm));
                                member.setMeetingSeq(((RegularMeetingActivity)getActivity()).getSeq());
                                member.setName(name);
                                member.setRegistDate(new Date());
                                member.setStartDate(regularMeeting.getStartdate());
                            }
                        }
                    }, new Realm.Transaction.Callback() {
                        @Override
                        public void onSuccess() {
                            refreshList();
                            Toast.makeText(getContext(), R.string.toast_regist_success, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(Exception e) {
                            Toast.makeText(getContext(), String.format(getString(R.string.toast_regist_fail), e.toString()), Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
                }
            case REQUEST_MEMBER_WRITE:
                if(resultCode == Activity.RESULT_OK)
                    refreshList();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_add_member:{
                int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS);
                if(permissionCheck == PackageManager.PERMISSION_DENIED) { // 마시멜로우 버젼 관련 체크
                    new TedPermission(getActivity())
                            .setPermissionListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted() {
                                    ChongmooDialog dialog = new ChongmooDialog(getActivity(), ChongmooDialog.MODE_LIST);
                                    dialog.setList(new String[]{getString(R.string.dialog_chongmoo_list_1), getString(R.string.dialog_chongmoo_list_2)}, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case 0: {
                                                    Intent intent = new Intent(getActivity(), AddressBookActivity.class);
                                                    startActivityForResult(intent, REQUEST_ADDRESS);
                                                    break;
                                                }
                                                case 1: {
                                                    Intent intent = new Intent(getActivity(), MemberWriteActivity.class);
                                                    startActivityForResult(intent, REQUEST_ADDRESS);
                                                    break;
                                                }
                                            }
                                        }
                                    });
                                    dialog.show();
                                }

                                @Override
                                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                    Toast.makeText(getActivity(), getString(R.string.toast_authority_deny_txt) + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setDeniedMessage(getString(R.string.toast_casual_meeting_file_write_role_txt))
                            .setPermissions(Manifest.permission.READ_CONTACTS)
                            .check();
                    return;
                }

                ChongmooDialog dialog = new ChongmooDialog(getActivity(), ChongmooDialog.MODE_LIST);
                dialog.setList(new String[]{getString(R.string.dialog_chongmoo_list_1), getString(R.string.dialog_chongmoo_list_2)}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0: {
                                Intent intent = new Intent(getActivity(), AddressBookActivity.class);
                                startActivityForResult(intent, REQUEST_ADDRESS);
                                break;
                            }
                            case 1: {
                                Intent intent = new Intent(getActivity(), MemberWriteActivity.class);
                                startActivityForResult(intent, REQUEST_ADDRESS);
                                break;
                            }
                        }
                    }
                });
                dialog.show();
                break;
            }
            case R.id.btn_month_left: {
                mMonth = mMonth - 1;
                if (mMonth < 0) {
                    mMonth = 11;
                    mYear--;
                }
                refreshList();
                break;
            }
            case R.id.text_month: {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, mYear);
                cal.set(Calendar.MONTH, mMonth);

                final ChongmooDialog dialog = new ChongmooDialog(getActivity(), ChongmooDialog.MODE_CALRENDAR_MONTH, false);
                dialog.setTitle(getString(R.string.dialog_calendar_text));
                dialog.setDate(new Date(cal.getTimeInMillis()));
                dialog.setPositiveButton(getString(R.string.dialog_calendar_positive_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(dialog.getDate().getTime());

                        mYear = cal.get(Calendar.YEAR);
                        mMonth = cal.get(Calendar.MONTH);

                        refreshList();
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(getString(R.string.dialog_calendar_negative_text), null);
                dialog.show();
                break;
            }
            case R.id.btn_month_right: {
                mMonth = mMonth + 1;
                if (mMonth > 11) {
                    mMonth = 0;
                    mYear++;
                }
                refreshList();
                break;
            }
        }
    }

    public void refreshList() {
        final int scrollY = mScrollY;
        mScrollY = 0;
        mTextMonth.setText(String.format(getString(R.string.fragment_income_month), mYear, mMonth+1));

        RealmQuery<RegularMeeting> meetingRealmQuery = mRealm.where(RegularMeeting.class);
        RegularMeeting regularMeeting = meetingRealmQuery.equalTo("seq", ((RegularMeetingActivity)getActivity()).getSeq()).findFirst();
        RealmList<MonthlyFee> monthlyFeeList = regularMeeting.getMonthlyFeeList();

        RealmQuery<Member> memberQuery = mRealm.where(Member.class);
        RealmResults<Member> memberList = memberQuery.equalTo("meetingSeq", ((RegularMeetingActivity)getActivity()).getSeq()).findAll();
        int total = 0;

        mLayoutEmpty.setVisibility(memberList.size() == 0 ? View.VISIBLE : View.GONE);
        mLayoutStatus.setVisibility(memberList.size() == 0 ? View.GONE : View.VISIBLE);

        for(Member member : memberList) {
            if(member.getOutDate() != null)
                continue;

            RealmQuery<Income> query = mRealm.where(Income.class);
            RealmResults<Income> incomeList = query.equalTo("memberSeq", member.getSeq()).findAllSorted("date", Sort.ASCENDING);

            int selectedYear = DateUtil.getYear(new Date());
            int selectedMonth = DateUtil.getMonth(new Date());

            int startYear = DateUtil.getYear(member.getStartDate());
            int startMonth = DateUtil.getMonth(member.getStartDate());

            int outYear = DateUtil.getYear(member.getOutDate());
            int outMonth = DateUtil.getMonth(member.getOutDate());

            RealmList<Exemption> exemptionList = member.getExemptionList();
            int status = PaymentStatus.STATUS_NOTHING;
            if (startYear < selectedYear || (startYear == selectedYear && startMonth <= selectedMonth)) {
                if (outYear > -1 && (outYear < selectedYear || (outYear == selectedYear && outMonth < selectedMonth))) {
                    status = PaymentStatus.STATUS_NOTHING;
                } else {
                    status = PaymentStatus.STATUS_NOT_PAID;

                    ArrayList<Income> list = new ArrayList<>();
                    for (Income ic : incomeList) {
                        list.add(new Income(ic));
                    }

                    int y = startYear;
                    int m = startMonth;
                    long fee = 0;
                    loop:
                    while (y < selectedYear || (y == selectedYear && m <= selectedMonth)) {
                        for(Exemption exemption : exemptionList) {
                            if (Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", y, m+1))) {
                                m++;
                                if(m > 11){
                                    m %= 12;
                                    y++;
                                }
                                continue loop;
                            }
                        }

                        int size = monthlyFeeList.size();
                        for (MonthlyFee mf : monthlyFeeList) {
                            if (Integer.parseInt(mf.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m + 1))) {
                                fee = mf.getAmount();
                                size--;
                            }
                        }

                        if (list.size() > 0) {
                            Income currnetIncome = list.get(0);
                            currnetIncome.setAmount(currnetIncome.getAmount() - fee);
                            long result = currnetIncome.getAmount();
                            if (result == 0)
                                list.remove(0);
                        } else {
                            total += fee;
                        }

                        m++;
                        if (m > 11) {
                            m %= 12;
                            y++;
                        }
                    }
                }
            }
        }

        mTextTotal.setText(String.format(getString(R.string.fragment_paymnet_status_fee_total), total));

        int month = ((mMonth+1) - 4) > 0 ? ((mMonth+1) - 4) : 12 + ((mMonth+1) - 4);
        for(int i=1; i<mLayoutTitle.getChildCount(); i++){
            View view = mLayoutTitle.getChildAt(i);
            if(view instanceof TextView){
                int newMonth = (month++%12);
                if(newMonth == 0){
                    newMonth = 12;
                }

                if(!((TextView) view).getText().equals(getString(R.string.fragment_paymnet_status_gubun))){
                    ((TextView) view).setText(newMonth + getString(R.string.fragment_paymnet_status_month));
                }
            }
        }

        int sYear = mYear;
        int sMonth = mMonth - 4;
        if (sMonth < 0) {
            sMonth = 12 + (mMonth - 4);
            sYear--;
        }

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, sYear);
        cal.set(Calendar.MONTH, sMonth);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        RealmQuery<Member> query = mRealm.where(Member.class);
        RealmResults<Member> results =
                query.equalTo("meetingSeq", ((RegularMeetingActivity) getActivity()).getSeq()).isNull("outDate")
                .or().equalTo("meetingSeq", ((RegularMeetingActivity) getActivity()).getSeq()).greaterThan("outDate", new Date(cal.getTimeInMillis()))
                .findAllSorted("name", Sort.ASCENDING);
        results = results.sort("outDate", Sort.ASCENDING);


        mRecyclerPaymentStatus.setAdapter(new BeautifulRecyclerAdapter<>(getContext(), PaymentStatusHolder.RESOURCE_ID, results, new PaymentStatusHolder.CommonCoordinator(getActivity(), this, mRealm, mMode, mRecyclerPaymentStatus)));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mRecyclerPaymentStatus.scrollBy(0, scrollY);
            }
        }, 0);
    }

    public Realm getRealm(){
        return mRealm;
    }

    public int getYear() {
        return mYear;
    }

    public int getMonth() {
        return mMonth;
    }

    public int getScrollY() {
        return mScrollY;
    }
}
