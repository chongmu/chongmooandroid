package kr.co.chongmoo.chongmooandroid.util;

import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

/**
 * 문자열 관련 유틸 클래스
 */
public class StringUtil {

    public static final String EMPTY_STRING = "";

    /**
     * 빈문자열인지 체크한다.
     * @param input
     * @return
     */
    public static boolean isNotEmpty(String input) {

        boolean returnStatus = false;
        if(input !=  null && EMPTY_STRING.equals(input) == false) {
            returnStatus = true;
        }
        return returnStatus;

    }

    /**
     * 금액일때 String을 int로 바꾼다. (내부에 "," 가 있을 경우 알아서 제외 시켜준다.)
     * @param input
     * @return
     */
    public static int parseToInterger(String input) {

        int returnValue = 0;
        input = input.replaceAll(",", "");
        returnValue = (input.equals(EMPTY_STRING) || input == null) ? 0 : Integer.parseInt(input);
        return returnValue;

    }

    /**
     * 백업 파일 명 가져오기
     * @param groupName
     * @return
     */
    public static String getBackupFileName(String groupName) {
        return String.format("%s_%s.realm", groupName, DateUtil.getCurrentYyyymmddHHmmss());
    }

    public static boolean compareSameString(String input1, String input2) {

        if(input1 == null && input2 == null) {
            return true;
        } else if(input1 == null || input2 == null) {
            return false;
        } else if(input1.equals(input2) == false) {
            return false;
        } else {
            return true;
        }

    }

    public static void setTextViewColorPartial(TextView view, int color, String fulltext, String... targetTexts) {
        view.setText(fulltext, TextView.BufferType.SPANNABLE);
        Spannable str = (Spannable) view.getText();
        for(String target : targetTexts){
            int i = fulltext.indexOf(target);
            str.setSpan(new ForegroundColorSpan(color), i, i + target.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }
}