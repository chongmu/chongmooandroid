package kr.co.chongmoo.chongmooandroid.ui.regular.setting;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.woojinsoft.chongmoo.android.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import kr.co.chongmoo.chongmooandroid.model.BackupFile;
import kr.co.chongmoo.chongmooandroid.model.ClosingAccount;
import kr.co.chongmoo.chongmooandroid.model.Exemption;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.MonthlyFee;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.ui.holder.BackupFileHolder;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.util.StringUtil;
import kr.co.chongmoo.chongmooandroid.util.sort.BackupFileComparator;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 백업 파일 목록 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class BackupFileListActivity extends BaseSubActivity {

    final static private String BACKUP_FILE_SUBFIX_REGEX = ".*realm$";

    final static private String EXCLUDE_TEXT_RECYCLE = "recycle";
    final static private String EXCLUDE_TEXT_PACKAGE_NAME = "com.woojinsoft.chongmoo.android";

    final static private String RESTORE_NAME = "restore.realm";

    private Realm mRealm;

    private RecyclerView mRecyclerBackupFileList;
    private LinearLayout mLayoutEmpty;

    private int mSeq;

    private ChongmooDialog mChongmooDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backup_file_list);
        setTitle(getString(R.string.title_activity_restore));

        mRecyclerBackupFileList = (RecyclerView) findViewById(R.id.recycler_backup_file_list);
        mRecyclerBackupFileList.setLayoutManager(new LinearLayoutManager(BackupFileListActivity.this));

        mLayoutEmpty = (LinearLayout) findViewById(R.id.layout_empty);

        TextView textGuide3 = (TextView) findViewById(R.id.text_guide_3);
        StringUtil.setTextViewColorPartial(textGuide3, 0XFFB0071F, getString(R.string.restore_guide_text_3), getString(R.string.restore_guide_text_3_highlight_1), getString(R.string.restore_guide_text_3_highlight_2));

        mSeq = getIntent().getIntExtra(BackupActivity.MEETING_SEQ, 0);
        mRealm = Realm.getDefaultInstance();

        int permissionCheck = ContextCompat.checkSelfPermission(BackupFileListActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if(permissionCheck == PackageManager.PERMISSION_DENIED) { // 마시멜로우 버젼 관련 체크
            new TedPermission(BackupFileListActivity.this)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            Toast.makeText(BackupFileListActivity.this, getString(R.string.toast_authority_permit_txt), Toast.LENGTH_SHORT).show();
                            showBackupFileList();
                        }

                        @Override
                        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                            Toast.makeText(BackupFileListActivity.this, getString(R.string.toast_authority_deny_txt) + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setDeniedMessage(getString(R.string.toast_casual_meeting_file_read_role_txt))
                    .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                    .check();
        } else {
            showBackupFileList();
        }

    }

    /**
     * 백업 파일 목록을 그린다.
     */
    public void showBackupFileList() {

        final Handler handler = new Handler();
        mChongmooDialog = new ChongmooDialog(BackupFileListActivity.this, ChongmooDialog.MODE_LOADING);
        mChongmooDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {

                String sdCardRoot = Environment.getExternalStorageDirectory().getAbsolutePath();

                final List<BackupFile> backupFileList = getBackupList(sdCardRoot);
                Collections.sort(backupFileList, new BackupFileComparator());
                for(BackupFile backupFile : backupFileList) {
                    backupFile.setSeq(mSeq);
                }

                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        if(backupFileList.size() > 0){
                            mRecyclerBackupFileList.setVisibility(View.VISIBLE);
                            mLayoutEmpty.setVisibility(View.GONE);
                            mRecyclerBackupFileList.setAdapter(
                                    new BeautifulRecyclerAdapter<>(
                                            BackupFileListActivity.this, BackupFileHolder.RESOURCE_ID, backupFileList, new BackupFileHolder.CommonCoordinator(BackupFileListActivity.this)
                                    )
                            );
                        }
                        else{
                            mRecyclerBackupFileList.setVisibility(View.GONE);
                            mLayoutEmpty.setVisibility(View.VISIBLE);
                        }

                        mChongmooDialog.dismiss();

                    }
                });
            }

        }).start();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    /**
     * 해당 경로에 있는 파일을 읽어서 해당 시퀀스에 있는 컬럼을 교체한다.
     * @param   path
     * @param   seq
     * @return  boolean
     */
    public boolean restore(String path, String fileName, int seq) {

        boolean processStatus = false;

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 저장할 메모리 선언 시작
        List<Member> memberList = new ArrayList<>();
        List<Income> incomeList = new ArrayList<>();
        List<ClosingAccount> closingAccountList = new ArrayList<>();
        RegularMeeting regularMeeting = new RegularMeeting();
        // 저장할 메모리 선언 종료
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 백업 파일에서 데이터를 가져오는 로직 시작
        String parentPath = path.replaceAll(fileName, "");
        File file = new File(parentPath);

        Log.d("RESTORE", String.format("%s::%s::%s::%s::%s", path, parentPath, fileName, seq, file.getAbsolutePath()));
        Realm restoreRealm = Realm.getInstance(new RealmConfiguration.Builder(file).name(fileName).build());
        Log.d("RESTORE", restoreRealm.getPath());

        RealmQuery<Member> queryMember = restoreRealm.where(Member.class);
        RealmResults<Member> members = queryMember.findAll();
        for(Member member : members) {
            Member memberClone = new Member();
            memberClone.setSeq(member.getSeq());
            memberClone.setMeetingSeq(seq);
            memberClone.setStartDate(member.getStartDate());
            memberClone.setRegistDate(member.getRegistDate());
            memberClone.setMemo(member.getMemo());
            memberClone.setName(member.getName());
            memberClone.setOutDate(member.getOutDate());
            RealmList<Exemption> exemptionListInForClone = new RealmList<>();
            RealmList<Exemption> exemptionListInFor = member.getExemptionList();
            for(Exemption exemption : exemptionListInFor) {
                Exemption exemptionClone = new Exemption();
                exemptionClone.setDate(exemption.getDate());
                exemptionListInForClone.add(exemptionClone);
            }
            memberClone.setExemptionList(exemptionListInForClone);
            memberList.add(memberClone); // Log.d("RESTORE", member.getName() + "::" + member.getMemo());
        }

        RealmQuery<Income> queryIncome = restoreRealm.where(Income.class);
        RealmResults<Income> incomes = queryIncome.findAll();
        for(Income income : incomes) {
            Income incomeClone = new Income();
            incomeClone.setSeq(income.getSeq());
            incomeClone.setImageFile(income.getImageFile());
            incomeClone.setMemberSeq(income.getMemberSeq());
            incomeClone.setType(income.getType());
            incomeClone.setAmount(income.getAmount());
            incomeClone.setCategory(income.getCategory());
            incomeClone.setContent(income.getContent());
            incomeClone.setDate(income.getDate());
            incomeClone.setDay(income.getDay());
            incomeClone.setIsTop(income.isTop());
            incomeClone.setMeetingSeq(seq);
            incomeClone.setMemo(income.getMemo());
            incomeList.add(incomeClone);
        }

        RealmQuery<ClosingAccount> queryClosingAccount = restoreRealm.where(ClosingAccount.class);
        RealmResults<ClosingAccount> closingAccounts = queryClosingAccount.findAll();
        for(ClosingAccount closingAccount : closingAccounts) {
            ClosingAccount closingAccountClone = new ClosingAccount();
            closingAccountClone.setCode(closingAccount.getCode());
            closingAccountClone.setType(closingAccount.getType());
            closingAccountClone.setRegistDate(closingAccount.getRegistDate());
            closingAccountClone.setStartDate(closingAccount.getStartDate());
            closingAccountClone.setEndDate(closingAccount.getEndDate());
            closingAccountClone.setDataJson(closingAccount.getDataJson());
            closingAccountClone.setMeetingSeq(seq);
            closingAccountList.add(closingAccountClone);
        }

        RealmQuery <RegularMeeting> queryRegularMeeting = restoreRealm.where(RegularMeeting.class);
        RegularMeeting regularMeetingClone = queryRegularMeeting.findAll().first(); // Log.d("RESTORE", regularMeeting.getName() + "::" + regularMeeting.getStartdate());
        regularMeeting.setSeq(seq);
        regularMeeting.setRegistDate(regularMeetingClone.getRegistDate());
        regularMeeting.setStartdate(regularMeetingClone.getStartdate());
        regularMeeting.setAccountHolder(regularMeetingClone.getAccountHolder());
        regularMeeting.setAccountNumber(regularMeetingClone.getAccountNumber());
        regularMeeting.setBank(regularMeetingClone.getBank());
        regularMeeting.setModifyDate(regularMeetingClone.getModifyDate());
        regularMeeting.setName(regularMeetingClone.getName());
        RealmList<MonthlyFee> monthlyFeeList = new RealmList<>();
        RealmList<MonthlyFee> monthlyFeeRealmList =  regularMeetingClone.getMonthlyFeeList();
        for(MonthlyFee monthlyFeeClone : monthlyFeeRealmList) {
            MonthlyFee monthlyFee = new MonthlyFee();
            monthlyFee.setAmount(monthlyFeeClone.getAmount());
            monthlyFee.setDate(monthlyFeeClone.getDate());
            monthlyFeeList.add(monthlyFee);
        }
        regularMeeting.setMonthlyFeeList(monthlyFeeList);
        restoreRealm.close();
        // 백업 파일에서 데이터를 가져오는 로직 종료
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 기존 데이터와 교체 로직 시작
        RealmQuery<RegularMeeting> queryRegularMeetingRestore = mRealm.where(RegularMeeting.class);
        RealmQuery<Income> queryIncomeRestore = mRealm.where(Income.class);
        RealmQuery<Member> queryMemberRestore = mRealm.where(Member.class);
        RealmQuery<ClosingAccount> queryClosingAccountRestore = mRealm.where(ClosingAccount.class);

        RegularMeeting regularMeetingRestore = queryRegularMeetingRestore.equalTo("seq", seq).findFirst();
        long queryIncomeRestoreCount = queryIncomeRestore.findAll().size();
        long queryMemberRestoreCount = queryMemberRestore.findAll().size();
        long queryClosingAccountRestoreCount = queryClosingAccountRestore.findAll().size();

        RealmResults<Income> incomesRestore = null;
        RealmResults<Member> membersRestore = null;
        RealmResults<ClosingAccount> closingAccountRestore = null;

        if(queryIncomeRestoreCount > 0) incomesRestore = queryIncomeRestore.equalTo("meetingSeq", seq).findAll();
        if(queryMemberRestoreCount > 0) membersRestore = queryMemberRestore.equalTo("meetingSeq", seq).findAll();
        if(queryClosingAccountRestoreCount > 0) closingAccountRestore = queryClosingAccountRestore.equalTo("meetingSeq", seq).findAll();

        mRealm.beginTransaction();

        regularMeetingRestore.setName(regularMeeting.getName());
        if(regularMeeting.getStartdate() != null ) regularMeetingRestore.setStartdate(regularMeeting.getStartdate());
        regularMeetingRestore.setBank(regularMeeting.getBank());
        regularMeetingRestore.setAccountHolder(regularMeeting.getAccountHolder());
        regularMeetingRestore.setAccountNumber(regularMeeting.getAccountNumber());
        regularMeetingRestore.setRegistDate(regularMeeting.getRegistDate());
        regularMeetingRestore.setModifyDate(regularMeeting.getModifyDate());

        RealmList<MonthlyFee> monthlyFeeListRestore = new RealmList<>();
        for(MonthlyFee monthlyFeeClone : monthlyFeeList) {
            MonthlyFee monthlyFee = mRealm.createObject(MonthlyFee.class);
            monthlyFee.setAmount(monthlyFeeClone.getAmount());
            monthlyFee.setDate(monthlyFeeClone.getDate());
            monthlyFeeListRestore.add(monthlyFee);
        }
        regularMeetingRestore.setMonthlyFeeList(monthlyFeeListRestore);

        if(queryIncomeRestoreCount > 0) incomesRestore.deleteAllFromRealm();
        if(queryMemberRestoreCount > 0) membersRestore.deleteAllFromRealm();
        if(queryClosingAccountRestoreCount > 0) closingAccountRestore.deleteAllFromRealm();

        for(Member member : memberList) {
            int seqBefore = member.getSeq();
            int seqAfter = Member.generateSequence(mRealm);
            for(Income income : incomeList) { // 맴버가 새로 들어가므로 기존 seq를 바꿔줘야 한다. 그러므로 찾아서 바꿔준다.
                if(income.getMemberSeq() == seqBefore) {
                    income.setMemberSeq(seqAfter);
                }
            }
            member.setSeq(seqAfter);
            mRealm.copyToRealm(member);
        }

        for(Income income : incomeList) {
            Income incomeClone = mRealm.createObject(Income.class);
            incomeClone.setSeq(Income.generateSequence(mRealm));
            incomeClone.setImageFile(income.getImageFile());
            incomeClone.setMemberSeq(income.getMemberSeq());
            incomeClone.setType(income.getType());
            incomeClone.setAmount(income.getAmount());
            incomeClone.setCategory(income.getCategory());
            incomeClone.setContent(income.getContent());
            incomeClone.setDate(income.getDate());
            incomeClone.setDay(income.getDay());
            incomeClone.setIsTop(income.isTop());
            incomeClone.setMeetingSeq(income.getMeetingSeq());
            incomeClone.setMemo(income.getMemo());
        }

        for(ClosingAccount closingAccount : closingAccountList) {
            ClosingAccount closingAccountClone = mRealm.createObject(ClosingAccount.class);
            closingAccountClone.setCode(closingAccount.getCode());
            closingAccountClone.setType(closingAccount.getType());
            closingAccountClone.setRegistDate(closingAccount.getRegistDate());
            closingAccountClone.setStartDate(closingAccount.getStartDate());
            closingAccountClone.setEndDate(closingAccount.getEndDate());
            closingAccountClone.setDataJson(closingAccount.getDataJson());
            closingAccountClone.setMeetingSeq(closingAccount.getMeetingSeq());
            closingAccountClone.setSeq(ClosingAccount.generateSequence(mRealm));
        }

        mRealm.commitTransaction();

        // 기존 데이터와 교체 로직 종료
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        processStatus = true;
        Log.d("RESTORE", "FINISHED RESTORE");

        return processStatus;

    }

    /**
     * 백업 대상 리스트 목록을 sdcard를 다 조회하여 가져온다.
     * @param   path
     * @return  List<BackupFile>
     */
    private List<BackupFile> getBackupList(String path) {

        List<BackupFile> backupFileList = new ArrayList<>();

        File file = new File(path);

        boolean isDirectoryStatus = file.isDirectory();
        File[] fileArray = file.listFiles();

        if(fileArray != null && fileArray.length > 0) {
            for (File fileInFor : fileArray) {

                String fileName = fileInFor.getName();
                String absolutePath = fileInFor.getAbsolutePath();

                //Log.d("FILE_DEBUG", absolutePath);
                boolean isDirectoryStatusInFor = fileInFor.isDirectory();

                // 백업 파일일 경우 리스트에 넣는다.
                if (isDirectoryStatusInFor == true && absolutePath.contains(EXCLUDE_TEXT_RECYCLE) == false && absolutePath.contains(EXCLUDE_TEXT_PACKAGE_NAME) == false) {

                    List<BackupFile> backupFileListInFor = getBackupList(absolutePath);
                    backupFileList.addAll(backupFileListInFor);

                } else {

                    boolean matchStatus = fileName.matches(BACKUP_FILE_SUBFIX_REGEX);
                    if (matchStatus == true) {

                        BackupFile backupFile = new BackupFile();
                        backupFile.setFileName(fileName);
                        backupFile.setFileSize(Integer.toString((int) (fileInFor.length() / 1024)));
                        backupFile.setDate(DateUtil.getFileYyyymmddHHmm(new Date(fileInFor.lastModified())));
                        backupFile.setSortDate(DateUtil.getYyyymmddHHmmss(new Date(fileInFor.lastModified())));
                        backupFile.setAbsolutePath(absolutePath);
                        backupFileList.add(backupFile);
                        Log.d("FILE_DEBUG", absolutePath);

                    } /*else if(fileName.contains("realm") == true) {

                        BackupFile backupFile = new BackupFile();
                        backupFile.setFileName(fileName);
                        backupFile.setFileSize(Integer.toString((int) (fileInFor.length() / 1024)));
                        backupFile.setDate(DateUtil.getFileYyyymmddHHmm(new Date(fileInFor.lastModified())));
                        backupFile.setAbsolutePath(absolutePath);
                        backupFileList.add(backupFile);
                        Log.d("FILE_DEBUG", absolutePath);

                    }*/

                }

            }
        }

        return backupFileList;

    }

}
