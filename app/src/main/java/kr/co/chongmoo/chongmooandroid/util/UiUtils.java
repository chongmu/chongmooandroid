package kr.co.chongmoo.chongmooandroid.util;

import android.content.Context;

/**
 * 화면 관련 사이즈 관련 유틸
 */
public class UiUtils {
    public static float getDpi(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static int getPixelFromDp(Context context, float dp) {
        return (int) (getDpi(context) * dp);
    }

    public static float getPixelsFromDp(Context context, float dp) {
        return getDpi(context) * dp;
    }

    public static int getDisplayWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getDisplayHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }


}
