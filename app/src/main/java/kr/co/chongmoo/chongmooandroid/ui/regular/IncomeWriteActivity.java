package kr.co.chongmoo.chongmooandroid.ui.regular;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.woojinsoft.chongmoo.android.R;

import io.realm.RealmResults;
import io.realm.Sort;
import kr.co.chongmoo.chongmooandroid.model.Exemption;
import kr.co.chongmoo.chongmooandroid.model.HttpResponse;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.MonthlyFee;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.network.RetrofitManager;
import kr.co.chongmoo.chongmooandroid.network.service.APIService;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.ui.common.MemberSelectActivity;
import kr.co.chongmoo.chongmooandroid.ui.common.MoneyCommaEditTextWather;
import kr.co.chongmoo.chongmooandroid.ui.common.ImageCropActivity;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.util.ImageUtil;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 * 수입/지출 작성 화면 
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class IncomeWriteActivity extends BaseSubActivity implements View.OnClickListener {

    public static final int REQUEST_TAKE_PICTURE = 1;
    public static final int REQUEST_SELECT_PICTURE = 2;
    public static final int REQUEST_CROP_IMAGE = 3;
    public static final int REQUEST_SELECT_MEMBER = 4;


    public static final String EXTRA_MODE = "mode";
    public static final int MODE_WRITE = 1;
    public static final int MODE_READ = 2;
    public static final int MODE_UPDATE = 3;

    private ScrollView mScroll;

    private TextView mTextDate;
    private Button mBtnCalendar;

    private RadioGroup mGroupType;
    private RadioButton mRadioIncome;
    private RadioButton mRadioOutcome;
    private EditText mEditAmount;
    private Button mBtnCategory;
    private EditText mEditContent;
    private TextView mTextContent;
    private EditText mEditMemo;
    private Button mBtnCamera;
    private Button mBtnPicture;
    private TextView mTextPicture;
    private LinearLayout mLayoutImage;

    private RelativeLayout mLayoutPicture;
    private ImageView mImagePicture;
    private Button mBtnDeletePicture;

    private LinearLayout mLayoutPrev;
    private LinearLayout mLayoutDelete;
    private LinearLayout mLayoutSaveContinue;
    private LinearLayout mLayoutSave;
    private TextView mTextSave;

    private Handler mHandler;

    private Realm mRealm;

    private int mMode = MODE_READ;
    private int mSeq;
    private int mMeetingSeq;
    private int mMemberSeq;

    private Date mDate;
    private Bitmap mPicture;

    private Income mIncome;

    private List<String> mIncomeCategoryList = new ArrayList<>();
    private List<String> mOutcomeCategoryList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_income_write);
        setTitle(R.string.activity_income_write_title);

        mScroll = (ScrollView) findViewById(R.id.scroll);
        mTextDate = (TextView) findViewById(R.id.text_date);
        mBtnCalendar = (Button) findViewById(R.id.btn_calendar);
        mGroupType = (RadioGroup) findViewById(R.id.group_type);
        mRadioIncome = (RadioButton) findViewById(R.id.radio_income);
        mRadioOutcome = (RadioButton) findViewById(R.id.radio_outcome);
        mEditAmount = (EditText) findViewById(R.id.edit_amount);
        mBtnCategory = (Button) findViewById(R.id.btn_category);
        mEditContent = (EditText) findViewById(R.id.edit_content);
        mTextContent = (TextView) findViewById(R.id.text_content);
        mEditMemo = (EditText) findViewById(R.id.edit_memo);
        mBtnCamera = (Button) findViewById(R.id.btn_camera);
        mBtnPicture = (Button) findViewById(R.id.btn_picture);
        mTextPicture = (TextView) findViewById(R.id.text_picture);
        mLayoutImage = (LinearLayout) findViewById(R.id.layout_image);
        mLayoutPicture = (RelativeLayout) findViewById(R.id.layout_picture);
        mImagePicture = (ImageView) findViewById(R.id.image_picture);
        mBtnDeletePicture = (Button) findViewById(R.id.btn_delete_image);
        mLayoutPrev = (LinearLayout) findViewById(R.id.layout_prev);
        mLayoutDelete = (LinearLayout) findViewById(R.id.layout_delete);
        mLayoutSaveContinue = (LinearLayout) findViewById(R.id.layout_save_continue);
        mLayoutSave = (LinearLayout) findViewById(R.id.layout_save);
        mTextSave = (TextView) findViewById(R.id.text_save);

        mTextDate.setOnClickListener(this);
        mBtnCalendar.setOnClickListener(this);
        mBtnCategory.setOnClickListener(this);
        mTextContent.setOnClickListener(this);
        mBtnCamera.setOnClickListener(this);
        mBtnPicture.setOnClickListener(this);
        mBtnDeletePicture.setOnClickListener(this);
        mLayoutPrev.setOnClickListener(this);
        mLayoutDelete.setOnClickListener(this);
        mLayoutSaveContinue.setOnClickListener(this);
        mLayoutSave.setOnClickListener(this);

        mGroupType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mLayoutImage.setVisibility(checkedId == R.id.radio_income ? View.GONE : View.VISIBLE);

                if(checkedId == R.id.radio_income){
                    mLayoutPicture.setVisibility(View.GONE);
                }
                else{
                    if(mPicture != null){
                        mLayoutPicture.setVisibility(View.VISIBLE);
                    }
                    else{
                        mLayoutPicture.setVisibility(View.GONE);
                    }
                }

//                mBtnPicture.setVisibility(checkedId == R.id.radio_income ? View.GONE : View.VISIBLE);
//                mTextPicture.setVisibility(checkedId == R.id.radio_income ? View.GONE : View.VISIBLE);

                if(mBtnCategory.getText().toString().equals(getString(R.string.activity_income_write_category_fee))){
                    mEditContent.setText("");
                    mTextContent.setText("");
                    mEditContent.setVisibility(View.VISIBLE);
                    mTextContent.setVisibility(View.GONE);
                }
                mBtnCategory.setText(getString(R.string.activity_income_write_category_nothing));
            }
        });

        mEditAmount.addTextChangedListener(new MoneyCommaEditTextWather(mEditAmount));

        mEditMemo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_UP)
//                    mScroll.requestDisallowInterceptTouchEvent(false);
//                else
//                    mScroll.requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        mTextContent.setActivated(true);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            mMode = bundle.getInt(EXTRA_MODE);
            mSeq = bundle.getInt("SEQ", 0);
            mMeetingSeq = bundle.getInt("MEETING_SEQ", 0);
        }

        mDate = new Date();
        setTextDate(mTextDate, mDate);

        changeMode(mMode);

        mHandler = new Handler();
        mRealm = Realm.getDefaultInstance();

        if(mSeq != 0) {
            mRealm = Realm.getDefaultInstance();
            RealmQuery<Income> query = mRealm.where(Income.class);
            mIncome = query.equalTo("seq", mSeq).findFirst();

            refreshUi();
        }

        final ChongmooDialog dialg = new ChongmooDialog(this, ChongmooDialog.MODE_LOADING);
        dialg.show();

        final APIService apiService = RetrofitManager.getInstance().create(APIService.class);
        Call<HttpResponse<List<String>>> call = apiService.incomeTypeList();
        call.enqueue(new Callback<HttpResponse<List<String>>>() {
            @Override
            public void onResponse(Call<HttpResponse<List<String>>> call, Response<HttpResponse<List<String>>> response) {
                try {
                    int code = response.code();
                    if (code != 200) {
                        throw new Exception("Error : " + code + " : " + response.message());
                    }

                    HttpResponse<List<String>> httpResponse = response.body();
                    mIncomeCategoryList = httpResponse.getResults();


                    Call<HttpResponse<List<String>>> calls = apiService.spendingTypeList();
                    calls.enqueue(new Callback<HttpResponse<List<String>>>() {
                        @Override
                        public void onResponse(Call<HttpResponse<List<String>>> call, Response<HttpResponse<List<String>>> response) {
                            try {

                                int code = response.code();
                                if (code != 200) {
                                    throw new Exception("Error : " + code + " : " + response.message());
                                }

                                HttpResponse<List<String>> httpResponse = response.body();
                                mOutcomeCategoryList = httpResponse.getResults();
                                dialg.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(IncomeWriteActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                dialg.dismiss();
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<HttpResponse<List<String>>> call, Throwable t) {
                            Toast.makeText(IncomeWriteActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            dialg.dismiss();
                            finish();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(IncomeWriteActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    dialg.dismiss();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<HttpResponse<List<String>>> call, Throwable t) {
                Toast.makeText(IncomeWriteActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                dialg.dismiss();
                finish();
            }
        });


    }

    private void refreshUi(){
        mDate = mIncome.getDate();
        setTextDate(mTextDate, mDate);
        mGroupType.check(mIncome.getType() == 0 ? R.id.radio_income : R.id.radio_outcome);
        if(mMode != MODE_READ)
            mLayoutImage.setVisibility(mIncome.getType() == 0 ? View.GONE : View.VISIBLE);
        else
            mLayoutImage.setVisibility(View.GONE);
        mLayoutPicture.setVisibility(mIncome.getType() == 0 ? View.GONE : View.VISIBLE);
//        mBtnPicture.setVisibility(mIncome.getType() == 0 ? View.GONE : View.VISIBLE);
//        mTextPicture.setVisibility(mIncome.getType() == 0 ? View.GONE : View.VISIBLE);
        mEditAmount.setText(mIncome.getAmount() > 0 ? mIncome.getAmount() + "" : "");

        String category = mIncome.getCategory();
        mBtnCategory.setText(category);
        if(category.equals(getString(R.string.activity_income_write_category_fee))){
            mEditContent.setVisibility(View.GONE);
            mTextContent.setVisibility(View.VISIBLE);
        }
        else{
            mEditContent.setVisibility(View.VISIBLE);
            mTextContent.setVisibility(View.GONE);
        }
        mEditContent.setText(mIncome.getContent());
        mTextContent.setText(mIncome.getContent());
        mMemberSeq = mIncome.getMemberSeq();

        mEditMemo.setText(mIncome.getMemo());
        byte[] imageFile = mIncome.getImageFile();
        if(imageFile != null && imageFile.length != 0){
            mPicture = ImageUtil.byteArrayToBitmap(imageFile);
            mImagePicture.setImageBitmap(mPicture);
            mLayoutPicture.setVisibility(View.VISIBLE);
        }
        else{
            mPicture = null;
            mImagePicture.setImageBitmap(null);
            mLayoutPicture.setVisibility(View.GONE);
        }
    }

    private void changeMode(int mode){
        boolean isReadMode = (mode == MODE_READ);
        mBtnCalendar.setVisibility(isReadMode ? View.GONE : View.VISIBLE);
        mGroupType.setEnabled(isReadMode ? false : true);
        mRadioIncome.setEnabled(isReadMode ? false : true);
        mRadioOutcome.setEnabled(isReadMode ? false : true);
        mEditAmount.setEnabled(isReadMode ? false : true);
        mBtnCategory.setEnabled(isReadMode ? false : true);
        mEditContent.setEnabled(isReadMode ? false : true);
        mTextContent.setEnabled(isReadMode ? false : true);
        mEditMemo.setEnabled(isReadMode ? false : true);
        mLayoutImage.setVisibility(isReadMode ? View.GONE : View.VISIBLE);
        mBtnDeletePicture.setVisibility(isReadMode ? View.GONE : View.VISIBLE);

        mLayoutPrev.setVisibility(isReadMode ? View.GONE : View.VISIBLE);
        mLayoutDelete.setVisibility(isReadMode ? View.VISIBLE : View.GONE);
        mLayoutSaveContinue.setVisibility(View.GONE);

        mTextSave.setText(isReadMode ? R.string.modify : R.string.save);

        if(mode == MODE_WRITE){
            mLayoutImage.setVisibility(View.GONE);
            mLayoutPrev.setVisibility(View.GONE);
            mLayoutDelete.setVisibility(View.GONE);
            mLayoutSaveContinue.setVisibility(View.VISIBLE);
            mTextSave.setText(R.string.save);
        }

        hideKeyboard();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case REQUEST_TAKE_PICTURE:
                if(resultCode == RESULT_OK) {
                    File tempDir = new File(Environment.getExternalStorageDirectory() + File.separator + "chongmoo" + File.separator + "temp");
                    if(!tempDir.exists()){
                        tempDir.mkdirs();
                    }

                    File tempFile = new File(tempDir, "temp.jpg");
                    Intent intent = new Intent(IncomeWriteActivity.this, ImageCropActivity.class);
                    intent.putExtra(ImageCropActivity.EXTRA_IMAGE_PATH, tempFile.getAbsolutePath());
                    startActivityForResult(intent, REQUEST_CROP_IMAGE);
                }
                break;
            case REQUEST_SELECT_PICTURE:
                if(resultCode == RESULT_OK) {
                    try {
                        Bitmap original = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                        int height = original.getHeight();
                        int width = original.getWidth();
                        mPicture = null;
                        if(width > 1080){
                            mPicture = Bitmap.createScaledBitmap(original, 1080, (height * 1080) / width, true);
                            original.recycle();
                            original = null;
                        }
                        else{
                            mPicture = original;
                        }

                        final ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_LOADING);
                        dialog.show();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                File tempDir = new File(Environment.getExternalStorageDirectory() + File.separator + "chongmoo" + File.separator + "temp");
                                if(!tempDir.exists()){
                                    tempDir.mkdirs();
                                }

                                final File tempFile = new File(tempDir, "temp.jpg");
                                FileOutputStream fos = null;
                                try{
                                    tempFile.createNewFile();
                                    fos = new FileOutputStream(tempFile);
                                    mPicture.compress(Bitmap.CompressFormat.JPEG, 100, fos);

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            dialog.dismiss();
                                            Intent intent = new Intent(IncomeWriteActivity.this, ImageCropActivity.class);
                                            intent.putExtra(ImageCropActivity.EXTRA_IMAGE_PATH, tempFile.getAbsolutePath());
                                            startActivityForResult(intent, REQUEST_CROP_IMAGE);
                                        }
                                    });
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } finally {
                                    if(fos != null)
                                        try {
                                            fos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                }
                            }
                        }).start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case REQUEST_CROP_IMAGE:
                if(resultCode == RESULT_OK){
                    String imagePath = data.getStringExtra(ImageCropActivity.EXTRA_IMAGE_PATH);
                    File imgFile = new  File(imagePath);
                    if(imgFile.exists()){
                        mPicture = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        mImagePicture.setImageBitmap(mPicture);
                        mLayoutPicture.setVisibility(View.VISIBLE);
                    }
                }
                else{
                    if(mPicture !=null){
                        mPicture.recycle();
                        mPicture = null;
                    }
                }
                break;
            case REQUEST_SELECT_MEMBER:
                if(resultCode == RESULT_OK){
                    Bundle bundle = data.getExtras();
                    mMemberSeq = bundle.getInt("SEQ", 0);
                    mTextContent.setText(bundle.getString("NAME"));
                    mEditContent.setText(bundle.getString("NAME"));
                }
                break;
        }
    }

    private boolean isContinue = false;

    @Override
    public void onClick(View v) {
        isContinue = false;
        switch (v.getId()){
            case R.id.text_date:
            case R.id.btn_calendar:{
                final ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_CALRENDAR, false);
                dialog.setTitle(getString(R.string.dialog_calendar_text));
                dialog.setDate(mDate);
                dialog.setPositiveButton(getString(R.string.dialog_calendar_positive_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDate = dialog.getDate();
                        setTextDate(mTextDate, mDate);
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(getString(R.string.dialog_calendar_negative_text), null);
                dialog.show();
                break;
            }
            case R.id.btn_category:{
                final String[] itemArray = mGroupType.getCheckedRadioButtonId() == R.id.radio_income ?  mIncomeCategoryList.toArray(new String[]{}) : mOutcomeCategoryList.toArray(new String[]{});
                ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_LIST);
                dialog.setList(itemArray, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String category = itemArray[which];
                        mBtnCategory.setText(category);
                        if(category.equals(getString(R.string.activity_income_write_category_fee))){
                            mEditContent.setText("");
                            mEditContent.setVisibility(View.GONE);
                            mTextContent.setVisibility(View.VISIBLE);

                            Intent intent = new Intent(IncomeWriteActivity.this, MemberSelectActivity.class);
                            intent.putExtra("MEETING_SEQ", mMeetingSeq);
                            startActivityForResult(intent, REQUEST_SELECT_MEMBER);
                        }
                        else{
                            mMemberSeq = 0;
                            mEditContent.setText("");
                            mEditContent.setVisibility(View.VISIBLE);
                            mTextContent.setVisibility(View.GONE);
                        }
                       dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            }
            case R.id.text_content:{
                Intent intent = new Intent(IncomeWriteActivity.this, MemberSelectActivity.class);
                intent.putExtra("MEETING_SEQ", mMeetingSeq);
                startActivityForResult(intent, REQUEST_SELECT_MEMBER);
                break;
            }
            case R.id.btn_camera: {
                int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if(permissionCheck == PackageManager.PERMISSION_DENIED) { // 마시멜로우 버젼 관련 체크
                    new TedPermission(this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                File tempDir = new File(Environment.getExternalStorageDirectory() + File.separator + "chongmoo" + File.separator + "temp");
                                if(!tempDir.exists()){
                                    tempDir.mkdirs();
                                }

                                File tempFile = new File(tempDir, "temp.jpg");
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
                                startActivityForResult(intent, REQUEST_TAKE_PICTURE);
                            }

                            @Override
                            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                Toast.makeText(IncomeWriteActivity.this, getString(R.string.toast_authority_deny_txt) + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setDeniedMessage(getString(R.string.toast_casual_meeting_file_write_role_txt))
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check();
                    return;
                }

                File tempDir = new File(Environment.getExternalStorageDirectory() + File.separator + "chongmoo" + File.separator + "temp");
                if(!tempDir.exists()){
                    tempDir.mkdirs();
                }

                File tempFile = new File(tempDir, "temp.jpg");
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
                startActivityForResult(intent, REQUEST_TAKE_PICTURE);
                break;
            }
            case R.id.btn_picture: {
                int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if(permissionCheck == PackageManager.PERMISSION_DENIED) { // 마시멜로우 버젼 관련 체크
                    new TedPermission(this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                Intent intent = new Intent(Intent.ACTION_PICK);
                                intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
                                intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(intent, REQUEST_SELECT_PICTURE);
                            }

                            @Override
                            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                Toast.makeText(IncomeWriteActivity.this, getString(R.string.toast_authority_deny_txt) + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setDeniedMessage(getString(R.string.toast_casual_meeting_file_write_role_txt))
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check();
                    return;
                }

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
                intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, REQUEST_SELECT_PICTURE);
                break;
            }
            case R.id.btn_delete_image:
                mLayoutPicture.setVisibility(View.GONE);
                mPicture = null;
                break;
            case R.id.layout_delete:
                if(mIncome.getCategory().equals(getString(R.string.activity_income_write_category_fee))) {
                    RealmQuery<Income> query =  mRealm.where(Income.class);
                    RealmResults<Income> incomeList = query.equalTo("memberSeq", mIncome.getMemberSeq()).findAllSorted("date", Sort.ASCENDING);

                    for(Income income : incomeList){
                        if(income.getDate().getTime() > mIncome.getDate().getTime()){
                            Toast.makeText(getBaseContext(), R.string.toast_change_lately, Toast.LENGTH_SHORT).show();
                            return ;
                        }
                    }
                }

                ChongmooDialog dialog = new ChongmooDialog(this);
                dialog.setMessage(getString(R.string.dialog_chongmoo_income_delete));
                dialog.setPositiveButton(getString(R.string.yes), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                RealmQuery<Income> query = realm.where(Income.class);
                                Income income = query.equalTo("seq", mSeq).findFirst();
                                income.removeFromRealm();
                            }
                        }, new Realm.Transaction.Callback() {
                            @Override
                            public void onSuccess() {
                                finish();
                            }

                            @Override
                            public void onError(Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getBaseContext(), String.format(getString(R.string.dialog_chongmoo_income_delete_fail), e.toString()), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
                dialog.setNegativeButton(getString(R.string.no), null);
                dialog.show();
                break;
            case R.id.layout_prev:
                onBackPressed();
                break;
            case R.id.layout_save_continue:
                isContinue = true;
            case R.id.layout_save:
                if(mMode == MODE_READ){
                    if(mIncome.getCategory().equals("정기회비")) {
                        RealmQuery<Income> query =  mRealm.where(Income.class);
                        RealmResults<Income> incomeList = query.equalTo("memberSeq", mIncome.getMemberSeq()).findAllSorted("date", Sort.ASCENDING);

                        for(Income income : incomeList){
                            if(income.getDate().getTime() > mIncome.getDate().getTime()){
                                Toast.makeText(getBaseContext(), R.string.toast_change_lately, Toast.LENGTH_SHORT).show();
                                return ;
                            }
                        }
                    }
                    mMode = MODE_UPDATE;
                    changeMode(mMode);
                    refreshUi();
                }
                else if(mMode == MODE_UPDATE){
                    final int day = DateUtil.getDayOfMonth(mDate);
                    final int type = mGroupType.getCheckedRadioButtonId() == R.id.radio_income ? 0 : 1;
                    String amountString = mEditAmount.getText().toString().replaceAll(",", "");
                    final int amount = amountString.equals("") ? 0 : Integer.parseInt(amountString);
                    final String category = mBtnCategory.getText().toString();
                    final String content = mEditContent.getText().toString().trim();;
                    final String memo = mEditMemo.getText().toString();
                    final byte[] imageFile = ImageUtil.bitmapToByteArray(mPicture);

                    if(amount <= 0){
                        Toast.makeText(getBaseContext(), R.string.toast_require_amount, Toast.LENGTH_SHORT).show();
                        return ;
                    }
                    if(TextUtils.isEmpty(content)){
                        Toast.makeText(getBaseContext(), category.equals(getString(R.string.activity_income_write_category_fee)) ? R.string.toast_require_member_2 : R.string.toast_require_content, Toast.LENGTH_SHORT).show();
                        return ;
                    }

                    if(category.equals(getString(R.string.activity_income_write_category_fee))) {
                        RealmQuery<Member> memberQuery = mRealm.where(Member.class);
                        Member member = memberQuery.equalTo("seq", mMemberSeq).findFirst();

                        RealmQuery<RegularMeeting> meetingRealmQuery = mRealm.where(RegularMeeting.class);
                        RegularMeeting regularMeeting = meetingRealmQuery.equalTo("seq", member.getMeetingSeq()).findFirst();
                        RealmList<MonthlyFee> monthlyFeeList = regularMeeting.getMonthlyFeeList();

                        RealmQuery<Income> query = mRealm.where(Income.class);
                        RealmResults<Income> incomeList = query.equalTo("memberSeq", member.getSeq()).findAllSorted("date", Sort.ASCENDING);

                        long lastTime = 0;
                        int incomeAmount = 0;
                        for(Income income : incomeList){
                            incomeAmount += income.getAmount();
                            if(mIncome.getDate().getTime() <= income.getDate().getTime())
                                continue;

                            if(lastTime < income.getDate().getTime()){
                                lastTime = income.getDate().getTime();
                            }
                        }

                        if(mIncome.getDate().getTime() != mDate.getTime() && lastTime > mDate.getTime()){
                            Toast.makeText(getBaseContext(), String.format(getString(R.string.toast_lower_date_2), DateUtil.getDateToYearMonthDayDotString(new Date(lastTime))), Toast.LENGTH_SHORT).show();
                            return ;
                        }

                        int selectedYear = Integer.parseInt(monthlyFeeList.get(monthlyFeeList.size() - 1).getDate().substring(0, 4));
                        int selectedMonth = Integer.parseInt(monthlyFeeList.get(monthlyFeeList.size() - 1).getDate().substring(4, 6));

                        int startYear = DateUtil.getYear(member.getStartDate());
                        int startMonth = DateUtil.getMonth(member.getStartDate());
                        if(startYear > selectedYear || (startYear == selectedYear && startMonth > selectedMonth)){
                            selectedYear = startYear;
                            selectedMonth = startMonth;
                        }

                        RealmList<Exemption> exemptionList = member.getExemptionList();
                        int totalAmount = amount + incomeAmount;
                        int y = startYear;
                        int m = startMonth;
                        long fee = 0;
                        for(MonthlyFee monthlyFee : monthlyFeeList){
                            if(Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m+1))){
                                fee = monthlyFee.getAmount();
                            }
                        }

                        loop:
                        while (y < selectedYear || (y == selectedYear && m <= selectedMonth)) {
                            for (Exemption exemption : exemptionList) {
                                if (Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", y, m + 1))) {
                                    m++;
                                    if (m > 11) {
                                        m %= 12;
                                        y++;
                                    }
                                    continue loop;
                                }
                            }

                            for (MonthlyFee monthlyFee : monthlyFeeList) {
                                if (Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m + 1))) {
                                    fee = monthlyFee.getAmount();
                                }
                            }

                            totalAmount -= fee;
                            if (totalAmount <= 0) {
                                break;
                            }

                            m++;
                            if (m > 11) {
                                m %= 12;
                                y++;
                            }
                        }

                        if(fee != 0 && totalAmount % fee != 0){
                            Toast.makeText(getBaseContext(), R.string.toast_require_multiple, Toast.LENGTH_SHORT).show();
                            return ;
                        }
                    }

                    mRealm.beginTransaction();
                    mIncome.setDate(mDate);
                    mIncome.setDay(day);
                    mIncome.setType(type);
                    mIncome.setAmount(amount);
                    mIncome.setCategory(category);
                    mIncome.setContent(content);
                    mIncome.setMemberSeq(mMemberSeq);
                    mIncome.setMemo(memo);
                    if(type == 1)
                        mIncome.setImageFile(imageFile);
                    else
                        mIncome.setImageFile(null);
                    mRealm.commitTransaction();

                    Toast.makeText(getBaseContext(), R.string.toast_modified, Toast.LENGTH_SHORT).show();
                    finish();
                }
                else {
                    final int day = DateUtil.getDayOfMonth(mDate);
                    final int type = mGroupType.getCheckedRadioButtonId() == R.id.radio_income ? 0 : 1;
                    String amountString = mEditAmount.getText().toString().replaceAll(",", "");
                    final int amount = amountString.equals("") ? 0 : Integer.parseInt(amountString);
                    final String category = mBtnCategory.getText().toString();
                    final String content = mEditContent.getText().toString().trim();
                    final String memo = mEditMemo.getText().toString();
                    final byte[] imageFile = ImageUtil.bitmapToByteArray(mPicture);

                    if(amount <= 0){
                        Toast.makeText(getBaseContext(), R.string.toast_require_amount, Toast.LENGTH_SHORT).show();
                        return ;
                    }
                    if(TextUtils.isEmpty(content)){
                        Toast.makeText(getBaseContext(), R.string.toast_require_content, Toast.LENGTH_SHORT).show();
                        return ;
                    }

                    if(category.equals(getString(R.string.activity_income_write_category_fee))) {
                        RealmQuery<Member> memberQuery = mRealm.where(Member.class);
                        Member member = memberQuery.equalTo("seq", mMemberSeq).findFirst();

                        RealmQuery<RegularMeeting> meetingRealmQuery = mRealm.where(RegularMeeting.class);
                        RegularMeeting regularMeeting = meetingRealmQuery.equalTo("seq", member.getMeetingSeq()).findFirst();
                        RealmList<MonthlyFee> monthlyFeeList = regularMeeting.getMonthlyFeeList();

                        RealmQuery<Income> query = mRealm.where(Income.class);
                        RealmResults<Income> incomeList = query.equalTo("memberSeq", member.getSeq()).findAllSorted("date", Sort.ASCENDING);

                        long lastTime = 0;
                        int incomeAmount = 0;
                        for(Income income : incomeList){
                            if(lastTime < income.getDate().getTime()){
                                lastTime = income.getDate().getTime();
                            }
                            incomeAmount += income.getAmount();
                        }

                        if(lastTime > mDate.getTime()){
                            Toast.makeText(getBaseContext(), String.format(getString(R.string.toast_lower_date_3), DateUtil.getDateToYearMonthDayDotString(new Date(lastTime))), Toast.LENGTH_SHORT).show();
                            return ;
                        }

                        int selectedYear = Integer.parseInt(monthlyFeeList.get(monthlyFeeList.size() - 1).getDate().substring(0, 4));
                        int selectedMonth = Integer.parseInt(monthlyFeeList.get(monthlyFeeList.size() - 1).getDate().substring(4, 6));

                        int startYear = DateUtil.getYear(member.getStartDate());
                        int startMonth = DateUtil.getMonth(member.getStartDate());
                        if(startYear > selectedYear || (startYear == selectedYear && startMonth > selectedMonth)){
                            selectedYear = startYear;
                            selectedMonth = startMonth;
                        }

                        RealmList<Exemption> exemptionList = member.getExemptionList();
                        int totalAmount = amount + incomeAmount;
                        int y = startYear;
                        int m = startMonth;
                        long fee = 0;
                        for(MonthlyFee monthlyFee : monthlyFeeList){
                            if(Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m+1))){
                                fee = monthlyFee.getAmount();
                            }
                        }

                        loop:
                        while(y < selectedYear || (y == selectedYear && m <= selectedMonth)){
                            for(Exemption exemption : exemptionList) {
                                if (Integer.parseInt(exemption.getDate()) == Integer.parseInt(String.format("%d%02d", y, m+1))) {
                                    m++;
                                    if(m > 11){
                                        m %= 12;
                                        y++;
                                    }
                                    continue loop;
                                }
                            }

                            for(MonthlyFee monthlyFee : monthlyFeeList){
                                if(Integer.parseInt(monthlyFee.getDate()) <= Integer.parseInt(String.format("%d%02d", y, m+1))){
                                    fee = monthlyFee.getAmount();
                                }
                            }

                            totalAmount -= fee;
                            if(totalAmount <= 0){
                                break;
                            }

                            m++;
                            if(m > 11){
                                m %= 12;
                                y++;
                            }
                        }

                        if(fee != 0 && totalAmount % fee != 0){
                            Toast.makeText(getBaseContext(), R.string.toast_require_multiple, Toast.LENGTH_SHORT).show();
                            return ;
                        }
                    }

                    mRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            Income income = realm.createObject(Income.class);
                            income.setSeq(Income.generateSequence(realm));
                            income.setMeetingSeq(mMeetingSeq);
                            income.setDate(mDate);
                            income.setDay(day);
                            income.setType(type);
                            income.setAmount(amount);
                            income.setCategory(category);
                            income.setContent(content);
                            income.setMemberSeq(mMemberSeq);
                            income.setMemo(memo);
                            if(type == 1)
                                income.setImageFile(imageFile);
                        }
                    }, new Realm.Transaction.Callback() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(getBaseContext(), R.string.toast_save, Toast.LENGTH_SHORT).show();
                            if (isContinue) {
                                mIncome = new Income();
                                mIncome.setDate(new Date());
                                mIncome.setCategory(getString(R.string.activity_income_write_category_nothing));
                                refreshUi();
                                mIncome = null;
                                mScroll.fullScroll(ScrollView.FOCUS_UP);
                                mScroll.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        mEditAmount.requestFocus();
                                    }
                                });
                            } else {
                                finish();
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            Toast.makeText(getBaseContext(), getString(R.string.toast_save_fail) + e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });

                }
                break;
        }
    }

    private void setTextDate(TextView textView, Date date){
        textView.setText(DateUtil.getDateToYearMonthDayWeekString(date));
    }

    @Override
    public void onBackPressed() {
        if(mMode == MODE_UPDATE){
            mMode = MODE_READ;
            changeMode(mMode);
            refreshUi();
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mPicture != null) {
            mPicture.recycle();
            mPicture = null;
        }
        mRealm.close();
    }
}
