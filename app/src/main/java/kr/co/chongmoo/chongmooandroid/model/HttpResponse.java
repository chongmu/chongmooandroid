package kr.co.chongmoo.chongmooandroid.model;

/**
 *
 * HttpResponse
 *
 * @author  Hoon Jang
 * @since   2016.04.06
 */
public class HttpResponse<T> {

    private String returnCode;
    private String returnMessage;
    private T results;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public T getResults() {
        return results;
    }

    public void setResults(T result) {
        this.results = result;
    }
}
