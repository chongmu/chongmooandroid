package kr.co.chongmoo.chongmooandroid.ui.regular;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tsengvn.typekit.Typekit;
import com.woojinsoft.chongmoo.android.R;

import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;

import kr.co.chongmoo.chongmooandroid.util.UiUtils;
import kr.co.chongmoo.chongmooandroid.widget.SlidingTabLayout;

/**
 *
 * 정기모임 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class RegularMeetingActivity extends BaseSubActivity {

    private SlidingTabLayout mTabRegularMeeting;
    private ViewPager mPager;

    private int seq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regular_meeting);
        setTitle(R.string.activity_regular_meeting_title);

        mTabRegularMeeting = (SlidingTabLayout)findViewById(R.id.tab_regular_meeting);
        mPager = (ViewPager)findViewById(R.id.pager);

        mTabRegularMeeting.setWidthLayoutParam(true);
        mTabRegularMeeting.setSelectedIndicatorColors(Color.parseColor("#FF52A7D8"));
        mTabRegularMeeting.setSelectedIndicatorSize(UiUtils.getPixelFromDp(this, 3));
        mTabRegularMeeting.setCustomTabView(R.layout.tab_regular_meeting, R.id.text_title, new SlidingTabLayout.CustomTabCoordinator() {
            @Override
            public void coordinate(ViewGroup tabStrip, int tabIndex) {
                int tabStripChildCount = tabStrip.getChildCount();
                for (int i = 0; i < tabStripChildCount; i++) {
                    if (i == tabIndex)
                        continue;
                    View v = tabStrip.getChildAt(i);
                    TextView textTitle = (TextView) v.findViewById(R.id.text_title);
                    textTitle.setTypeface(Typekit.getInstance().get(Typekit.Style.Normal));
                    textTitle.setTextColor(Color.parseColor("#FF777777"));
                }

                View view = tabStrip.getChildAt(tabIndex);
                TextView textTitle = (TextView) view.findViewById(R.id.text_title);
                textTitle.setTypeface(Typekit.getInstance().get(Typekit.Style.Bold));
                textTitle.setTextColor(Color.parseColor("#FF52A7D8"));
            }
        });

        mPager.setAdapter(new RegularMeetingPagerAdapter(getSupportFragmentManager()));
        mPager.setCurrentItem(1);
        mTabRegularMeeting.setViewPager(mPager);

        Bundle b = getIntent().getExtras();
        if(b != null) {
            setTitle(b.getString("NAME"));
            seq = b.getInt("SEQ", 0);
//            Toast.makeText(RegularMeetingActivity.this, "" + seq, Toast.LENGTH_SHORT).show();
        }
    }

    public int getSeq() {
        return seq;
    }
}
