package kr.co.chongmoo.chongmooandroid.ui.holder;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import com.woojinsoft.chongmoo.android.R;
import kr.co.chongmoo.chongmooandroid.model.Notice;
import kr.co.chongmoo.chongmooandroid.ui.common.WebViewActivity;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 게시글 목록 holder
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class NoticeHolder extends RecyclerView.ViewHolder{
    public static final int RESOURCE_ID = R.layout.recycler_item_notice;

    public TextView textTitle;
    public TextView textDate;

    public NoticeHolder(View itemView) {
        super(itemView);
        textTitle = (TextView)itemView.findViewById(R.id.text_title);
        textDate = (TextView)itemView.findViewById(R.id.text_date);
    }

    public static class CommonCoordinator implements BeautifulRecyclerAdapter.Coordinator<NoticeHolder, Notice>{
        private Activity mActivity;
        private String mCategory;

        public CommonCoordinator(Activity mActivity, String category) {
            this.mActivity = mActivity;
            this.mCategory = category;
        }

        @Override
        public void coordinate(BeautifulRecyclerAdapter<NoticeHolder, Notice> adapter, NoticeHolder holder, List<Notice> itemList, int position) {
            final Notice item = itemList.get(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, WebViewActivity.class);
                    intent.putExtra(WebViewActivity.EXTRA_CATEGORY, mCategory);
                    intent.putExtra(WebViewActivity.EXTRA_TITLE, item.getTitle());
                    intent.putExtra(WebViewActivity.EXTRA_DATE, item.getRegistDate());
                    intent.putExtra(WebViewActivity.EXTRA_CONTENT, item.getContent());
                    intent.putExtra(WebViewActivity.EXTRA_IS_URL, false);
                    mActivity.startActivity(intent);
                }
            });
            holder.textTitle.setText(item.getTitle());
            holder.textDate.setText(item.getRegistDate().split("\\s")[0]);
        }
    }
}
