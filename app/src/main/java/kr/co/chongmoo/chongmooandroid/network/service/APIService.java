package kr.co.chongmoo.chongmooandroid.network.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import kr.co.chongmoo.chongmooandroid.model.FeePaymentSummary;
import kr.co.chongmoo.chongmooandroid.model.HttpResponse;
import kr.co.chongmoo.chongmooandroid.model.MobileVersion;
import kr.co.chongmoo.chongmooandroid.model.Notice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 *
 * API 정보
 *
 * @author  Hoon Jang
 * @since   2016.04.05
 */
public interface APIService {

    @GET("/hankyung/login/{userid}")
    Call<Map<String, Object>> login(@Path("userid") String userid);

    @GET("/api/version/andorid/lastestVersion")
    Call<HttpResponse<MobileVersion>> lastestVersion();

    @GET("/api/board/faq/getFaqList")
    Call<HttpResponse<List<Notice>>> getFaqList();

    @GET("/api/board/alarm/getAlarmList")
    Call<HttpResponse<List<Notice>>> getNoticeList();

    @Headers( "Content-Type: application/json" )
    @POST("/api/share/addMonthySummary")
    Call<HttpResponse<String>> addMonthySummary(@Body HashMap<String, Object> json);

    @Headers( "Content-Type: application/json" )
    @POST("/api/share/addFeePaymentSummary")
    Call<HttpResponse<String>> addFeePaymentSummary(@Body FeePaymentSummary json);

    @GET("/api/category/incomeTypeList")
    Call<HttpResponse<List<String>>> incomeTypeList();

    @GET("/api/category/spendingTypeList")
    Call<HttpResponse<List<String>>> spendingTypeList();

}


