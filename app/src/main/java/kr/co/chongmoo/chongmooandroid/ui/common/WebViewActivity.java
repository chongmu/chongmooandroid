package kr.co.chongmoo.chongmooandroid.ui.common;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.woojinsoft.chongmoo.android.R;
import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;

/**
 *
 * 웹뷰
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class WebViewActivity extends BaseSubActivity {
    public static final String EXTRA_CATEGORY = "CATEGORY";
    public static final String EXTRA_TITLE = "TITLE";
    public static final String EXTRA_DATE = "DATE";
    public static final String EXTRA_CONTENT = "CONTENT";
    public static final String EXTRA_IS_URL = "IS_URL";

    private LinearLayout mLayoutCategory;
    private TextView mTextSubject;
    private TextView mTextDate;
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        setTitle("Web");

        mLayoutCategory = (LinearLayout) findViewById(R.id.layout_category);
        mTextSubject = (TextView) findViewById(R.id.text_subject);
        mTextDate = (TextView) findViewById(R.id.text_date);
        mWebView = (WebView) findViewById(R.id.webview);

        initWebView();

        Intent intent = getIntent();
        if(intent != null){
            Bundle extras = intent.getExtras();
            if(extras != null){
                String category = extras.getString(EXTRA_CATEGORY);
                String title = extras.getString(EXTRA_TITLE);
                String date = extras.getString(EXTRA_DATE);
                String content = extras.getString(EXTRA_CONTENT);
                boolean isUrl = extras.getBoolean(EXTRA_IS_URL);

                setTitle(category);
                if(isUrl){
                    mWebView.setWebViewClient(new WebClient());
                    mWebView.loadUrl(content);
                }
                else{
                    mLayoutCategory.setVisibility(View.VISIBLE);
                    mTextSubject.setText(title);
                    mTextDate.setText(date);
                    mWebView.loadData(content, "text/html; charset=UTF-8", "UTF-8");
                }
            }
        }
    }

    private void initWebView(){
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUseWideViewPort(false);
        settings.setDefaultTextEncodingName("UTF-8"); //or UTF-8
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        settings.setSupportMultipleWindows(false);
        settings.setAllowFileAccess(true);
        settings.setAppCacheEnabled(true);
        settings.setAppCachePath(getCacheDir().toString());
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setLoadsImagesAutomatically(true);
        settings.setLoadWithOverviewMode(true);
        settings.setNeedInitialFocus(false);
        settings.setGeolocationEnabled(true);

        mWebView.requestFocusFromTouch();
//        mWebView.setWebChromeClient(new HipassWebChromeClient(this, mWebView));
//        mWebView.setWebViewClient(new HipassWebViewClient(this, mWebView));

    }

    public static class WebClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
