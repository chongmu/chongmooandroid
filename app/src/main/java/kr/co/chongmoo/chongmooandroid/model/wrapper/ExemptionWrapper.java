package kr.co.chongmoo.chongmooandroid.model.wrapper;

import kr.co.chongmoo.chongmooandroid.model.Exemption;

/**
 *
 * Exemption Wrapper
 *
 * @author  Hoon Jang
 * @since   2016.04.30
 */
public class ExemptionWrapper {

    private String date;

    public ExemptionWrapper(Exemption original) {
        this.date = original.getDate();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
