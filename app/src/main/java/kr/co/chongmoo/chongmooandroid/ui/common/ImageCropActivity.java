package kr.co.chongmoo.chongmooandroid.ui.common;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.woojinsoft.chongmoo.android.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import kr.co.chongmoo.chongmooandroid.ui.base.BaseActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.IncomeWriteActivity;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 *
 * 이미지 크롭 액티비티
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class ImageCropActivity extends BaseActivity implements View.OnClickListener{
    public static final String EXTRA_IMAGE_PATH = "IMAGE";

    private ImageView mImage;
    private View mViewCrop;

    private PhotoViewAttacher mAttacher;

    private int mDegree = 0;
    private Bitmap mOriginalImage;
    private Bitmap mScaledBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);

        mImage = (ImageView) findViewById(R.id.image);
        mViewCrop = findViewById(R.id.view_crop);
        Button btnClose = (Button) findViewById(R.id.btn_close);
        ImageButton btnX = (ImageButton) findViewById(R.id.btn_x);
        ImageButton btnRotate = (ImageButton) findViewById(R.id.btn_rotate);
        ImageButton btnOk = (ImageButton) findViewById(R.id.btn_ok);

        btnClose.setOnClickListener(this);
        btnX.setOnClickListener(this);
        btnRotate.setOnClickListener(this);
        btnOk.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String imagePath = bundle.getString(EXTRA_IMAGE_PATH);
            File imgFile = new  File(imagePath);
            if(imgFile.exists()){
                Bitmap original = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                int height = original.getHeight();
                int width = original.getWidth();
                mOriginalImage = null;
                if(width > 1080){
                    mOriginalImage = Bitmap.createScaledBitmap(original, 1080, (height * 1080) / width, true);
                    original.recycle();
                    original = null;
                }
                else{
                    mOriginalImage = original;
                }
                mImage.setImageBitmap(mOriginalImage);
            }
        }

        mAttacher = new PhotoViewAttacher(mImage);
        mAttacher.setMinimumScale(0.5f);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_close:
            case R.id.btn_x:
                onBackPressed();
                break;
            case R.id.btn_rotate:
                mDegree = (mDegree + 90) % 360;
                mAttacher.setRotationTo(mDegree);
                break;
            case R.id.btn_ok:
                Bitmap bitmap = Bitmap.createBitmap(mImage.getWidth(), mImage.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                mImage.draw(canvas);

                int[] activityLocation = new int[2];
                mViewCrop.getRootView().findViewById(android.R.id.content).getLocationInWindow(activityLocation);

                int[] screenLocation = new int[2];
                mViewCrop.getLocationOnScreen(screenLocation);

                Bitmap croppedBitmap = Bitmap.createBitmap(bitmap, screenLocation[0] , screenLocation[1] - activityLocation[1] , mViewCrop.getWidth() , mViewCrop.getHeight());
                bitmap.recycle();
                bitmap = null;

                int height = croppedBitmap.getHeight();
                int width = croppedBitmap.getWidth();
                mScaledBitmap = null;
                if(width > 480){
                    mScaledBitmap = Bitmap.createScaledBitmap(croppedBitmap, 480, (height * 480) / width, true);
                    croppedBitmap.recycle();
                    croppedBitmap = null;
                }
                else{
                    mScaledBitmap = croppedBitmap;
                }

                final ChongmooDialog dialog = new ChongmooDialog(this, ChongmooDialog.MODE_LOADING);
                dialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        File tempDir = new File(Environment.getExternalStorageDirectory() + File.separator + "chongmoo" + File.separator + "temp");
                        if(!tempDir.exists()){
                            tempDir.mkdirs();
                        }

                        final File tempFile = new File(tempDir, "crop.jpg");
                        FileOutputStream fos = null;
                        try{
                            tempFile.createNewFile();
                            fos = new FileOutputStream(tempFile);
                            mScaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    Intent intent = new Intent();
                                    intent.putExtra(EXTRA_IMAGE_PATH, tempFile.getAbsolutePath());
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if(fos != null)
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                        }
                    }
                }).start();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mOriginalImage != null){
            mOriginalImage.recycle();
            mOriginalImage = null;
        }
        if(mScaledBitmap != null){
            mScaledBitmap.recycle();
            mScaledBitmap = null;
        }
    }
}
