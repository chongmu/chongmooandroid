package kr.co.chongmoo.chongmooandroid.ui.regular;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.LinkAddress;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import com.woojinsoft.chongmoo.android.R;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.ui.holder.IncomeHolder;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;
import kr.co.chongmoo.chongmooandroid.widget.GraphView;
import kr.lunawyrd.widget.adapter.BeautifulRecyclerAdapter;

/**
 *
 * 수입/지출 Fragment
 *
 * @author  Hoon Jang
 * @since   2016.04.06
 */
public class IncomeFragment extends Fragment implements View.OnClickListener{
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshList();
        }
    };


    private TextView mTextMonth;
    private TextView mTextTotal;
    private TextView mTextTotalOutcome;
    private TextView mTextTotalIncome;
    private GraphView mGraph;

    private LinearLayout mLayoutEmpty;
    private RecyclerView mRecyclerIncome;

    private Realm mRealm;

    private int mYear;
    private int mMonth;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_income, container, false);

        Button btnMonthLeft = (Button) rootView.findViewById(R.id.btn_month_left);
        mTextMonth = (TextView) rootView.findViewById(R.id.text_month);
        Button btnMonthRight = (Button) rootView.findViewById(R.id.btn_month_right);
        mTextTotal = (TextView) rootView.findViewById(R.id.text_total);
        mTextTotalOutcome = (TextView) rootView.findViewById(R.id.text_total_outcome);
        mTextTotalIncome = (TextView) rootView.findViewById(R.id.text_total_income);
        mGraph = (GraphView) rootView.findViewById(R.id.graph);

        btnMonthLeft.setOnClickListener(this);
        mTextMonth.setOnClickListener(this);
        btnMonthRight.setOnClickListener(this);

        mLayoutEmpty = (LinearLayout)rootView.findViewById(R.id.layout_empty);
        mRecyclerIncome = (RecyclerView)rootView.findViewById(R.id.recycler_income);
        mRecyclerIncome.setLayoutManager(new LinearLayoutManager(getContext()));

        LinearLayout layoutWrite = (LinearLayout)rootView.findViewById(R.id.layout_write);
        layoutWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), IncomeWriteActivity.class);
                intent.putExtra(IncomeWriteActivity.EXTRA_MODE, IncomeWriteActivity.MODE_WRITE);
                intent.putExtra("MEETING_SEQ", ((RegularMeetingActivity) getActivity()).getSeq());
                startActivity(intent);
            }
        });

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(new Date().getTime());

        mYear = cal.get(Calendar.YEAR);
        mMonth = cal.get(Calendar.MONTH);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mRealm = Realm.getDefaultInstance();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mReceiver, new IntentFilter("REFRESH"));
        refreshList();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_month_left: {
                mMonth = mMonth - 1;
                if (mMonth < 0) {
                    mMonth = 11;
                    mYear--;
                }
                refreshList();
                break;
            }
            case R.id.text_month: {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, mYear);
                cal.set(Calendar.MONTH, mMonth);

                final ChongmooDialog dialog = new ChongmooDialog(getActivity(), ChongmooDialog.MODE_CALRENDAR_MONTH, false);
                dialog.setTitle(getString(R.string.dialog_calendar_text));
                dialog.setDate(new Date(cal.getTimeInMillis()));
                dialog.setPositiveButton(getString(R.string.dialog_calendar_positive_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(dialog.getDate().getTime());

                        mYear = cal.get(Calendar.YEAR);
                        mMonth = cal.get(Calendar.MONTH);

                        refreshList();
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton(getString(R.string.dialog_calendar_negative_text), null);
                dialog.show();
                break;
            }
            case R.id.btn_month_right: {
                mMonth = mMonth + 1;
                if (mMonth > 11) {
                    mMonth = 0;
                    mYear++;
                }
                refreshList();
                break;
            }
        }
    }

    private void refreshList(){

        mTextMonth.setText(String.format(getString(R.string.fragment_income_month), mYear, mMonth+1));

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, mYear);
        cal.set(Calendar.MONTH, mMonth);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date startDate = new Date();
        cal.set(Calendar.YEAR, mYear);
        cal.set(Calendar.MONTH, mMonth);
        startDate.setTime(cal.getTimeInMillis());

        Date endDate = new Date();
        cal.set(Calendar.YEAR, mYear);
        cal.set(Calendar.MONTH, mMonth + 1);
        endDate.setTime(cal.getTimeInMillis());

        RealmQuery<Income> query = mRealm.where(Income.class);
        RealmResults<Income> results = query.equalTo("meetingSeq", ((RegularMeetingActivity) getActivity()).getSeq()).between("date", startDate, endDate).findAllSorted("date", Sort.DESCENDING);
        ArrayList<Income> itemList = new ArrayList<>(results);

        int change = 0;
        query = mRealm.where(Income.class);
        RealmResults<Income> beforeIncomeList = query.equalTo("meetingSeq", ((RegularMeetingActivity) getActivity()).getSeq()).lessThan("date", startDate).findAll();
        Date firstDate = null;
        for(Income income : beforeIncomeList){
            if(firstDate == null || firstDate.getTime() > income.getDate().getTime())
                firstDate = income.getDate();

            if(income.getType() == 0){
                change += income.getAmount();
            }
            else{
                change -= income.getAmount();
            }
        }

        if(firstDate != null) {
            Calendar firstCal = Calendar.getInstance();
            firstCal.setTime(firstDate);

            int firstYear = firstCal.get(Calendar.YEAR);
            int firstMonth = firstCal.get(Calendar.MONTH);
            if(firstYear < mYear || (firstYear == mYear && firstMonth < mMonth)){
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, mYear);
                calendar.set(Calendar.MONTH, mMonth);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                Income chargeIncome = new Income();
                chargeIncome.setType(0);
                chargeIncome.setDate(new Date(calendar.getTimeInMillis()));
                chargeIncome.setDay(1);
                chargeIncome.setAmount(change);
                chargeIncome.setCategory(getString(R.string.activity_closing_account_write_month));
                chargeIncome.setContent(getString(R.string.activity_closing_account_write_month));
                itemList.add(chargeIncome);
            }
        }

        for(int i=1; i<32; i++){
            for(int index=0; index < itemList.size(); index++){
                Income income = itemList.get(index);
                int day = income.getDay();
                if(i == day){
                    income.setIsTop(true);
                    break;
                }
            }
        }

        long incomeTotal = 0;
        long outcomeTotal = 0;
        for(Income income : itemList){
            if(income.getType() == 0){
                incomeTotal += income.getAmount();
            }
            else{
                outcomeTotal += income.getAmount();
            }
        }

        mGraph.setTotalIncome(incomeTotal);
        mGraph.setTotalOutome(outcomeTotal);

        long total = incomeTotal - outcomeTotal;
        mTextTotal.setText(String.format("%,d", total));
        mTextTotal.setTextColor(total < 0 ? Color.parseColor("#FFF5606B") : Color.parseColor("#FF222222"));
        mTextTotalOutcome.setText(String.format(getString(R.string.fragment_income_outcome), outcomeTotal));
        mTextTotalIncome.setText(String.format(getString(R.string.fragment_income_income), incomeTotal));

        mRecyclerIncome.setAdapter(new BeautifulRecyclerAdapter<>(getContext(), IncomeHolder.RESOURCE_ID, itemList, new IncomeHolder.CommonCoordinator(getActivity())));

        mLayoutEmpty.setVisibility(itemList.size() == 0 ? View.VISIBLE : View.GONE);
        mRecyclerIncome.setVisibility(itemList.size() != 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mReceiver);
        mRealm.close();
    }

}
