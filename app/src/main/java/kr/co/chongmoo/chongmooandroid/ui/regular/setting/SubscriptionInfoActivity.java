package kr.co.chongmoo.chongmooandroid.ui.regular.setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.google.gson.Gson;
import com.woojinsoft.chongmoo.android.R;

import java.util.Date;

import kr.co.chongmoo.chongmooandroid.ui.base.BaseSubActivity;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.util.Purchase;

/**
 *
 * 구독 정보 화면
 *
 * @author  Hoon Jang
 * @since   2016.05.23
 */
public class SubscriptionInfoActivity extends BaseSubActivity {

    private TextView mTextStatus;
    private TextView mTextStartDate;
    private TextView mTextOrderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_info);
        setTitle(R.string.activity_subscription_info_title);

        mTextStatus = (TextView) findViewById(R.id.text_status);
        mTextStartDate = (TextView) findViewById(R.id.text_start_date);
        mTextOrderId = (TextView) findViewById(R.id.text_order_id);

        SharedPreferences pref = getSharedPreferences("chongmoo", Context.MODE_PRIVATE);
        String json = pref.getString("PURCHASE", "");
        if(!TextUtils.isEmpty(json)){
            Purchase purchase = new Gson().fromJson(json, Purchase.class);

            String product = "";
            switch (purchase.getSku()){
                case "monthlyproduct":
                    product = getString(R.string.activity_subscription_info_product_monthly);
                    break;
                case "yearlyproduct":
                    product = getString(R.string.activity_subscription_info_product_yearly);
                    break;
                default:
                    product = purchase.getSku();
            }

            mTextStatus.setText(product);
            mTextStartDate.setText(DateUtil.getDateToYearMonthDayDotString(new Date(purchase.getPurchaseTime())));
            mTextOrderId.setText(purchase.getOrderId());
        }
    }
}
