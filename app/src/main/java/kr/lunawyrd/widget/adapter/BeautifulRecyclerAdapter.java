package kr.lunawyrd.widget.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import kr.co.chongmoo.chongmooandroid.model.BackupFile;
import kr.co.chongmoo.chongmooandroid.ui.holder.BackupFileHolder;

/**
 *
 * 일반적인 RecyclerView를 만들 때 사용할 수 있는 공통 어댑터
 *
 * @author Hoon Jang
 * @since 2015-07-17
 * @version 1.1(2015-08-13)
 * @param <VH> ViewHolder
 * @param <E> Item
 */
public class BeautifulRecyclerAdapter<VH extends RecyclerView.ViewHolder, E> extends RecyclerView.Adapter<VH>{


    /**
     *
     * UI 작업을 위한 인터페이스, Holder와 1 : N 관계를 가진다.
     *
     * @author Hoon Jang
     * @since 2015-07-17
     * @version 1.1(2015-08-13)
     * @param <VH> ViewHolder
     * @param <E> Item
     */
    public interface Coordinator<VH extends RecyclerView.ViewHolder, E>{
        void coordinate(BeautifulRecyclerAdapter<VH, E> adapter, VH holder, List<E> itemList, int position);
    }

    private Context mContext;
    private LayoutInflater mInflater;
    private int mResourceId;
    private List<E> mItemList;
    private Coordinator<VH, E> mCoordinator;

    public BeautifulRecyclerAdapter(@NonNull Context context, int resourceId, @NonNull List<E> itemList, @NonNull Coordinator<VH, E> coordinator) {
        mContext = context;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResourceId = resourceId;
        mItemList = itemList;
        mCoordinator = coordinator;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType){
        View view = mInflater.inflate(mResourceId, parent, false);
        VH holder = null;
        try {
            ParameterizedType pt1 = (ParameterizedType)mCoordinator.getClass().getGenericInterfaces()[0];
            holder = (VH)Class.forName(pt1.getActualTypeArguments()[0].toString().split("\\s")[1]).getConstructor(View.class).newInstance(view);
        } catch (InstantiationException e) {
            e.printStackTrace();
            Log.d("TEST", e.toString());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            Log.d("TEST", e.toString());
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            Log.d("TEST", e.toString());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            Log.d("TEST", e.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            Log.d("TEST", e.toString());
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        mCoordinator.coordinate(this, holder, mItemList, position);
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    public List<E> getItemList() {
        return mItemList;
    }

    public void setItemList(List<E> itemList) {
        this.mItemList = itemList;
    }
}
