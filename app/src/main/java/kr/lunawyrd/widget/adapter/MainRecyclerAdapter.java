package kr.lunawyrd.widget.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import com.woojinsoft.chongmoo.android.R;

import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import kr.co.chongmoo.chongmooandroid.model.CasualMeeting;
import kr.co.chongmoo.chongmooandroid.model.ClosingAccount;
import kr.co.chongmoo.chongmooandroid.model.Exemption;
import kr.co.chongmoo.chongmooandroid.model.Income;
import kr.co.chongmoo.chongmooandroid.model.Member;
import kr.co.chongmoo.chongmooandroid.model.MonthlyFee;
import kr.co.chongmoo.chongmooandroid.model.PaymentStatus;
import kr.co.chongmoo.chongmooandroid.model.RegularMeeting;
import kr.co.chongmoo.chongmooandroid.ui.MainActivity;
import kr.co.chongmoo.chongmooandroid.ui.casual.CasualMeetingActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.RegularMeetingActivity;
import kr.co.chongmoo.chongmooandroid.ui.regular.setting.RegularMeetingSettingActivity;
import kr.co.chongmoo.chongmooandroid.util.DateUtil;
import kr.co.chongmoo.chongmooandroid.widget.ChongmooDialog;
import kr.co.chongmoo.chongmooandroid.widget.VersionDialog;

/**
 * 메인 화면에 정기모임 및 N빵 미팅 이 있을 경우 해당 데이터를 뿌린다.
 * Created by ??? on 2016-04-15.
 */
public class MainRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int mCasualMeetingResourceId = CasualMeetingHolder.RESOURCE_ID;
    private int mRegularMeetingResourceId = RegularMeetingHolder.RESOURCE_ID;

    private LayoutInflater mInflater;
    private List<Object> mItemList;
    private Context mContext;



    public MainRecyclerAdapter(@NonNull Context context, @NonNull List<Object> itemList) {
        mContext = context;
        mItemList = itemList;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(viewType, parent, false);
        RecyclerView.ViewHolder holder = null;

        if(viewType == mCasualMeetingResourceId) {
            holder = new CasualMeetingHolder(view);
        }
        else if(viewType == mRegularMeetingResourceId) {
            holder = new RegularMeetingHolder(view);
        }
        else {
            holder = new RegularMeetingHolder(view);
        }
        return holder;
    }

    @Override
    public int getItemViewType(int position) {
        Object object = getItemList().get(position);

        int returnResourceId = 0;
        if(object instanceof CasualMeeting) {
            returnResourceId = mCasualMeetingResourceId;
        }
        else if(object instanceof RegularMeeting) {
            returnResourceId = mRegularMeetingResourceId;
        }
        else {
            returnResourceId = mRegularMeetingResourceId;
        }
        return returnResourceId;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof CasualMeetingHolder) {

            final CasualMeeting item = (CasualMeeting) getItemList().get(position);
            ((CasualMeetingHolder) holder).textViewTxtYesDataListTitle.setText(item.getName());
            ((CasualMeetingHolder) holder).textViewTxtYesDataListDate.setText(DateUtil.getDateToYearMonthDayString(item.getDate()));
            ((CasualMeetingHolder) holder).linearLayoutLayerYesDataList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, CasualMeetingActivity.class);
                    intent.putExtra(CasualMeetingActivity.USE_MODE_KEY, CasualMeetingActivity.USE_MODE_VALUE_READ);
                    intent.putExtra(CasualMeetingActivity.SEQ_KEY, item.getSeq());
                    mContext.startActivity(intent);
                }
            });
            ((CasualMeetingHolder) holder).imageViewBtnYesDataListDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final VersionDialog versionDialog = new VersionDialog(mContext);
                    versionDialog.setDialogText(mContext.getString(R.string.dialog_wanna_delete_text));
                    versionDialog.setNegativeButtonText(mContext.getString(R.string.no));
                    versionDialog.setPositiveButtonText(mContext.getString(R.string.yes));
                    versionDialog.setPositiveButtonListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            versionDialog.dismiss();
                            ((MainActivity) mContext).deleteCasualMeeting(item.getSeq());
                        }
                    });
                    versionDialog.show();
                }
            });

        } else if (holder instanceof RegularMeetingHolder) {
            final RegularMeeting item = (RegularMeeting) getItemList().get(position);

            Realm realm = ((MainActivity)mContext).getRealm();


            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, DateUtil.getYear(new Date()));
            cal.set(Calendar.MONTH, DateUtil.getMonth(new Date()));
            cal.set(Calendar.DAY_OF_MONTH, 1);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);

            cal.add(Calendar.MONTH, 1);

            Date startDate = new Date();
            startDate.setTime(cal.getTimeInMillis());

            long change = 0;
            RealmQuery<Income> query = realm.where(Income.class);
            RealmResults<Income> beforeIncomeList = query.equalTo("meetingSeq", item.getSeq()).lessThan("date", startDate).findAll();
            Date firstDate = null;
            for(Income income : beforeIncomeList){
                if(firstDate == null || firstDate.getTime() > income.getDate().getTime())
                    firstDate = income.getDate();

                if(income.getType() == 0){
                    change += income.getAmount();
                }
                else{
                    change -= income.getAmount();
                }
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!((MainActivity)mContext).isNetWorkAble()){
                        ((MainActivity)mContext).showNetworkProblemDialog(false);
                        return;
                    }

                    if(((MainActivity)mContext).isSubscription()){
                        Intent intent = new Intent(mContext, RegularMeetingActivity.class);
                        intent.putExtra("NAME", item.getName());
                        intent.putExtra("SEQ", item.getSeq());
                        mContext.startActivity(intent);
                    }
                    else{
                        final ChongmooDialog chongmooDialog = new ChongmooDialog(mContext);
                        chongmooDialog.setMessage(mContext.getString(R.string.dialog_regular_delete_message));
                        chongmooDialog.setPositiveButton(mContext.getString(R.string.dialog_regular_delete), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Realm realm = ((MainActivity)mContext).getRealm();
                                realm.beginTransaction();
                                RealmQuery<Income> incomeQuery = realm.where(Income.class);
                                incomeQuery.equalTo("meetingSeq", item.getSeq()).findAll().deleteAllFromRealm();

                                RealmQuery<Member> memberQuery = realm.where(Member.class);
                                memberQuery.equalTo("meetingSeq", item.getSeq()).findAll().deleteAllFromRealm();

                                RealmQuery<ClosingAccount> closingAccountQuery = realm.where(ClosingAccount.class);
                                closingAccountQuery.equalTo("meetingSeq", item.getSeq()).findAll().deleteAllFromRealm();

                                RealmQuery<RegularMeeting> query = realm.where(RegularMeeting.class);
                                RegularMeeting regularMeeting = query.equalTo("seq", item.getSeq()).findFirst();
                                regularMeeting.removeFromRealm();
                                realm.commitTransaction();
                                chongmooDialog.dismiss();;

                                ((MainActivity)mContext).showMeetingListArea();
                            }
                        });
                        chongmooDialog.setNegativeButton(mContext.getString(R.string.close), null);
                        chongmooDialog.show();
                    }
                }
            });
            ((RegularMeetingHolder) holder).textName.setText(item.getName());
            ((RegularMeetingHolder) holder).textMemberCount.setText(((MainActivity) mContext).getMemberCount(item.getSeq()) + "");

            ((RegularMeetingHolder) holder).textMonthlyFee.setText(String.format("%,d", change));
            ((RegularMeetingHolder) holder).layoutSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!((MainActivity)mContext).isNetWorkAble()){
                        ((MainActivity)mContext).showNetworkProblemDialog(false);
                        return;
                    }

                    Intent intent = new Intent(mContext, RegularMeetingSettingActivity.class);
                    intent.putExtra("NAME", item.getName());
                    intent.putExtra("SEQ", item.getSeq());
                    intent.putExtra("IS_SUBSCRIPTION",((MainActivity)mContext).isSubscription());
                    mContext.startActivity(intent);
                }
            });
            ((RegularMeetingHolder) holder).btnSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!((MainActivity)mContext).isNetWorkAble()){
                        ((MainActivity)mContext).showNetworkProblemDialog(false);
                        return;
                    }

                    Intent intent = new Intent(mContext, RegularMeetingSettingActivity.class);
                    intent.putExtra("NAME", item.getName());
                    intent.putExtra("SEQ", item.getSeq());
                    intent.putExtra("IS_SUBSCRIPTION",((MainActivity)mContext).isSubscription());
                    mContext.startActivity(intent);
                }
            });


            Drawable bookmarkDrawable = null;
            if(item.isTop() == true) {
                bookmarkDrawable = mContext.getResources().getDrawable(R.drawable.main_star_act);
            } else {
                bookmarkDrawable = mContext.getResources().getDrawable(R.drawable.main_star_deact);
            }
            ((RegularMeetingHolder) holder).btnMainRegularBookmark.setImageDrawable(bookmarkDrawable);
            ((RegularMeetingHolder) holder).btnMainRegularBookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    boolean isTop = item.isTop();
                    int seq = item.getSeq();
                    ((MainActivity) mContext).updateBookMark(seq, isTop);
                    ((MainActivity) mContext).showMeetingListArea();

                }
            });

        } else {

        }
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    public List<Object> getItemList() {
        return mItemList;
    }

    public void setItemList(List<Object> itemList) {
        this.mItemList = itemList;
    }

    public class RegularMeetingHolder extends RecyclerView.ViewHolder {

        public static final int RESOURCE_ID = R.layout.recycler_item_regular_meeting;

        private TextView textName;
        private TextView textMemberCount;
        private TextView textMonthlyFee;
        private LinearLayout layoutSetting;
        private Button btnSetting;
        private ImageView btnMainRegularBookmark;

        public RegularMeetingHolder(View itemView) {

            super(itemView);
            textName = (TextView)itemView.findViewById(R.id.text_name);
            textMemberCount = (TextView)itemView.findViewById(R.id.text_member_count);
            textMonthlyFee = (TextView)itemView.findViewById(R.id.text_monthly_fee);
            layoutSetting = (LinearLayout) itemView.findViewById(R.id.layout_setting);
            btnSetting = (Button)itemView.findViewById(R.id.btn_setting);
            btnMainRegularBookmark = (ImageView) itemView.findViewById(R.id.btn_main_regular_bookmark);

        }
    }

    public class CasualMeetingHolder extends RecyclerView.ViewHolder {

        public static final int RESOURCE_ID = R.layout.recycler_item_casual_meeting;

        private LinearLayout linearLayoutLayerYesDataList;
        private TextView textViewTxtYesDataListDate;
        private TextView textViewTxtYesDataListTitle;
        private ImageView imageViewBtnYesDataListDel;

        public CasualMeetingHolder(View itemView) {

            super(itemView);
            linearLayoutLayerYesDataList = (LinearLayout) itemView.findViewById(R.id.layer_yes_data_list);
            textViewTxtYesDataListDate = (TextView) itemView.findViewById(R.id.txt_yes_data_list_date);
            textViewTxtYesDataListTitle = (TextView) itemView.findViewById(R.id.txt_yes_data_list_title);
            imageViewBtnYesDataListDel = (ImageView) itemView.findViewById(R.id.btn_yes_data_list_del);

        }
    }

}
